import { Workday } from './workday';
import { Task } from './task';

export interface Offer {
  id: number;
  guaranteedDays: Workday[];
  task: Task;
}

export interface CreateOffer {
  taskId: number;
  guaranteedDays: Workday[];
}
