import React from 'react';
import { makeStyles, Box } from '@material-ui/core';

const LIMIT = 15;

interface IPersonListProps {
  value: number;
  onInput: (v: number) => void;
}

const useStyles = makeStyles((theme) => ({
  person: {
    width: '17px',
    height: '44px',
    cursor: 'pointer',
    '&:nth-child(5n+1)': {
      marginLeft: theme.spacing(2),
    },
  },
}));

const PersonList: React.FC<IPersonListProps> = ({ value, onInput }) => {
  const classes = useStyles();

  const persons = [];
  for (let i = 1; i <= LIMIT; i++) {
    persons.push(i <= value);
  }

  return (
    <Box display="flex" alignItems="center">
      {persons.map((p, i) => (
        <img
          key={i}
          className={classes.person}
          alt="person icon"
          src={`/img/person_${p ? 'green' : 'grey'}.svg`}
          onClick={() => onInput(i + 1)}
        />
      ))}
      {value > LIMIT && (
        <Box ml={2}><b>+ {value - LIMIT}</b></Box>
      )}
    </Box>
  );
};

export default PersonList;
