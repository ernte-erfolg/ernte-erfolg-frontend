import React from 'react';
import { useLocation } from 'react-router-dom';
import { ThemeProvider, CssBaseline, makeStyles } from '@material-ui/core';
import Navigation from '../components/Navigation';
import theme from '../theme';
import WideContainer from './WideContainer';
import Footer from '../components/Footer';

const useStyles = makeStyles({
  root: {
    display: 'flex',
    flexDirection: 'column',
    height: '100%',
  },
  mainContent: {
    flexGrow: 1,
  },
});

const Layout: React.FC<React.PropsWithChildren<{}>> = ({ children }) => {
  const classes = useStyles();
  const location = useLocation();

  // on the landing page and on the farm profile page we want the hero image sitting flush with the bottom
  // of the navigation bar.
  const applyNegativeMargin = location.pathname === '/' || location.pathname.startsWith('/farm/profile');

  return (
    <ThemeProvider theme={theme}>
      <CssBaseline></CssBaseline>
      <div className={classes.root}>
        <Navigation />
        <div className={classes.mainContent}>
          <WideContainer applyNegativeMargin={applyNegativeMargin}>{children}</WideContainer>
        </div>
        <WideContainer>
          <Footer />
        </WideContainer>
      </div>
    </ThemeProvider>
  );
};

export default Layout;
