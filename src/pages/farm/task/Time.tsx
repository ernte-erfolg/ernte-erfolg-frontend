import React from 'react';
import { makeStyles, Typography } from '@material-ui/core';
import { Period } from '../../../components';
import { useSelector } from 'react-redux';
import { selectFarmDaysMoment } from '../../../slices/farmCalendarSlice';

const useStyles = makeStyles((theme) => ({
  root: {
    marginTop: theme.spacing(4),
  },
}));

const Time: React.FC = () => {
  const classes = useStyles();
  const days = useSelector(selectFarmDaysMoment);

  const timePeriods: Array<{ startIndex: number; endIndex: number }> = [];
  days.forEach((d, i) => {
    if (timePeriods.length === 0) {
      timePeriods.push({ startIndex: i, endIndex: i });
    } else {
      const lastPeriod = timePeriods[timePeriods.length - 1];
      const lastDay = days[lastPeriod.endIndex];
      if (d.date.diff(lastDay.date, 'days') > 1 || !d.date.isSame(lastDay.date, 'month')) {
        timePeriods.push({ startIndex: i, endIndex: i });
      } else {
        lastPeriod.endIndex = i;
      }
    }
  });

  return (
    <div className={classes.root}>
      <Typography variant="h3">Personenbedarf</Typography>
      <Typography variant="body2">
        Hier können Sie definieren, wieviele Personen um welche Uhrzeit Sie für den gewählten Zeitraum
        benötigen.
      </Typography>

      {timePeriods.map((p) => (
        <Period key={p.startIndex} days={days.slice(p.startIndex, p.endIndex + 1)}></Period>
      ))}
    </div>
  );
};

export default Time;
