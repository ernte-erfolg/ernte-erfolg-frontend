export interface Workday {
  date: string;
  workingHours: number;
}
