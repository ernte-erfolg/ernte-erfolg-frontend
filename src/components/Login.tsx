import React, { useState, useEffect } from 'react';
import { TextField, Checkbox, FormControlLabel, Box, Button } from '@material-ui/core';
import { useHistory } from 'react-router-dom';
import { AccountAPI } from '../api';
import { LoadingButton, LoadingErrorDisplay } from '../components';
import { useSelector, useDispatch } from 'react-redux';
import { selectUserState, login } from '../slices/userSlice';

interface ILoginProps {
  onLoginComplete?: () => void;
  onRegisterClicked?: () => void;
}

const Login: React.FC<ILoginProps> = ({ onLoginComplete, onRegisterClicked }) => {
  const { loggedIn, user, farm, helper } = useSelector(selectUserState);
  const dispatch = useDispatch();
  const [error, setError] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [rememberMe, setRememberMe] = useState(false);
  const [loading, setLoading] = useState(false);
  const history = useHistory();

  const doLogin = async (ev: React.FormEvent) => {
    ev.preventDefault();
    if (!(email.length > 0 && password.length > 0)) {
      return;
    }
    try {
      setLoading(true);
      setError('');
      const response = await new AccountAPI().authorize({
        username: email.trim(),
        password,
        rememberMe,
      });
      localStorage.setItem('token', response.id_token);
      if (!(await dispatch(login()))) {
        throw new Error();
      }
    } catch (err) {
      setLoading(false);
      if (err && err.message && err.message === 'Unauthorized') {
        return setError('Dein Nutzer existiert nicht oder die E-Mail-Adresse ist noch nicht bestätigt.');
      }
      console.error('Error', err);
      return setError((err as Error).message || 'Ups, da ist leider etwas schief gegangen :/');
    }
  };

  useEffect(() => {
    if (!loggedIn) {
      return;
    }
    if (onLoginComplete) {
      onLoginComplete();
    } else if (user?.userClass === 'FARM') {
      if (farm) {
        history.push('/farm');
      } else {
        // user has not yet created his profile -> initial login
        history.push('/farm/edit-profile');
      }
    } else if (user?.userClass === 'HELPER') {
      if (helper) {
        history.push('/helping');
      } else {
        // user has not yet created his profile -> initial login
        history.push('/helping/edit-profile');
      }
    } else {
      setError(`Ungültige Nutzerklasse: ${user?.userClass}`);
    }
  }, [loggedIn, user, farm, history, helper, onLoginComplete]);

  const registerAsFarmer = () => {
    if (onRegisterClicked) {
      onRegisterClicked();
    }
    history.push('/farm/register');
  };

  const registerAsHelper = () => {
    if (onRegisterClicked) {
      onRegisterClicked();
    }
    history.push('/helping/register');
  };

  return (
    <>
      <form onSubmit={doLogin}>
        {(loading || error) && (
          <Box mb={3}>
            <LoadingErrorDisplay loading={loading} error={error} />
          </Box>
        )}
        <Box my={2}>
          <TextField
            id="email"
            label="E-Mail"
            variant="outlined"
            fullWidth
            value={email}
            onInput={(e) => setEmail((e.target as HTMLInputElement).value)}
          />
        </Box>
        <Box my={2}>
          <TextField
            id="password"
            label="Passwort"
            variant="outlined"
            fullWidth
            type="password"
            value={password}
            onInput={(e) => setPassword((e.target as HTMLInputElement).value)}
          />
        </Box>
        <Box my={2}>
          <FormControlLabel
            control={
              <Checkbox
                id="rememberMe"
                checked={rememberMe}
                onChange={(e) => setRememberMe((e.target as HTMLInputElement).checked)}
                name="rememberMe"
                color="primary"
              />
            }
            label="Merken"
          />
        </Box>
        <Box my={2}>
          <LoadingButton
            variant="contained"
            disabled={!(email.length > 0 && password.length > 0)}
            color="primary"
            loading={loading}
            type="submit"
            fullWidth
          >
            Anmelden
          </LoadingButton>
        </Box>
      </form>
      <Box mt={4}>
        <h3>Noch kein Mitglied?</h3>
        <Box my={2}>
          <Button variant="outlined" fullWidth onClick={registerAsHelper}>
            Als Helfer registrieren
          </Button>
        </Box>
        <Box my={2}>
          <Button variant="outlined" fullWidth onClick={registerAsFarmer}>
            Als Landwirt registrieren
          </Button>
        </Box>
      </Box>
    </>
  );
};

export default Login;
