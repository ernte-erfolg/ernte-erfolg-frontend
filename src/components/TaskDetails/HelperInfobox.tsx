import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { Box, Button } from '@material-ui/core';
import FavoriteIcon from '@material-ui/icons/Favorite';
import { Offer, Task } from '../../api';
import { selectUserState } from '../../slices/userSlice';
import Infobox from '../Infobox';
import HelperTimeDialog from '../HelperTimeDialog';
import LoginDialog from '../LoginDialog';

interface IHelperInfoboxProps {
  task: Task | null;
  offer: Offer | null;
  canHelp: boolean;
}

const HelperInfobox: React.FC<IHelperInfoboxProps> = ({ task, offer, canHelp }) => {
  const { helper, loggedIn } = useSelector(selectUserState);
  const history = useHistory();

  const [showLoginDialog, setShowLoginDialog] = useState(false);

  const offerHelp = () => {
    if (loggedIn) {
      history.push(`/helping/offer/${task!.id}`);
    } else {
      setShowLoginDialog(true);
    }
  }

  return (
    <Infobox title="Dein Hilfsangebot" icon={<FavoriteIcon />}>
      {helper && offer && (
        <HelperTimeDialog
          helper={helper}
          activator={
            <Box mt={2}>
              <u>Deine ausgewählten Zeiten sehen</u>
            </Box>
          }
          days={offer.guaranteedDays}
        />
      )}
      {task && !offer && (
        <Box>
          {<p>Du hast noch keine Hilfe für dieses Gesuch angeboten</p>}
          <Button
            variant="contained"
            size="large"
            color="primary"
            disabled={!canHelp}
            onClick={offerHelp}
          >
            Jetzt Hilfe anbieten
          </Button>
        </Box>
      )}
      <LoginDialog open={showLoginDialog} setOpen={setShowLoginDialog} />
    </Infobox>
  );
};

export default HelperInfobox;
