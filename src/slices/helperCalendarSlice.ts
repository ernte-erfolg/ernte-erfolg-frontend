import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { AppDispatch, RootState, AppGetState } from '../store';
import { Moment } from 'moment';
import { ICreateOfferWorkdayInArray, ICreateOfferWorkday, IBasicWorkday } from '../types';
import { convertWorkdaysDateToArray, convertDateArraysToWorkdays } from '../utils/workdayConverter';

interface SliceState {
  availableDays: ICreateOfferWorkdayInArray[];
  selectedDays: ICreateOfferWorkdayInArray[];
}

const initialState: SliceState = {
  availableDays: [],
  selectedDays: [],
};

export const helperCalendarSlice = createSlice({
  name: 'helperCalendar',
  initialState,
  reducers: {
    setAvailableDays: (state, action: PayloadAction<ICreateOfferWorkdayInArray[]>) => {
      state.availableDays = action.payload;
    },
    setDays: (state, action: PayloadAction<ICreateOfferWorkdayInArray[]>) => {
      state.selectedDays = action.payload;
    },
    resetHelperCalendarState: () => initialState,
  },
});

export const { setDays, setAvailableDays, resetHelperCalendarState } = helperCalendarSlice.actions;

export const setHelperAvailableWorkdays = (workdays: ICreateOfferWorkday[]) => (dispatch: AppDispatch) => {
  dispatch(setAvailableDays(convertWorkdaysDateToArray(workdays)));
};

export const addHelperSelectedWorkdays = (newWorkdays: IBasicWorkday[]) => async (
  dispatch: AppDispatch,
  getState: AppGetState
) => {
  const availableWorkdays: ICreateOfferWorkday[] = convertDateArraysToWorkdays(
    await getState().helperCalendar.availableDays
  );
  const currentlySelectedWorkdays: ICreateOfferWorkday[] = convertDateArraysToWorkdays(
    await getState().helperCalendar.selectedDays
  );

  const newSelectedWorkdays = newWorkdays.reduce((acc: ICreateOfferWorkday[], cur) => {
    const baseWorkday = availableWorkdays.find((day) => day.date.isSame(cur.date));
    baseWorkday &&
      acc.push({
        ...baseWorkday,
        selectedHours: baseWorkday.selectedHours > 0 ? 0 : baseWorkday.remainingHours >= 10 ? 10 : 5,
      });
    return acc;
  }, []);

  const combinedWorkDays = currentlySelectedWorkdays.concat(newSelectedWorkdays);
  const noDuplicatesWorkDays = combinedWorkDays.filter((v, i) => {
    return combinedWorkDays.findIndex((workday) => v.date.isSame(workday.date, 'day')) === i;
  });
  dispatch(setDays(convertWorkdaysDateToArray(noDuplicatesWorkDays)));
};

export const removeHelperWorkdays = (removedWorkdays: IBasicWorkday[]) => async (
  dispatch: AppDispatch,
  getState: AppGetState
) => {
  const currentlySelectedWorkdays: ICreateOfferWorkday[] = convertDateArraysToWorkdays(
    await getState().helperCalendar.selectedDays
  );
  const updatedWorkDays = currentlySelectedWorkdays.reduce((acc: ICreateOfferWorkday[], cur) => {
    const isDayToKeep = !removedWorkdays
      .map((removedDay) => removedDay.date.isSame(cur.date, 'day'))
      .includes(true);
    isDayToKeep && acc.push(cur);
    return acc;
  }, []);

  dispatch(setDays(convertWorkdaysDateToArray(updatedWorkDays)));
};

export const setHourOnSelectedDay = (selectedHours: number, selectedDay: Moment) => async (
  dispatch: AppDispatch,
  getState: AppGetState
) => {
  const currentlySelectedWorkdays: ICreateOfferWorkday[] = convertDateArraysToWorkdays(
    await getState().helperCalendar.selectedDays
  );
  const updatedSelectedWorkdays = currentlySelectedWorkdays.map((workday) => {
    const isSelectedDay = workday.date.isSame(selectedDay, 'day');
    const areAvailableHours = workday.remainingHours >= selectedHours;

    if (isSelectedDay && areAvailableHours) {
      return { ...workday, selectedHours };
    } else {
      return workday;
    }
  });

  dispatch(setDays(convertWorkdaysDateToArray(updatedSelectedWorkdays)));
};

export const selectHelperCalendarState = (state: RootState) => state.helperCalendar;
export const selectHelperAvailableDaysMoment = (state: RootState): ICreateOfferWorkday[] =>
  convertDateArraysToWorkdays(state.helperCalendar.availableDays);
export const selectHelperSelectedDaysMoment = (state: RootState): ICreateOfferWorkday[] =>
  convertDateArraysToWorkdays(state.helperCalendar.selectedDays);

export default helperCalendarSlice.reducer;
