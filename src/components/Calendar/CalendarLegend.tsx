import React from 'react';
import { Typography, makeStyles } from '@material-ui/core';
import CalendarDay from './CalendarDay';

const useStyles = makeStyles((theme) => ({
  container: {
    display: 'grid',
    gridTemplateColumns: '70px auto',
    gridGap: theme.spacing(3),
    alignItems: 'center',
  },
  preview: {
    width: '70px',
    height: '70px',
    gridColumn: '0 span 1',
  },
  description: {
    gridColumn: '1 span 1',
  },
}));

export default function CalendarLegend() {
  const classes = useStyles();
  return (
    <div className={classes.container}>
      <div className={classes.preview}>
        <CalendarDay day="27" selectable={false} />
      </div>
      <Typography className={classes.description} variant="subtitle2">
        An diesen Tagen wird noch viel Hilfe benötigt, sie sind aber noch nicht von Dir ausgewählt.
      </Typography>

      <div className={classes.preview}>
        <CalendarDay status="halfCovered" day="27" selectable={false} />
      </div>
      <Typography className={classes.description} variant="subtitle2">
        An diesen Tagen wird noch Hilfe benötigt, sie sind aber schon zu mind. 50% mit Helfern gedeckt.
      </Typography>

      <div className={classes.preview}>
        <CalendarDay selected day="27" selectable={false} />
      </div>
      <Typography className={classes.description} variant="subtitle2">
        An diesen Tagen hast du deine Hilfe ausgewählt und kannst sie im nächsten Schritt mit Stunden
        hinterlegen.
      </Typography>

      <div className={classes.preview}>
        <CalendarDay status="covered" day="27" selectable={false} />
      </div>
      <Typography className={classes.description} variant="subtitle2">
        Hier wird keine Hilfe benötigt.
      </Typography>
    </div>
  );
}
