import React, { useState } from 'react';
import moment, { Moment } from 'moment';
import { makeStyles } from '@material-ui/core/styles';
import CalendarDay from './CalendarDay';
import { useDispatch, useSelector } from 'react-redux';
import { addFarmWorkdays, selectFarmDaysMoment, removeFarmWorkdays } from '../../slices/farmCalendarSlice';
import { IBasicWorkday } from '../../types';
import { ClickAwayListener, Typography } from '@material-ui/core';
import {
  addHelperSelectedWorkdays,
  removeHelperWorkdays,
  selectHelperSelectedDaysMoment,
  selectHelperAvailableDaysMoment,
} from '../../slices/helperCalendarSlice';

const useStyles = makeStyles({
  month: {
    display: 'grid',
    'grid-template-columns': '1fr 1fr 1fr 1fr 1fr 1fr 1fr',
  },
  day: {
    'place-self': 'center',
    height: '3rem',
  },
  dayHeading: {
    'place-self': 'center',
    padding: '0.4rem',
  },
  selected: {
    border: '1px solid black',
    borderRadius: '1rem',
  },
  disabled: {
    color: 'gray',
    fontWeight: 100,
  },
  enabled: {
    cursor: 'pointer',
  },
  dayNumber: {
    width: '2rem',
    height: '2rem',
    padding: '0.4rem',
    textAlign: 'center',
  },
});

interface IMonthViewProps {
  today?: Moment;
  year: number;
  month: number;
  helper?: boolean;
}

interface IDay {
  isPlaceholder: boolean;
  day: Moment;
}

const MonthView: React.FC<IMonthViewProps> = ({ today, year, month, helper }) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const availableHelperDays = useSelector(selectHelperAvailableDaysMoment);

  const helperSelectedDays = useSelector(selectHelperSelectedDaysMoment);
  const farmSelectedDays = useSelector(selectFarmDaysMoment);
  const days: IBasicWorkday[] = helper ? helperSelectedDays : farmSelectedDays;

  const currentDay = today || moment();
  const firstDayOfMonth = moment([year, month]);
  const lastDayOfMonth = firstDayOfMonth.clone().endOf('month');

  // Days go from 1 - 7
  const monthStartOffset = firstDayOfMonth.isoWeekday() - 1;
  const monthEndOffset = lastDayOfMonth.isoWeekday() - 1;

  const daysInMonth = firstDayOfMonth.daysInMonth();

  const heading = ['Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa', 'So'];

  const daysArray: IDay[] = [];

  // placeholders before the month starts
  for (let i = 0; i < monthStartOffset; i++) {
    daysArray.push({
      isPlaceholder: true,
      day: firstDayOfMonth.clone().subtract(monthStartOffset - i, 'days'),
    });
  }

  // days in month
  for (let i = 0; i < daysInMonth; i++) {
    daysArray.push({ isPlaceholder: false, day: moment([year, month, i + 1]) });
  }

  // placeholders after the month ends
  for (let i = 0; i < 6 - monthEndOffset; i++) {
    daysArray.push({ isPlaceholder: true, day: lastDayOfMonth.clone().add(i + 1, 'days') });
  }

  const [tempSelectionStart, setTempSelectionStart] = useState<number | null>(null);
  const [tempSelectedDays, setTempSelectedDays] = useState<number[]>([]);

  const handleSelectionCancel = () => {
    if (tempSelectedDays.length) {
      setTempSelectedDays([]);
      setTempSelectionStart(null);
    }
  };

  const isSelectionStartSelected =
    !!tempSelectionStart &&
    !!days.find((day) => day.date.isSame(moment([year, month, tempSelectionStart]), 'day'));

  const tempDaysToBasicWorkDays = (tempDays: number[]): IBasicWorkday[] =>
    tempDays.map((day) => ({
      date: moment([year, month, day]),
    }));

  const enabledDays: Moment[] = availableHelperDays.filter((d) => d.remainingHours >= 5).map((d) => d.date);

  const halfCoveredDays: Moment[] = availableHelperDays
    .filter((d) => d.remainingHours <= d.requestedHours / 2 && d.remainingHours !== 0)
    .map((d) => d.date);

  const getSelectedDaysOfTheMonth = (start: number, end: number) => {
    const daysBetween = Array.from({ length: Math.abs(end - start) + 1 }, (_, i) => Math.min(start, end) + i);
    const availableDaysBetween = daysBetween.reduce((acc: number[], cur) => {
      const currentInDaysArray = daysArray.find((day) => day.day.isSame(moment([year, month, cur]), 'day'));

      const isDayAvailable =
        !!currentInDaysArray &&
        !currentInDaysArray.isPlaceholder &&
        currentInDaysArray.day.isSameOrAfter(currentDay);

      const isHelperDayAvailable = !!enabledDays.find((day) => day.isSame(currentInDaysArray?.day, 'day'));

      if (isDayAvailable && (!helper || isHelperDayAvailable)) {
        acc.push(cur);
      }

      return acc;
    }, []);
    return availableDaysBetween;
  };

  const renderDay = (d: IDay) => {
    const dayOfTheMonth = d.day.date();

    if (d.isPlaceholder) {
      return <span key={`day-${d.day.unix()}`} onPointerUp={handleSelectionCancel} />;
    }

    // Days not in the month are fillers
    const isEnabledDay =
      !helper || enabledDays.filter((enabledDay) => enabledDay.isSame(d.day, 'day')).length;
    const enabled = !d.isPlaceholder && d.day.isSameOrAfter(currentDay) && isEnabledDay;
    const isTempSelected = !!tempSelectedDays.find((selectedDay) => selectedDay === dayOfTheMonth);
    const isDaySelected = !!days.filter((selectedDay: IBasicWorkday) => selectedDay.date.isSame(d.day, 'day'))
      .length;

    const isHalfCovered =
      !!halfCoveredDays.length &&
      halfCoveredDays.find((halfCoveredDay) => halfCoveredDay.isSame(d.day, 'day'));

    const getStatus = () => {
      if (!enabled) return 'covered';
      if (isHalfCovered) return 'halfCovered';
      return 'needHelp';
    };

    const handleSelectionStart = () => {
      if (enabled) {
        setTempSelectionStart(dayOfTheMonth);
        setTempSelectedDays([dayOfTheMonth]);
      }
    };

    const handleSelection = () => {
      tempSelectionStart && setTempSelectedDays(getSelectedDaysOfTheMonth(tempSelectionStart, dayOfTheMonth));
    };

    const handleSelectionEnd = () => {
      helper
        ? dispatch(
            isSelectionStartSelected
              ? removeHelperWorkdays(tempDaysToBasicWorkDays(tempSelectedDays))
              : addHelperSelectedWorkdays(tempDaysToBasicWorkDays(tempSelectedDays))
          )
        : dispatch(
            isSelectionStartSelected
              ? removeFarmWorkdays(tempDaysToBasicWorkDays(tempSelectedDays))
              : addFarmWorkdays(tempDaysToBasicWorkDays(tempSelectedDays))
          );

      setTempSelectedDays([]);
      setTempSelectionStart(null);
    };

    return (
      <CalendarDay
        key={`day-${d.day.unix()}`}
        day={dayOfTheMonth.toString()}
        selected={isDaySelected}
        tempSelected={!isSelectionStartSelected && isTempSelected}
        tempUnselected={isSelectionStartSelected && isTempSelected}
        status={getStatus()}
        onMouseDown={handleSelectionStart}
        onMouseEnter={handleSelection}
        onMouseUp={handleSelectionEnd}
      />
    );
  };

  return (
    <>
      <div className={classes.month}>
        {heading.map((val) => (
          <div key={`day-heading-${val}`} className={classes.dayHeading}>
            <Typography variant="subtitle2">{val}</Typography>
          </div>
        ))}
      </div>

      <ClickAwayListener mouseEvent="onMouseUp" onClickAway={handleSelectionCancel}>
        <div className={classes.month}>{daysArray.map(renderDay)}</div>
      </ClickAwayListener>
    </>
  );
};

export default MonthView;
