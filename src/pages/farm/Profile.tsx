import React, { useState, useEffect } from 'react';
import { useParams, Link as RouterLink } from 'react-router-dom';
import { Grid, Typography, Link, Button, Box, makeStyles } from '@material-ui/core';
import moment from 'moment';
import { FarmAPI, Farm, Task, TaskAPI } from '../../api';
import {
  LoadingErrorDisplay,
  TaskList,
  ActionBar,
  DeleteButton,
  DeleteDialog,
  ProfilePicture,
} from '../../components';
import { useSelector } from 'react-redux';
import { selectUserState } from '../../slices/userSlice';
import { sortTasks } from '../../utils/taskUtils';
import { GreyBarPage } from '../../containers';

const useStyles = makeStyles((theme) => ({
  heroImage: {
    width: '100%',
    height: '420px',
    objectFit: 'cover',
  },
  greyBar: {
    [theme.breakpoints.down('sm')]: {
      textAlign: 'center',
    },
  },
}));

const Profile: React.FC = () => {
  const classes = useStyles();
  const { id } = useParams<{ id: string }>();
  const { user } = useSelector(selectUserState);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState('');
  const [tasksOfFarm, setTasksOfFarm] = useState<Task[]>([]);
  const [farm, setFarm] = useState<Farm | null>(null);

  useEffect(() => {
    (async () => {
      if (!id || !parseInt(id)) {
        setError('Ungültige ID');
        return;
      }
      try {
        setError('');
        setLoading(true);
        const farm = await new FarmAPI().getFarm(parseInt(id));
        const tasks = sortTasks(await new TaskAPI().getTasksByFarm(farm.id), 'firstDay', 'ascending');
        setFarm(farm);
        setTasksOfFarm(tasks);
      } catch (err) {
        setError((err as Error).message || 'Fehler beim Laden des Profils.');
        console.log(err);
      } finally {
        setLoading(false);
      }
    })();
  }, [id]);

  const isFarm = user?.userClass === 'FARM';

  const deleteAccount = async () => {
    // TODO: perform api call here
    console.log('Delete Account called');
  };

  let sortedTasks: Task[] = [];
  if (tasksOfFarm) {
    sortedTasks = tasksOfFarm.filter((t) =>
      t.requestedDays.some((d) => moment(d.date).isSameOrAfter(moment(), 'day'))
    );
    sortedTasks = sortTasks(sortedTasks, 'firstDay', 'ascending');
  }

  let actionBar;
  if (isFarm) {
    actionBar = (
      <ActionBar
        sticky
        leftActions={
          <DeleteDialog
            title="Account löschen"
            onDelete={deleteAccount}
            activator={
              <DeleteButton variant="outlined" size="large">
                Account löschen
              </DeleteButton>
            }
          >
            Wollen Sie ihren Account wirklich dauerhaft löschen? Alle Daten und Gesuche werden unwiderruflich
            gelöscht.
          </DeleteDialog>
        }
        rightActions={
          <Button
            component={RouterLink}
            to="/farm/edit-profile"
            size="large"
            variant="contained"
            color="primary"
          >
            Profil bearbeiten
          </Button>
        }
      />
    );
  }

  const greyBar = (
    <div className={classes.greyBar}>
      <Grid container spacing={4}>
        <Grid item xs={12} md={3}>
          <ProfilePicture url="/img/404_karotte.png" />
        </Grid>
        <Grid item xs={12} md={9}>
          <Box display="flex" alignItems="center" height="100%">
            <Box width="100%">
              <Box mb={6}>
                <Typography variant="h1">{(farm && farm.name) || ''}</Typography>
              </Box>
              <Grid container spacing={2}>
                <Grid item xs={12} md={6}>
                  <Box mb={2}>
                    <Typography variant="h4">Adressdaten</Typography>
                  </Box>
                  {farm && farm.street && farm.houseNr && farm.zip && farm.city && (
                    <Typography variant="body2">
                      {farm.street} {farm.houseNr}, {farm.zip} {farm.city}
                    </Typography>
                  )}
                </Grid>
                <Grid item xs={12} md={6}>
                  <Box mb={2}>
                    <Typography variant="h4">Kontaktdaten</Typography>
                  </Box>
                  {farm && (farm.contactMail || farm.phone) && (
                    <Typography variant="body2">
                      {farm.contactMail && (
                        <span>
                          E-Mail: <Link href={`mailto:${farm.contactMail}`}>{farm.contactMail}</Link>
                        </span>
                      )}
                      {farm.contactMail && farm.phone && <br />}
                      {farm.phone && (
                        <span>
                          Telefon: <Link href={`tel:${farm.phone}`}>{farm.phone}</Link>
                        </span>
                      )}
                    </Typography>
                  )}
                </Grid>
              </Grid>
            </Box>
          </Box>
        </Grid>
      </Grid>
    </div>
  );

  return (
    <Box mb={5}>
      <img className={classes.heroImage} src="/img/hero2.jpg" alt="Titelbild" />
      <GreyBarPage disableMarginTop greyBarContent={greyBar}>
        <Grid container spacing={6}>
          {(error || loading) && (
            <Grid item xs={12}>
              <LoadingErrorDisplay error={error} loading={loading} />
            </Grid>
          )}

          {farm && farm.description && (
            <Grid item xs={12}>
              <Box mb={2}>
                <Typography variant="h4">Beschreibung</Typography>
              </Box>
              <Typography variant="body2">{farm.description}</Typography>
            </Grid>
          )}

          <Grid item xs={12}>
            <TaskList
              title={
                <Box mb={2}>
                  <Typography variant="h2">Aktive Gesuche des Landwirts</Typography>
                </Box>
              }
              tasks={sortedTasks}
            />
          </Grid>
        </Grid>
        {actionBar}
      </GreyBarPage>
    </Box>
  );
};

export default Profile;
