import React from 'react';
import { makeStyles } from '@material-ui/core';
import c from 'classnames';

const useStyles = makeStyles((theme) => ({
  day: {
    position: 'relative',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    border: `1px solid ${theme.palette.grey[300]}`,
    backgroundColor: theme.palette.grey[100],
    height: '100%',
    '&::before': {
      content: '""',
      paddingBottom: '100%',
      display: 'inline-block',
      verticalAlign: 'top',
    },
    userSelect: 'none',
  },
  selectable: {
    cursor: 'pointer',
  },
  dayHalfCovered: {
    background: `linear-gradient(180deg, white 50%, ${theme.palette.grey[200]} 50%)`,
  },
  dayCovered: {
    color: theme.palette.action.disabled,
    backgroundColor: theme.palette.grey[50],
    cursor: 'default',
  },
  dayNumber: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    border: `1px solid transparent`,
    borderRadius: '55%',
    height: '55%',
    width: '55%',
    fontWeight: theme.typography.fontWeightBold,
  },
  selected: {
    backgroundColor: theme.palette.primary.main,
    color: theme.palette.common.white,
    border: `1px solid ${theme.palette.common.black}`,
  },
  tempSelected: {
    backgroundColor: theme.palette.grey[300],
    color: theme.palette.common.black,
    border: `1px solid ${theme.palette.grey[600]}`,
  },
  tempUnselected: {
    backgroundColor: theme.palette.grey[50],
    color: theme.palette.common.black,
    border: `1px solid ${theme.palette.grey[300]}`,
  },
}));

interface ICalendarDayProps {
  status?: 'needHelp' | 'halfCovered' | 'covered';
  selectable?: boolean;
  selected?: boolean;
  tempSelected?: boolean;
  tempUnselected?: boolean;
  day: string;
  onClick?: () => void;
  onMouseEnter?: () => void;
  onMouseDown?: () => void;
  onMouseUp?: () => void;
}

const CalendarDay: React.FC<ICalendarDayProps> = ({
  status = 'needHelp', // 'needHelp | halfCovered | covered'
  selectable = true,
  selected,
  tempSelected,
  tempUnselected,
  day,
  onClick,
  onMouseEnter,
  onMouseDown,
  onMouseUp,
  ...rest
}) => {
  const classes = useStyles();
  return (
    <div
      className={c([
        classes.day,
        status === 'halfCovered' && classes.dayHalfCovered,
        status === 'covered' && classes.dayCovered,
        selectable && classes.selectable,
      ])}
      onClick={onClick}
      onMouseEnter={onMouseEnter}
      onMouseDown={onMouseDown}
      onMouseUp={onMouseUp}
      {...rest}
    >
      <div
        className={c([
          classes.dayNumber,
          selected && classes.selected,
          tempSelected && classes.tempSelected,
          tempUnselected && classes.tempUnselected,
        ])}
      >
        {day}
      </div>
    </div>
  );
};

export default CalendarDay;
