import React from 'react';
import { Grid, Link, Box } from '@material-ui/core';
import { Alert } from '@material-ui/lab';
import FavoriteIcon from '@material-ui/icons/Favorite';
import { ContentContainer } from '../containers';
import { Login as LoginComponent, Infobox } from '../components';
import { SUPPORT_MAIL } from '../constants';

const Login: React.FC = () => {
  return (
    <ContentContainer>
      <Box mb={4}>
        <h1>Anmelden</h1>
      </Box>
      <Grid container>
        <Grid item xs={12} md={8} lg={6}>
          <Box mb={5}>
            <Alert severity="success">Deine E-Mail-Adresse wurde erfolgreich bestätigt.</Alert>
          </Box>
          <LoginComponent />
        </Grid>
        <Grid item xs={12} md={1} lg={3}>
          <Box my={4}></Box>
        </Grid>
        <Grid item xs={12} md={3}>
          <Infobox title="Schön, dass du da bist!" icon={<FavoriteIcon />}>
            <p>Deine E-Mail-Adresse wurde erfolgreich bestätigt. Du kannst dich nun direkt anmelden.</p>
            <p>
              Wenn Du Probleme beim Anmelden hast, schreib uns gerne an. Wir helfen Dir so schnell wie
              möglich: <Link href={`mailto:${SUPPORT_MAIL}`}>{SUPPORT_MAIL}</Link>
            </p>
          </Infobox>
        </Grid>
      </Grid>
    </ContentContainer>
  );
};

export default Login;
