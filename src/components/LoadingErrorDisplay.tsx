import React from 'react';
import { Box, LinearProgress } from '@material-ui/core';
import { Alert } from '@material-ui/lab';

interface ILoadingErrorDisplayProps {
  loading?: boolean;
  error?: string;
}

const LoadingErrorDisplay: React.FC<React.PropsWithChildren<ILoadingErrorDisplayProps>> = (props) => {
  return (
    <>
      {props.error && (
        <Box my={3}>
          <Alert severity="error" color="error">
            {props.error}
          </Alert>
        </Box>
      )}
      {props.loading && (
        <Box my={3}>
          <LinearProgress />
        </Box>
      )}
    </>
  );
};

export default LoadingErrorDisplay;
