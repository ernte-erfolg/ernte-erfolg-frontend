/// <reference types="cypress" />
/* global cy, Given, When, Then */
require('chai').should();
const axios = require('axios');

Given('I am not logged in', () => {
  cy.clearLocalStorage();
});

Given('I am on the {string} page', (path) => {
  cy.visit(path);
});

Given('I type {string} into the {string} field', (value, fieldName) => {
  cy.get(`input[id=${fieldName}], input[name=${fieldName}]`).type(value);
});

Given('I check {string}', (fieldName) => {
  cy.get(`input[id=${fieldName}][type="checkbox"], input[name=${fieldName}][type="checkbox"]`).check();
});

Given('I have no emails in my email inbox for {string}', async () => {
  await axios.delete(`${global.mailhogUrl}/api/v1/messages`);
});

When('I click on the {string} button', (buttonLabel) => {
  cy.contains(buttonLabel).click();
});

Then('I should see an error message', () => {
  cy.get('div.MuiAlert-root.MuiAlert-standardError');
});

Then('I should be on the {string} page', (path) => {
  cy.url().should('satisfy', (v) => v.endsWith(path));
});

Then('there should be a button {string}', (buttonLabel) => {
  cy.get('.MuiButton-root').contains(buttonLabel);
});

Then('I should get an email at {string}', async (emailAddress) => {
  // wait 5s to make sure the email arrived
  cy.wait(5000);
  cy.wrap(null).then(async () => {
    const messages = (await axios.get(`${global.mailhogUrl}/api/v1/messages`)).data;
    messages.should.have.length(1);
    const email = messages[0];
    email.Content.Headers.To.should.include(emailAddress);
  });
});
