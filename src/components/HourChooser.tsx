import React from 'react';
import { makeStyles } from '@material-ui/core';
import c from 'classnames';
import { useDispatch } from 'react-redux';
import { ICreateOfferWorkday } from '../types';
import { setHourOnSelectedDay } from '../slices/helperCalendarSlice';

const useStyles = makeStyles((theme) => ({
  container: {
    height: '70px',
    display: 'flex',
  },
  hour: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    fontWeight: theme.typography.fontWeightBold,
    fontSize: theme.typography.h6.fontSize,
    width: '100px',
    cursor: 'pointer',
    backgroundColor: theme.palette.grey[100],
    color: theme.palette.grey[700],
    border: '1px solid grey',
    userSelect: 'none',
  },
  selected: {
    border: '1px solid black',
    backgroundColor: theme.palette.primary.main,
    color: theme.palette.primary.contrastText,
  },
  disabled: {
    backgroundColor: theme.palette.grey[100],
    color: theme.palette.grey[300],
    cursor: 'unset',
  },
  noBorderLeft: {
    borderLeft: 'none',
  },
  noBorderRight: {
    borderRight: 'none',
  },
}));

const HourChooser: React.FC<{ day: ICreateOfferWorkday }> = ({
  day: { date, selectedHours, remainingHours },
}) => {
  const classes = useStyles();
  const dispatch = useDispatch();

  return (
    <div className={classes.container}>
      <div
        className={c(
          classes.hour,
          selectedHours !== 5 && classes.noBorderRight,
          selectedHours === 5 && classes.selected,
          remainingHours && remainingHours < 5 && classes.disabled
        )}
        onClick={() => dispatch(setHourOnSelectedDay(5, date))}
      >
        &#189; Tag
      </div>
      <div
        className={c(
          classes.hour,
          selectedHours !== 10 && classes.noBorderLeft,
          selectedHours === 10 && classes.selected,
          remainingHours && remainingHours < 10 && classes.disabled
        )}
        onClick={() => dispatch(setHourOnSelectedDay(10, date))}
      >
        1 Tag
      </div>
    </div>
  );
};

export default HourChooser;
