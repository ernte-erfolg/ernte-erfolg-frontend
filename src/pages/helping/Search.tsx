import React, { useState, useEffect } from 'react';
import { Box, Typography, TextField, Button } from '@material-ui/core';
import { useParams, useHistory } from 'react-router-dom';
import { TasksInArea, TaskAPI } from '../../api';
import { TaskList, LoadingErrorDisplay } from '../../components';
import { GreyBarPage } from '../../containers';
import { sortTasksByDistance, sortTasks } from '../../utils/taskUtils';

const Search: React.FC = () => {
  const { zip } = useParams();
  const history = useHistory();
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState('');
  const [searchResults, setSearchResults] = useState<TasksInArea[] | null>(null);
  const [inputZip, setInputZip] = useState('');

  useEffect(() => {
    (async () => {
      if (!zip) {
        return setError('Ungültige Postleitzahl');
      }
      try {
        setInputZip(zip);
        setError('');
        setLoading(true);
        setSearchResults(null);
        const results = await new TaskAPI().searchTasks(zip);
        results.sort((a, b) => a.maxDistance - b.maxDistance);
        setSearchResults(results);
      } catch (err) {
        console.log(err);
        setError((err as Error).message || 'Fehler bei der Suche.');
      } finally {
        setLoading(false);
      }
    })();
    // Do not put helper in here! It will result in the user being unable to change the zip in the input field
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [zip]);

  const search = (ev?: React.FormEvent) => {
    if (ev) {
      ev.preventDefault();
    }
    history.push(`/helping/search/${inputZip}`);
  };

  return (
    <GreyBarPage
      headline={<Typography variant="h1">Diese Landwirte brauchen Deine Hilfe</Typography>}
      greyBarContent={
        <form onSubmit={search}>
          <Box position="relative" display="flex">
            <TextField
              id="outlined-basic"
              label="PLZ"
              variant="outlined"
              value={inputZip}
              onInput={(ev) => setInputZip((ev.target as HTMLInputElement).value)}
            />
            <Box ml={3}>
              <Button variant="contained" color="primary" style={{ height: '100%' }} type="submit">
                Mehr Gesuche finden
              </Button>
            </Box>
          </Box>
        </form>
      }
    >
      <LoadingErrorDisplay error={error} loading={loading} />
      {searchResults && searchResults.length === 0 && (
        <Typography variant="body1">Leider konnten wir keine Gesuche in deiner Nähe finden.</Typography>
      )}
      {searchResults &&
        searchResults.map((sr, i) => (
          <Box key={i} my={6}>
            <TaskList title={`Gesuche innerhalb von ${sr.maxDistance} km`} tasks={sortTasksByDistance(sortTasks(sr.tasks, 'firstDay', 'ascending'), sr.distances)} />
          </Box>
        ))}
    </GreyBarPage>
  );
};

export default Search;
