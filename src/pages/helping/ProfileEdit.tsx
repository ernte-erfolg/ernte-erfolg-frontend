import React, { useState } from 'react';
import { Typography, Box, Grid } from '@material-ui/core';
import { useHistory } from 'react-router-dom';
import { useForm } from 'react-hook-form';
import { CreateHelper, Helper, HelperAPI, ValidationTypes } from '../../api';
import { DynamicGrid, ContentContainer } from '../../containers';
import { getFormTextField, LoadingButton, LoadingErrorDisplay } from '../../components';
import { useSelector, useDispatch } from 'react-redux';
import { selectUserState, setHelper } from '../../slices/userSlice';

export default function Profile() {
  const history = useHistory();
  const dispatch = useDispatch();
  const { helper } = useSelector(selectUserState);
  const { register, errors, formState, handleSubmit } = useForm<CreateHelper>({
    mode: 'onChange',
    defaultValues: {
      firstName: helper?.firstName,
      lastName: helper?.lastName,
      zip: helper?.zip,
      phone: helper?.phone,
    },
  });

  const [loading, setLoading] = useState(false);
  const [error, setError] = useState('');

  const save = async (profile: CreateHelper) => {
    try {
      setLoading(true);
      setError('');
      let updatedProfile: Helper;
      if (helper) {
        updatedProfile = await new HelperAPI().updateHelper({ ...helper, ...profile });
      } else {
        updatedProfile = await new HelperAPI().createHelper(profile);
      }
      dispatch(setHelper(updatedProfile));
      history.push(`/helping`);
    } catch (err) {
      console.log(err);
      setLoading(false);
      setError((err as Error).message || 'Fehler beim Speichern des Profils');
    }
  };

  return (
    <ContentContainer>
      <DynamicGrid mt>
        <Grid item xs={12}>
          <LoadingErrorDisplay error={error} loading={loading} />
          <Box mb={2}>
            <Typography variant="h4">Profil bearbeiten</Typography>
          </Box>
          <Box mb={2}>
            <Typography variant="body2">
              Hier kannst du dein Profil bearbeiten. Die darin angegebenen Daten können nur von den Betrieben
              eingesehen werden, denen du deine Hilfe angeboten hast.
            </Typography>
          </Box>
          <Box my={4}>
            <form onSubmit={handleSubmit(save)}>
              <Grid container spacing={3}>
                <Grid item xs={12} lg={6}>
                  {getFormTextField<CreateHelper>(
                    'firstName',
                    'Vorname',
                    register({ ...ValidationTypes.FirstName, required: true }),
                    errors,
                    { required: true }
                  )}
                </Grid>
                <Grid item xs={12} lg={6}>
                  {getFormTextField<CreateHelper>(
                    'lastName',
                    'Nachname',
                    register({ ...ValidationTypes.LastName, required: true }),
                    errors,
                    { required: true }
                  )}
                </Grid>
                <Grid item xs={12}>
                  {getFormTextField<CreateHelper>(
                    'zip',
                    'PLZ',
                    register({ ...ValidationTypes.Zip, required: true }),
                    errors,
                    { required: true }
                  )}
                </Grid>
                <Grid item xs={12}>
                  <Box mt={3}>
                    <Typography variant="h5">Kontaktdaten</Typography>
                    <p>Wie können die Betriebe dich erreichen?</p>
                  </Box>
                </Grid>
                <Grid item xs={12}>
                  {getFormTextField<CreateHelper>(
                    'phone',
                    'Telefonnummer (optional)',
                    register(ValidationTypes.Phone),
                    errors
                  )}
                </Grid>
                <Grid item xs={12}>
                  <Box display="flex" justifyContent="flex-end" mt={2}>
                    <LoadingButton
                      color="primary"
                      variant="contained"
                      type="submit"
                      disabled={!formState.isValid}
                      loading={loading}
                    >
                      Profil speichern
                    </LoadingButton>
                  </Box>
                </Grid>
              </Grid>
            </form>
          </Box>
        </Grid>
      </DynamicGrid>
    </ContentContainer>
  );
}
