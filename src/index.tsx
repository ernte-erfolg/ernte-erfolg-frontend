import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import TagManager from 'react-gtm-module';
import { ThemeProvider, CssBaseline, StylesProvider } from '@material-ui/core';
import moment from 'moment';
import * as serviceWorker from './serviceWorker';
import 'moment/locale/de';
import App from './App';
import store from './store';
import theme from './theme';
import './index.css';
import { classNameGenerator } from './utils/classNameGenerator';

if (window.location.hostname === 'ernte-erfolg.de') {
  TagManager.initialize({ gtmId: 'GTM-M4L8VSC' });
}

ReactDOM.render(
  <Provider store={store}>
    <StylesProvider generateClassName={classNameGenerator()}>
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <App />
      </ThemeProvider>
    </StylesProvider>
  </Provider>,
  document.getElementById('root')
);

moment.locale('de', {
  week: {
    dow: 1,
    doy: 4,
  },
});

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
