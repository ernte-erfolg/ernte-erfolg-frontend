Feature: Registration

  Scenario: Registration Dialog
    Given I am not logged in
    And I am on the "/" page
    When I click on the "Anmelden" button
    Then the login dialog should be open
    And there should be a button "Als Helfer registrieren"
    And there should be a button "Als Landwirt registrieren"

  Scenario Outline: Registration Email
    Given I have no emails in my email inbox for "<email>"
    And I am on the "<registrationPage>" page
    And I type "<email>" into the "email" field
    And I type "<password>" into the "password" field
    And I type "<password>" into the "repeatPassword" field
    And I check "confirmed"
    When I click on the "Jetzt anmelden" button
    Then I should get an email at "<email>"

    Examples:
    | registrationPage  | email                  | password |
    | /farm/register    | farm@ernte-erfolg.de   | 112211aA |
    | /helping/register | helper@ernte-erfolg.de | 112211aA |
