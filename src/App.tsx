import React, { useEffect, useState } from 'react';
import { Router, Route, useHistory, useLocation, Switch } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { Backdrop, CircularProgress } from '@material-ui/core';

import Layout from './containers/Layout';

import { selectUserState, login } from './slices/userSlice';
import LandingPage from './pages/LandingPage';
import Login from './pages/Login';
import Registration from './pages/Registration';
import TaskDetails from './pages/TaskDetails';
import MailConfirmation from './pages/MailConfirmation';
import Faq from './pages/Faq';
import Imprint from './pages/Imprint';
import Privacy from './pages/Privacy';
import Version from './pages/Version';
import NotFound from './pages/NotFound';

import NewTask from './pages/farm/task/NewTask';
import FarmDashboard from './pages/farm/Dashboard';
import FarmProfile from './pages/farm/Profile';
import FarmProfileEdit from './pages/farm/ProfileEdit';
import FarmConfirmDelete from './pages/farm/ConfirmDelete';

import HelperDashboard from './pages/helping/Dashboard';
import Search from './pages/helping/Search';
import NewOffer from './pages/helping/NewOffer';
import MyOffers from './pages/helping/MyOffers';
import HelperProfileEdit from './pages/helping/ProfileEdit';
import CookieBar from './components/CookieBar';
import customHistory from './customHistory';

/** When mounted, this component tries to login when mounted
 * While the login is performed, it renders a loading spinner.
 * Only afterwards it renders the child.
 * This is done to have a consistent state for the whole application.
 */
const LoginComponent: React.FC<React.PropsWithChildren<{}>> = ({ children }) => {
  const [initialUseEffectCall, setInitialUseEffectCall] = useState(true);
  const [initialLogin, setInitialLogin] = useState(true);
  const { loggingIn, farm, helper } = useSelector(selectUserState);
  const dispatch = useDispatch();

  const history = useHistory();
  const location = useLocation();

  useEffect(() => {
    (async () => {
      const hasBeenLoggedIn = await dispatch(login());
      if (!hasBeenLoggedIn) {
        setInitialLogin(false);
      }
    })();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    if (initialUseEffectCall) {
      setInitialUseEffectCall(false);
      return;
    }
    if (!loggingIn) {
      if (location.pathname === '/') {
        if (farm) {
          history.push('/farm');
        } else if (helper) {
          history.push('/helping');
        }
      }
      setInitialLogin(false);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [loggingIn]);
  return (
    <>
      {initialLogin && (
        <Backdrop open>
          <CircularProgress color="primary" />
        </Backdrop>
      )}
      {!initialLogin && children}
    </>
  );
};

const App: React.FC = () => {
  return (
    <Router history={customHistory}>
      <LoginComponent>
        <Layout>
          <Switch>
            {/* Static Pages */}
            <Route path="/" exact component={LandingPage} />
            <Route path="/faq" exact component={Faq} />
            <Route path="/imprint" exact component={Imprint} />
            <Route path="/privacy" exact component={Privacy} />

            {/* Misc */}
            <Route path="/login" exact component={Login} />
            <Route path="/confirm/:id" component={MailConfirmation} />
            <Route path="/task/:id" component={TaskDetails} />
            <Route path="/version" component={Version} />

            {/* Farm */}
            <Route path="/farm/register" render={() => <Registration profileType="FARM" />} />
            <Route path="/farm/task/create" component={NewTask} />
            <Route path="/farm/edit-profile" exact component={FarmProfileEdit} />
            <Route path="/farm/profile/:id" exact component={FarmProfile} />
            <Route path="/farm/confirmdelete/:key" exact component={FarmConfirmDelete} />
            <Route path="/farm" exact component={FarmDashboard} />

            {/* Helping */}
            <Route path="/helping/register" render={() => <Registration profileType="HELPER" />} />
            <Route path="/helping/offer/:taskId" component={NewOffer} />
            <Route path="/helping/my-offers" exact component={MyOffers} />
            <Route path="/helping/edit-profile" exact component={HelperProfileEdit} />
            <Route path="/helping/search/:zip" exact component={Search} />
            <Route path="/helping" exact component={HelperDashboard} />

            <Route path="*" component={NotFound} />
          </Switch>
        </Layout>
        <CookieBar />
      </LoginComponent>
    </Router>
  );
};

export default App;
