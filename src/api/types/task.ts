import { Workday } from "./workday";
import { Helper } from "./helper";

export interface CreateTask {
  title: string;
  taskDescription?: string;
  howToFindUs?: string;
  phone: string;
  email: string;
  street: string;
  houseNr: string;
  zip: string;
  city: string;
  requestedDays: Workday[];
}

export interface Task {
  id: number;
  farmId: number;
  farmName: string;
  title: string;
  taskDescription?: string;
  howToFindUs?: string;
  phone: string;
  email: string;
  street: string;
  houseNr: string;
  zip: string;
  city: string;
  lat: number;
  lon: number;
  active: boolean;
  requestedDays: Workday[];
  guaranteedDays: Workday[];
  guaranteedDaysByHelper?: Record<string, Workday[]>;
  helpers: Helper[];
  hiddenHelpers: boolean;
}

export interface TasksInArea {
  maxDistance: number;
  distances: Record<string, number>;
  tasks: Task[];
}
