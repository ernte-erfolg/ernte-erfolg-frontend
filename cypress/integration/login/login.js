/// <reference types="cypress" />
/* global cy, When, Then */
require('chai').should();

When('I click on the login button', () => {
  cy.get("div.MuiDialog-paper button").contains("Anmelden").click();
});

Then('I should be logged in as {string}', (profileName) => {
  cy.contains(profileName);
});
