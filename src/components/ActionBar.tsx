import React from 'react';
import { makeStyles, Box } from '@material-ui/core';
import c from 'classnames';

interface IActionBarProps {
  sticky?: boolean;
  topActions?: React.ReactNode;
  leftActions?: React.ReactNode;
  rightActions?: React.ReactNode;
}

const useStyles = makeStyles(
  (theme) => ({
    root: {
      marginTop: theme.spacing(3),
      paddingTop: theme.spacing(2),
      paddingBottom: theme.spacing(2),
      borderTop: '2px solid #3E3E3E',
      background: theme.palette.background.paper,
      zIndex: 10,
    },
    sticky: {
      position: 'sticky',
      bottom: 0,
    },
    spacer: {
      flexGrow: 1,
    },
  }),
  { name: 'ActionBar' }
);

const ActionBar: React.FC<IActionBarProps> = ({ sticky, topActions, leftActions, rightActions }) => {
  const classes = useStyles();
  return (
    <div className={c(classes.root, { [classes.sticky]: sticky })}>
      {topActions}
      <Box display="flex">
        {leftActions}
        <div className={classes.spacer} />
        {rightActions}
      </Box>
    </div>
  );
};

export default ActionBar;
