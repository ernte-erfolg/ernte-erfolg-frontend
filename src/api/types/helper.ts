export interface CreateHelper {
  firstName: string;
  lastName: string;
  phone?: string;
  zip: string;
}

export interface Helper {
  id: number;
  email: string;
  firstName: string;
  lastName: string;
  phone?: string;
  zip: string;
  lat?: number;
  lon?: number;
  level: number;
  levelProgress: number;
}
