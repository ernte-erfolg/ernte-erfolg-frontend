import React, { useState, useEffect } from 'react';
import { DynamicGrid } from '../containers';
import { Typography, Box, Button, Table, TableCell, TableRow, TableBody } from '@material-ui/core';
import moment from 'moment';
import { IBackendInfo, VersionAPI } from '../api';
import ContentContainer from '../containers/ContentContainer';

const Version: React.FC = () => {
  const [backendInfo, setBackendInfo] = useState<IBackendInfo | null>(null);
  const frontendVersion = process.env.REACT_APP_COMMIT_HASH;
  const frontendTags = process.env.REACT_APP_TAGS;
  const frontendBuildTime = parseInt(process.env.REACT_APP_BUILD_TIME ?? "0") * 1000;

  useEffect(() => {
    (async () => {
      setBackendInfo(await new VersionAPI().getVersion());
    })();
  }, []);

  const copy = () => {
    navigator.clipboard.writeText(`FE-${frontendVersion}/BE-${backendInfo?.git.commit.id.abbrev}`);
  };

  return (
    <ContentContainer>
      <DynamicGrid mt>
        <Typography variant="h1">Versionsinformationen</Typography>

        <Box my={3}>
          <Button variant="contained" color="primary" onClick={copy}>
            Kopieren
          </Button>
        </Box>

        <Box my={3}>
          <Typography variant="h5">Frontend</Typography>
          <Table>
            <TableBody>
              <TableRow>
                <TableCell>Commit</TableCell>
                <TableCell>{frontendVersion}</TableCell>
              </TableRow>
              <TableRow>
                <TableCell>Tags</TableCell>
                <TableCell>{frontendTags}</TableCell>
              </TableRow>
              <TableRow>
                <TableCell>Build-Zeitpunkt</TableCell>
                <TableCell>{frontendBuildTime > 0 ? moment(frontendBuildTime).calendar() : ""}</TableCell>
              </TableRow>
            </TableBody>
          </Table>
        </Box>

        <Box my={3}>
          <Typography variant="h5">Backend</Typography>
          {!!backendInfo && (
            <Table>
              <TableBody>
                <TableRow>
                  <TableCell>Commit</TableCell>
                  <TableCell>{backendInfo.git.commit.id.abbrev}</TableCell>
                </TableRow>
                <TableRow>
                  <TableCell>Branch</TableCell>
                  <TableCell>{backendInfo.git.branch}</TableCell>
                </TableRow>
                <TableRow>
                  <TableCell>Build-Zeitpunkt</TableCell>
                  <TableCell>{moment(backendInfo.build.time).calendar()}</TableCell>
                </TableRow>
                <TableRow>
                  <TableCell>Aktive Profile</TableCell>
                  <TableCell>{backendInfo.activeProfiles.join(', ')}</TableCell>
                </TableRow>
              </TableBody>
            </Table>
          )}
        </Box>
      </DynamicGrid>
    </ContentContainer>
  );
};

export default Version;
