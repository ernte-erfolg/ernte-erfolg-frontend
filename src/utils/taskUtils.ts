import { Task } from '../api';
import { hoursToHelpers } from './hourConversions';
import moment, { Moment } from 'moment';

export interface ITaskHourData {
  requestedHelpers: number;
  requestedHours: number;
  guaranteedHelpers: number;
  guaranteedHours: number;
}

export function getHoursFromTask(task: Pick<Task, 'requestedDays' | 'guaranteedDays'>): ITaskHourData {
  const requestedHours = task.requestedDays.reduce((p, c) => p + c.workingHours, 0);
  const guaranteedHours = task.guaranteedDays.reduce((p, c) => p + c.workingHours, 0);
  const requestedHelpers = hoursToHelpers(requestedHours);
  const guaranteedHelpers = hoursToHelpers(guaranteedHours);
  return { requestedHours, guaranteedHours, requestedHelpers, guaranteedHelpers };
}

export function getFirstDayOfTask(task: Pick<Task, 'requestedDays'>): Moment {
  return task.requestedDays.reduce<Moment|null>((p, c) => p && p.isBefore(moment(c.date)) ? p : moment(c.date), null) ?? moment();
}

export function getLastDayOfTask(task: Pick<Task, 'requestedDays'>): Moment {
  return task.requestedDays.reduce<Moment|null>((p, c) => p && p.isAfter(moment(c.date)) ? p : moment(c.date), null) ?? moment();
}

type SortDirection = 'ascending'|'descending';
function directionComparer(a: number, b: number, direction: SortDirection) {
  return direction === 'ascending' ? a - b : b - a;
}

export function sortTasks<T extends Pick<Task, 'requestedDays'>>(tasks: T[], sortBy: 'firstDay', direction: SortDirection = 'ascending'): T[] {
  switch (sortBy) {
    case 'firstDay':
      tasks.sort((a, b) => directionComparer(getFirstDayOfTask(a).unix(), getFirstDayOfTask(b).unix(), direction));
  }
  return tasks;
}

export function sortTasksByDistance<T extends Pick<Task, 'id'>>(tasks: T[], distances: { [taskId: string]: number }): T[] {
  tasks.sort((a, b) => distances[a.id.toString()] - distances[b.id.toString()]);
  return tasks;
}
