import React from 'react';
import { makeStyles, Box, LinearProgress, Typography, Grid, Button } from '@material-ui/core';
import ArrowBack from '@material-ui/icons/ArrowBack';

const useStyle = makeStyles({
  progress: {
    height: '40px',
  },
});

interface IStepPageProps {
  progress: number;
  title: string;
  titleHelper: string | React.ReactNode;
  rightSidebar?: React.ReactNode;
  onNextStepClick?: () => void;
  nextStepText?: string;
  nextStepDisabled?: boolean;
  backButton?: boolean;
  onBackButtonClick?: () => void;
}

const StepPage: React.FC<React.PropsWithChildren<IStepPageProps>> = ({
  progress,
  title,
  titleHelper,
  rightSidebar,
  onNextStepClick,
  nextStepText,
  nextStepDisabled,
  backButton,
  onBackButtonClick,
  children,
}) => {
  const classes = useStyle();
  return (
    <Box my={2}>
      <LinearProgress variant="determinate" value={progress} className={classes.progress} />
      <Box my={4}>
        <Box mb={2}>
          <Typography variant="h5">{title}</Typography>
        </Box>
        <Typography variant="body2" color="textSecondary">
          {titleHelper}
        </Typography>
      </Box>
      <Grid container spacing={8}>
        <Grid item xs={12} md={!!rightSidebar ? 8 : 12}>
          <Box>{children}</Box>
        </Grid>
        {rightSidebar && (
          <Grid item xs={12} md={4}>
            {rightSidebar}
          </Grid>
        )}
      </Grid>

      <Box mt={4}>
        <Box height="1px" borderBottom="1px solid grey" width="100%" my={2} />
        <Box display="flex" alignItems="center">
          <Box>
            {backButton && (
              <Button startIcon={<ArrowBack />} onClick={onBackButtonClick}>
                Zurück
              </Button>
            )}
          </Box>
          <Box ml="auto">
            {nextStepText && (
              <Button
                color="primary"
                variant="contained"
                size="large"
                onClick={onNextStepClick}
                disabled={nextStepDisabled}
              >
                {nextStepText}
              </Button>
            )}
          </Box>
        </Box>
      </Box>
    </Box>
  );
};

export default StepPage;
