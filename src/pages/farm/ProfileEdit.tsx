import React, { useState } from 'react';
import { Typography, Box, Grid, Button } from '@material-ui/core';
import { useHistory, Link } from 'react-router-dom';
import { useForm } from 'react-hook-form';
import { CreateFarm, FarmAPI, Farm, ValidationTypes } from '../../api';
import { getFormTextField, LoadingButton, LoadingErrorDisplay, ActionBar } from '../../components';
import { ContentContainer } from '../../containers';
import { useSelector, useDispatch } from 'react-redux';
import { selectUserState, setFarm } from '../../slices/userSlice';

export default function Profile() {
  const history = useHistory();
  const dispatch = useDispatch();
  const { user, farm } = useSelector(selectUserState);
  const { register, errors, formState, handleSubmit } = useForm<CreateFarm>({
    mode: 'onChange',
    defaultValues: {
      name: farm?.name,
      street: farm?.street,
      houseNr: farm?.houseNr,
      zip: farm?.zip,
      city: farm?.city,
      phone: farm?.phone,
      contactMail: farm?.contactMail ?? user?.email,
      description: farm?.description,
    },
  });

  const [loading, setLoading] = useState(false);
  const [error, setError] = useState('');

  const save = async (profile: CreateFarm) => {
    try {
      setLoading(true);
      setError('');
      let updatedProfile: Farm;
      profile.contactMail = profile.contactMail.trim();
      if (farm) {
        updatedProfile = await new FarmAPI().updateFarm({ ...farm, ...profile });
      } else {
        updatedProfile = await new FarmAPI().createFarm(profile);
      }
      dispatch(setFarm(updatedProfile));
      history.push(`/farm/profile/${updatedProfile.id}`);
    } catch (err) {
      console.log(err);
      setLoading(false);
      setError((err as Error).message || 'Fehler beim Speichern des Profils');
    }
  };

  return (
    <ContentContainer>
      <LoadingErrorDisplay error={error} loading={loading} />
      <Box mb={2}>
        <Typography variant="h1">Profil bearbeiten</Typography>
        <Typography variant="body2">
          Hier können Sie weitere Informationen zu Ihrem Profil eingeben. Dieses können potenzielle Helfer
          einsehen.
        </Typography>
      </Box>
      <Box my={4}>
        <form onSubmit={handleSubmit(save)}>
          <Grid container spacing={3}>
            <Grid item xs={12}>
              {getFormTextField<CreateFarm>(
                'name',
                'Name des Betriebs',
                register({ ...ValidationTypes.FarmName, required: true }),
                errors,
                { required: true }
              )}
            </Grid>
            <Grid item xs={12}>
              <Box mt={3}>
                <Typography variant="h3">Adresse des Profils</Typography>
              </Box>
            </Grid>
            <Grid item xs={9}>
              {getFormTextField<CreateFarm>(
                'street',
                'Straße',
                register({ ...ValidationTypes.Street, required: true }),
                errors,
                { required: true }
              )}
            </Grid>
            <Grid item xs={3}>
              {getFormTextField<CreateFarm>(
                'houseNr',
                'Nr.',
                register({ ...ValidationTypes.HouseNr, required: true }),
                errors,
                { required: true }
              )}
            </Grid>
            <Grid item xs={4}>
              {getFormTextField<CreateFarm>(
                'zip',
                'PLZ',
                register({ ...ValidationTypes.Zip, required: true }),
                errors,
                { required: true }
              )}
            </Grid>
            <Grid item xs={8}>
              {getFormTextField<CreateFarm>(
                'city',
                'Ort',
                register({ ...ValidationTypes.City, required: true }),
                errors,
                { required: true }
              )}
            </Grid>
            <Grid item xs={12}>
              <Box mt={3}>
                <Typography variant="h3">Kontaktdaten</Typography>
                <Typography variant="body2">Wie können die Helfer Sie notfalls erreichen?</Typography>
              </Box>
            </Grid>
            <Grid item xs={12}>
              {getFormTextField<CreateFarm>(
                'phone',
                'Telefonnummer',
                register({ ...ValidationTypes.Phone, required: true }),
                errors,
                { required: true }
              )}
            </Grid>
            <Grid item xs={12}>
              {getFormTextField<CreateFarm>(
                'contactMail',
                'E-Mail',
                register({ ...ValidationTypes.Email, required: true }),
                errors,
                { required: true, disabled: true }
              )}
            </Grid>
            <Grid item xs={12}>
              <Box mt={3}>
                <Typography variant="h3">Zusätzliche Informationen</Typography>
                <Typography variant="body2">
                  Weitere Informationen zu Ihrem Hof. Was sind Ihre Erzeugnisse? Welche Philosophie verfolgen
                  Sie?
                </Typography>
              </Box>
            </Grid>
            <Grid item xs={12}>
              {getFormTextField<CreateFarm>(
                'description',
                'Beschreibung',
                register(ValidationTypes.Description),
                errors,
                {
                  multiline: true,
                  rows: '6',
                }
              )}
            </Grid>
          </Grid>
        </form>
      </Box>
      <ActionBar
        sticky
        rightActions={
          <Box display="flex">
            <Button component={Link} to={`/farm/profile/${farm?.id}`} size="large" variant="outlined">
              Abbrechen
            </Button>
            <Box ml={2}>
              <LoadingButton
                size="large"
                color="primary"
                variant="contained"
                loading={loading}
                disabled={!formState.isValid}
                onClick={handleSubmit(save)}
              >
                Profil speichern
              </LoadingButton>
            </Box>
          </Box>
        }
      />
    </ContentContainer>
  );
}
