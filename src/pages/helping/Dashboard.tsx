import React, { useState, useEffect } from 'react';
import { Box, Typography, TextField, Button } from '@material-ui/core';
import { useHistory } from 'react-router-dom';
import { TaskAPI, Task, Offer, OfferAPI } from '../../api';
import { LoadingErrorDisplay, TaskList } from '../../components';
import { GreyBarPage } from '../../containers';
import { useSelector } from 'react-redux';
import { selectUserState } from '../../slices/userSlice';
import { sortTasks } from '../../utils/taskUtils';

const HelperDashboard: React.FC = () => {
  const history = useHistory();
  const { helper } = useSelector(selectUserState);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState('');
  const [tasksNearby, setTasksNearby] = useState<Task[] | null>(null);
  const [myOffers, setMyOffers] = useState<Offer[] | null>(null);
  const [inputZip, setInputZip] = useState('');

  useEffect(() => {
    (async () => {
      if (helper && helper.zip) {
        setInputZip(helper.zip);
        try {
          setError('');
          setLoading(true);

          // load nearby tasks
          const results = await new TaskAPI().searchTasks(helper.zip);
          if (results.length > 0) {
            results.sort((a, b) => a.maxDistance - b.maxDistance);
            setTasksNearby(results[0].tasks);
          } else {
            setTasksNearby(null);
          }

          // load offers
          const offers = await new OfferAPI().getOffers();
          setMyOffers(offers);
        } catch (err) {
          console.log(err);
          setError((err as Error).message || 'Fehler beim Laden');
        } finally {
          setLoading(false);
        }
      }
    })();
    // Do not put helper in here! It will result in the user being unable to change the zip in the input field
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const search = () => {
    history.push(`/helping/search/${inputZip}`);
  };

  return (
    <GreyBarPage
      headline={<Typography variant="h1">Diese Landwirte brauchen Deine Hilfe</Typography>}
      greyBarContent={
        <Box position="relative" display="flex">
          <TextField
            id="outlined-basic"
            label="PLZ"
            variant="outlined"
            value={inputZip}
            onInput={(ev) => setInputZip((ev.target as HTMLInputElement).value)}
          />
          <Box ml={3}>
            <Button variant="contained" color="primary" style={{ height: '100%' }} onClick={search}>
              Mehr Gesuche finden
            </Button>
          </Box>
        </Box>
      }
    >
      <LoadingErrorDisplay error={error} loading={loading} />

      <Box>
        <TaskList
          title="Gesuche in deiner Nähe"
          tasks={tasksNearby && sortTasks(tasksNearby, 'firstDay', 'ascending')}
          emptyText="Leider konnten wir keine Gesuche in deiner Nähe finden."
        />
      </Box>

      <Box mt={5}>
        <TaskList
          title="Meine Hilfsangebote"
          tasks={myOffers && sortTasks(myOffers.map((o) => o.task), 'firstDay', 'ascending')}
          emptyText="Du hast noch keine Hilfsangebote"
        />
      </Box>
    </GreyBarPage>
  );
};

export default HelperDashboard;
