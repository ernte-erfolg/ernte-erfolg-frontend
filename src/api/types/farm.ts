export interface CreateFarm {
  name: string;
  contactMail: string;
  phone: string;
  street: string;
  houseNr: string;
  zip: string;
  city: string;
  description?: string;
}

export interface Farm {
  id: number;
  name: string;
  contactMail: string;
  phone: string;
  street: string;
  houseNr: string;
  zip: string;
  city: string;
  description?: string;
  lat: number;
  lon: number;
}
