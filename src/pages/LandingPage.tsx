import React from 'react';
import { makeStyles, Button, Box, Grid, Typography, Container /*TextField*/ } from '@material-ui/core';
import { Link } from 'react-router-dom';

const useStyles = makeStyles((theme) => ({
  heroImage: {
    position: 'absolute',
    width: '100%',
    height: '100%',
    top: '0',
    objectFit: 'cover',
    zIndex: -1,
  },
  heroContainer: {
    position: 'relative',
  },
  hero1Container: {
    paddingTop: '50px',
    paddingBottom: '250px',
  },
  hero2Container: {
    paddingTop: '160px',
    paddingBottom: '160px',
  },
  headline: {
    marginBottom: '1em',
    fontWeight: 'bold',
  },
}));

export default function LandingPage() {
  const classes = useStyles();
  return (
    <Box mb={5}>
      <div className={`${classes.heroContainer} ${classes.hero1Container}`}>
        <div className={classes.heroImage}>
          <img style={{ width: '100%', height: '100%' }} src="/img/hero1.jpg" alt="Hero" />
        </div>
        <Container>
          <Grid container spacing={3}>
            <Grid container item xs={12} justify="flex-end">
              <Grid item xs={12} lg={3}>
                <img style={{ width: '340px' }} src="/img/wirvsviruslogo.png" alt="Wir vs Virus Logo"></img>
              </Grid>
            </Grid>
            <Grid container item xs={12}>
              <Grid item xs={12} lg={8}>
                <Typography className={classes.headline} variant="h1">
                  Wir verbinden Landwirte mit Erntehelfern zur Sicherung unserer Nahrungsmittel&shy;versorgung
                </Typography>
              </Grid>
            </Grid>
            <Grid container item xs={12} spacing={3}>
              <Grid item xs={12} sm={6} md={4} lg={3}>
                <Button
                  component={Link}
                  to="/farm/register"
                  size="large"
                  variant="contained"
                  color="primary"
                  fullWidth
                >
                  Als Betrieb anmelden
                </Button>
              </Grid>
              <Grid item xs={12} sm={6} md={4} lg={3}>
                <Button
                  component={Link}
                  to="/helping/register"
                  size="large"
                  variant="contained"
                  color="primary"
                  fullWidth
                >
                  Als Helfer anmelden
                </Button>
              </Grid>
            </Grid>
          </Grid>
        </Container>
      </div>

      <Container>
        <Box py={10}>
          <Grid container>
            <Grid item xs={12} md={8} lg={6}>
              <Typography variant="h1">Wir packen gemeinsam an</Typography>
              <p>
                Die Corona-Krise zwingt uns alle, unseren Alltag neu zu denken. Es gibt viele Probleme, die
                wir alle zusammen in den nächsten Wochen und Monaten lösen müssen – und mit Sicherheit zählen
                die Lebensmittelversorgung der Bevölkerung einerseits und Beschäftigungsmöglichkeiten
                andererseits zu den primären Herausforderungen. Wir von ErnteErfolg ermöglichen mit dieser
                Plattform in der Krisenzeit schnelle und einfache Unterstützung für die Landwirte und bieten
                gleichzeitig Arbeitssuchenden eine Beschäftigung. Damit die Ernten für dieses Jahr gesichert
                sind.
              </p>
            </Grid>
          </Grid>
        </Box>
      </Container>

      <div className={`${classes.heroContainer} ${classes.hero2Container}`}>
        <div className={classes.heroImage}>
          <img style={{ width: '100%', height: '100%' }} src="/img/hero2.jpg" alt="Hero" />
        </div>
        <Container>
          <Grid container justify="center">
            <Grid container item xs={12} md={10} lg={8} spacing={5}>
              <Grid item xs={12}>
                <Box color="white" textAlign="center">
                  <Typography variant="h1" style={{ color: 'white' }}>
                    Erkunde wo in deiner Nähe gerade deine Unterstützung benötigt wird
                  </Typography>
                </Box>
              </Grid>
              {/*<Grid item xs={12} lg={6}>
              <TextField variant="outlined" placeholder="PLZ" fullWidth></TextField>
            </Grid>
            <Grid item xs={12} lg={6}>
              <Button variant="contained" color="primary" fullWidth style={{ height: '100%' }}>
                Landwirte in deiner Nähe auflisten
              </Button>
  </Grid>*/}
              <Grid item xs={12}>
                <Box textAlign="center">
                  <Button
                    variant="contained"
                    color="primary"
                    component={Link}
                    to="/helping/register"
                    size="large"
                  >
                    Jetzt anmelden
                  </Button>
                </Box>
              </Grid>
            </Grid>
          </Grid>
        </Container>
      </div>

      {/* TODO <TaskList title="Gesuche mit dem höchsten Bedarf an Hilfe"></TaskList>*/}

    </Box>
  );
}
