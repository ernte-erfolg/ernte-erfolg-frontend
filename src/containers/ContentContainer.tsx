import React from 'react';
import { makeStyles, Container } from '@material-ui/core';

const useStyles = makeStyles(
  (theme) => ({
    root: {
      marginTop: theme.spacing(5),
      marginBottom: theme.spacing(5),
    },
  }),
  { name: 'ContentContainer' }
);

const ContentContainer: React.FC<React.PropsWithChildren<{}>> = ({ children }) => {
  const classes = useStyles();
  return <Container className={classes.root}>{children || {}}</Container>;
};

export default ContentContainer;
