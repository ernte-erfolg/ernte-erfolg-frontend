import { BaseAPI } from './base';
import { Helper, CreateHelper } from '../types';

export class HelperAPI extends BaseAPI {
  protected baseUrl = '/helpers';
  public createHelper = (helper: CreateHelper) => this.post<Helper>('/', helper);
  public updateHelper = (helper: CreateHelper) => this.put<Helper>('/', helper);
  public getHelper = (id: number) => this.get<Helper>(`/${id}`);
  public getHelperOfCurrentUser = () => this.get<Helper>('/byUserIsCurrentUser');
}
