import { makeStyles } from '@material-ui/core';

export interface INavigationProfileProps {
  name: string;
  avatarLetters: string;
  isFarm: boolean;
  onLogout: () => void;
}

export const useStyles = makeStyles((theme) => ({
  avatar: {
    width: '60px',
    height: '60px',
    backgroundColor: theme.palette.primary.main,
  },
  menuOpener: {
    cursor: 'pointer',
  },
  menuLink: {
    textDecoration: 'none',
  },
}));
