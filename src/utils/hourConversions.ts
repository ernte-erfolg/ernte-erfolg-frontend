import { HOURS_PER_HELPER_AND_DAY } from '../constants';

export function hoursToHelpers(hours: number) {
  return Math.ceil(hours / HOURS_PER_HELPER_AND_DAY);
}

export function helpersToHours(helpers: number) {
  return helpers * HOURS_PER_HELPER_AND_DAY;
}
