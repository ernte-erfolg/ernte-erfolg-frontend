import React from 'react';
import { Typography, Box, makeStyles } from '@material-ui/core';
import { DynamicGrid, ContentContainer } from '../containers';

const faqs = [
  {
    title: 'Wie funktioniert diese Plattform?',
    text:
      'ErnteErfolg verknüpft Erntehelfer und Landwirte, indem Helfer sich für Gesuche in ihrer Umgebung anmelden. Wir erhalten dafür keine Provision und es entstehen keinerlei Kosten für unsere Nutzer.',
  },
  {
    title: 'Muss ich für die Plattform etwas bezahlen?',
    text:
      'ErnteErfolg ist komplett kostenlos für unsere Ernte-Helfer*innen. Uns ist eine gemeinschaftliche Unterstützung der Landwirtschaft wichtiger als die Erzielung von Profit, daher arbeitet das Team hinter der Plattform größtenteils ehrenamtlich.',
  },
  {
    title: 'Welche Eigenschaften und Qualifikationen brauche ich?',
    text:
      'Jeder Einsatz hat unterschiedliche Voraussetzungen an Erfahrung und Qualifikationen. Die Landwirt*innen bringen dir alles Notwendige bei. Wenn du schon Vorerfahrung mitbringst, umso besser.',
  },
  {
    title: 'Welche Ausrüstung muss ich mitbringen?',
    text:
      'Ob und welche Ausrüstung du zu deinem jeweiligen Einsatz mitbringen musst, erfährst du in der Beschreibung des Gesuchs. Bitte denke an Wasser, Snacks, Wechselkleidung, festes Schuhwerk, Arbeitshandschuhe, Sonnenmilch und eine Sonnenhut.',
  },
  {
    title: 'Kann ich mich infizieren?',
    text:
      'Leider kann man sich momentan jederzeit und überall mit COVID-19 infizieren. Die Landwirte geben aber ihr Bestes, um die Ansteckungsgefahr so gering wie möglich zu halten.',
  },
  {
    title: 'Werde ich für meinen Einsatz als Erntehelfer bezahlt?',
    text: 'Die Bezahlung deines Einsatzes vereinbarst du mit dem jeweiligen Landwirt.',
  },
  {
    title: 'Wie komme ich zum Einsatzort bzw. zum Abholpunkt?',
    text: 'Die An- bzw. Abreise per Bahn, Auto oder Fahrrad solltest du im Regelfall selbst organisieren.',
  },
  {
    title: 'Werde ich vor Ort übernachten?',
    text:
      'Wenn dein Einsatz mehrere Tage dauert und der Weg zur täglichen An- und Abreise lang ist, dann stellt der Landwirt gewöhnlicherweise eine Unterkunft zur Verfügung. Genaue Infos erhältst du direkt vom Landwirt.',
  },
  {
    title: 'Gibt es bei meinem Einsatz Pausen?',
    text: 'Ob und wie lange du Pausen haben wirst, ist abhängig von deinem jeweiligen Einsatz.',
  },
  {
    title: 'Wie kann ich den Landwirt kontaktieren?',
    text:
      'Die Kontaktdaten deines Ansprechpartners findest du im Gesuch oder in der Bestätigungsmail, die du nach Anmeldung für ein Gesuch erhalten hast. Bitte kontaktiere ihn nur in dringenden Fällen, die meisten Landwirte haben aktuell alle Hände voll zu tun.',
  },
  {
    title: 'Was passiert wenn ich kurzfristig absagen muss, z.B. wegen Krankheit?',
    text:
      'Bitte wende dich umgehend bei dem jeweiligen Landwirt, falls sich deine Verfügbarkeit kurzfristig verändert. Beachte bitte, dass der Landwirt fest mit dir rechnet und deine Hilfe dringend braucht.',
  },
  {
    title: 'Übernimmt der Landwirt die Verpflegung oder muss ich ein Lunch Paket einpacken?',
    text:
      'Ob bei deinem Einsatz Verpflegung zur Verfügung gestellt wird, findest du in der Beschreibung des Gesuchs. Es schadet sicher nicht, einen Snack für zwischendurch einzupacken.',
  },
  {
    title: 'Wie kann ich mit dem Landwirt in Verbindung bleiben?',
    text:
      'Falls du mit dem Landwirt in Kontakt bleiben möchtest, solltet ihr nach dem Einsatz eure Kontaktdaten austauschen.',
  },
  {
    title: 'Wie und wo werden meine Daten gespeichert?',
    text:
      'Deine Daten werden bei der Registrierung in unserer Datenbank gespeichert und zum Zwecke der Vermittlung an Landwirt*innen weitergegeben. Weitere Informationen rund ums Thema Datenschutz erhältst du in unserer Datenschutzerklärung (s. u.).',
  },
];

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'grid',
    justifyContent: 'left',
  },
  icon: {
    gridColumn: '1 / 1',
    gridRow: '1 / 1',
    marginRight: theme.spacing(3),
  },
  title: {
    gridColumn: '2 / 2',
    gridRow: '1 / 1',
  },
  text: {
    gridColumn: '2 / 2',
    gridRow: '2 / 2',
  },
}));

const FaqEntry: React.FC<{ title: string; text: string }> = ({ title, text }) => {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <img className={classes.icon} src="/img/faq_symbol.svg" alt="Question mark symbol"></img>
      <Box className={classes.title} display="flex" alignItems="center">
        <Typography variant="h6">{title}</Typography>
      </Box>
      <p className={classes.text}>{text}</p>
    </div>
  );
};

const Faq: React.FC = () => {
  return (
    <ContentContainer>
      <DynamicGrid mt>
        <Typography variant="h4">Häufige Fragen & Antworten / FAQ</Typography>
        <p>
          Hier findet ihr häufig gestellte Fragen und Antworten. Zögert nicht, uns
          bei weiteren Fragen zu kontaktieren. Wir nehmen diese dann auch hier auf.
        </p>

        {faqs.map((f) => (
          <Box key={f.title} my={4}>
            <FaqEntry title={f.title} text={f.text} />
          </Box>
        ))}

        <Typography variant="h6">Kontaktdaten bei weiteren Fragen</Typography>
        <p>
          Du hast Fragen die hier noch nicht aufgenommen sind? Wende dich gerne über unser E-Mailadresse
          direkt an uns.
        </p>
      </DynamicGrid>
    </ContentContainer>
  );
};

export default Faq;
