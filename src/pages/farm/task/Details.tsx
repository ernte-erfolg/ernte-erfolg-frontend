import React from 'react';
import { Typography, Grid, Box } from '@material-ui/core';
import { getFormTextField, Infobox } from '../../../components';
import { CreateTask, ValidationTypes } from '../../../api';

function SectionHeadline(headlineText: string, descriptionText: string) {
  return (
    <Grid item xs={12}>
      <Box mt={3}>
        <Typography variant="h3">{headlineText}</Typography>
        <p>{descriptionText}</p>
      </Box>
    </Grid>
  );
}

type TaskDetails = Omit<CreateTask, 'farmId' | 'requestedDays'>;

interface IDetailsProps {
  register: any;
  errors: any;
}

const additionalInformationDescription =
  'Geben Sie Ihren Helfern eine genaue Beschreibung der Tätigkeit. Benötigen die Helfer Sprachkenntnisse, ' +
  'gibt es besondere Vorgaben zu den Fähigkeiten oder wünschen Sie bestimmte Erfahrungen. ' +
  'Was sollten die Helfer unbedingt wissen, damit sie Ihnen gut helfen können? ' +
  'Wollen Sie bereits über die Entlohnung Anreiz für die Mitarbeit schaffen?';

const Details: React.FC<IDetailsProps> = ({ register, errors }) => {
  return (
    <Grid container spacing={3}>
      <Grid item xs={12} lg={9}>
        <Grid container spacing={3}>
          <Grid item xs={12}>
            <Typography variant="h3">Details und Einsatzort</Typography>
          </Grid>

          <Grid item xs={12}>
            {getFormTextField<TaskDetails>(
              'title',
              'Titel des Projekts',
              register({ ...ValidationTypes.FarmTitle, required: true }),
              errors,
              { required: true }
            )}
          </Grid>

          {SectionHeadline('Kontaktdaten', 'Wie können die Helfer sie notfalls erreichen?')}

          <Grid item xs={12} lg={4}>
            {getFormTextField<TaskDetails>(
              'phone',
              'Telefon',
              register({ ...ValidationTypes.Phone, required: true }),
              errors,
              { required: true }
            )}
          </Grid>

          <Grid item xs={12} lg={8}>
            {getFormTextField<TaskDetails>(
              'email',
              'E-Mail',
              register({ ...ValidationTypes.Email, required: true }),
              errors,
              { required: true, disabled: true }
            )}
          </Grid>

          {SectionHeadline('Ausführliche Beschreibung zum Gesuch', additionalInformationDescription)}

          <Grid item xs={12}>
            {getFormTextField<TaskDetails>(
              'taskDescription',
              'Beschreibung',
              register(ValidationTypes.Description),
              errors,
              { multiline: true, rows: '6' }
            )}
          </Grid>

          {SectionHeadline('Einsatzort', 'Wo sollen die Helfer arbeiten?')}

          <Grid item xs={10}>
            {getFormTextField<TaskDetails>(
              'street',
              'Straße',
              register({ ...ValidationTypes.Street, required: true }),
              errors,
              { required: true }
            )}
          </Grid>
          <Grid item xs={2}>
            {getFormTextField<TaskDetails>(
              'houseNr',
              'Nr.',
              register({ ...ValidationTypes.HouseNr, required: true }),
              errors,
              { required: true }
            )}
          </Grid>

          <Grid item xs={4}>
            {getFormTextField<TaskDetails>(
              'zip',
              'PLZ',
              register({ ...ValidationTypes.Zip, required: true }),
              errors,
              { required: true }
            )}
          </Grid>
          <Grid item xs={8}>
            {getFormTextField<TaskDetails>(
              'city',
              'Ort',
              register({ ...ValidationTypes.City, required: true }),
              errors,
              { required: true }
            )}
          </Grid>

          <Grid item xs={12}>
            <Box mt={2}>
              Gibt es weitere Hinweise zum Einsatzort? Z. B. den Anschluss öffentlicher Verkehrsmittel oder
              Mitfahrgelegenheiten usw. oder sogar eine Übernachtungsmöglichkeit für die Helfer?
            </Box>
          </Grid>

          <Grid item xs={12}>
            {getFormTextField<TaskDetails>(
              'howToFindUs',
              'Beschreibung des Einsatzorts',
              register(ValidationTypes.Description),
              errors,
              { multiline: true, rows: '4' }
            )}
          </Grid>
        </Grid>
      </Grid>
      <Grid item xs={12} lg={3}>
        <Infobox title="Info">
          <span>
            Was ist für Ihre Helfer wichtig? Hier eine kleine Checkliste:
            <ul>
              <li>Um wieviel Uhr geht es los?</li>
              <li>Von wann bis wann wird gearbeitet?</li>
              <li>Wie wird die Arbeit entlohnt?</li>
              <li>Wird für die Helfer-Verpflegung gesorgt?</li>
              <li>Gibt es besondere Anreize, auf die sich Ihre Helfer freuen können?</li>
            </ul>
          </span>
        </Infobox>
      </Grid>
    </Grid>
  );
};

export default Details;
