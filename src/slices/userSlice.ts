import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { decode } from 'jsonwebtoken';
import { User, Farm, Helper, AccountAPI, FarmAPI, HelperAPI } from '../api';
import { AppDispatch, RootState, AppThunk } from '../store';

interface SliceState {
  loggingIn: boolean;
  loggedIn: boolean;
  user: User | null;
  farm: Farm | null;
  helper: Helper | null;
}

const initialState: SliceState = {
  loggingIn: false,
  loggedIn: false,
  user: null,
  farm: null,
  helper: null,
};

export const userSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {
    setLoggingIn: (state, action: PayloadAction<boolean>) => {
      state.loggingIn = action.payload;
    },
    setLoggedIn: (state, action: PayloadAction<boolean>) => {
      state.loggedIn = action.payload;
    },
    setUser: (state, action: PayloadAction<User | null>) => {
      state.user = action.payload;
    },
    setFarm: (state, action: PayloadAction<Farm | null>) => {
      state.farm = action.payload;
    },
    setHelper: (state, action: PayloadAction<Helper | null>) => {
      state.helper = action.payload;
    },
    resetUserState: () => initialState,
  },
});

export const { setLoggingIn, setLoggedIn, setUser, setFarm, setHelper, resetUserState } = userSlice.actions;

export const logout = () => (dispatch: AppDispatch) => {
  localStorage.removeItem('token');
  dispatch(resetUserState());
};

export const login = (): AppThunk => async (dispatch: AppDispatch) => {
  dispatch(setLoggingIn(true));
  if (!localStorage.getItem('token')) {
    dispatch(setLoggedIn(false));
    return false;
  }
  try {
    const { exp } = decode(localStorage.getItem('token')!) as Record<string, any>;
    if (Date.now() >= (exp - 3600) * 1000) {
      // ER-478: we already remove the token an hour before it expires.
      // This is to reduce the risk of the token expiring mid-session which could lead to bad UX.
      // Example: User fills out a form, meanwhile token expires -> all data is lost
      throw new Error('expired');
    }
  } catch (err) {
    localStorage.removeItem('token');
    return false;
  }
  const accountAPI = new AccountAPI();

  try {
    const user = await accountAPI.getAccount();
    dispatch(setUser(user));

    if (user.userClass === 'FARM') {
      const farm = await new FarmAPI().getFarmOfCurrentUser();
      dispatch(setFarm(farm));
    } else if (user.userClass === 'HELPER') {
      const helper = await new HelperAPI().getHelperOfCurrentUser();
      dispatch(setHelper(helper));
    }
  } catch (err) {
    console.error('userSlice - unhandled error');
  }

  dispatch(setLoggedIn(true));
  dispatch(setLoggingIn(false));

  return true;
};

export const selectUserState = (state: RootState) => state.user;

export default userSlice.reducer;
