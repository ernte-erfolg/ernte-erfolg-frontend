import React from 'react';
import moment, { Moment } from 'moment';
import { Box, Checkbox, Typography, FormControlLabel } from '@material-ui/core';
import { CalendarDay, HourChooser } from '../../../components';
import { ICreateOfferWorkday } from '../../../types';

interface ISingleMonthProps {
  month: Moment;
  days: ICreateOfferWorkday[];
}

const SingleMonth: React.FC<ISingleMonthProps> = ({ month, days }) => {
  const filteredDays = days.filter((d) => d.date.isSame(month, 'month') && d.selectedHours > 0);
  filteredDays.sort((a, b) => a.date.unix() - b.date.unix());

  return (
    <Box>
      <Typography variant="h3">{month.format('MMMM YYYY')}</Typography>
      <Box display="flex" mt={3} mb={1}>
        <Box width="70px">
          <Typography variant="body2">Tag</Typography>
        </Box>
        <Box ml={4}>
          <Typography variant="body2">Deine Stunden</Typography>
        </Box>
      </Box>
      <Box display="flex" flexDirection="column">
        {filteredDays.map((day) => (
          <Box display="flex" key={day.date.toString()}>
            <Box height="70px" width="70px" mb={1}>
              <CalendarDay day={day.date.format('DD')} selected selectable={false} />
            </Box>
            <Box display="flex" ml={4}>
              <HourChooser day={day} />
            </Box>
          </Box>
        ))}
      </Box>
    </Box>
  );
};

interface ISelectHoursProps {
  days: ICreateOfferWorkday[];
  confirmed: boolean;
  setConfirmed: (v: boolean) => void;
}

const SelectHours: React.FC<ISelectHoursProps> = ({ days, confirmed, setConfirmed }) => {
  const months: Moment[] = [];
  for (const rd of days) {
    if (rd.selectedHours > 0 && !months.find((m) => m.isSame(rd.date, 'month'))) {
      months.push(moment(rd.date));
    }
  }
  months.sort((a, b) => a.unix() - b.unix());

  return (
    <Box>
      {months.map((m) => (
        <Box my={6} key={m.unix()}>
          <SingleMonth month={m} days={days} />
        </Box>
      ))}
      <h2>Bestätigen</h2>
      <p>Bitte bestätige deinen gewählten Zeitraum und Stunden.</p>
      <FormControlLabel
        control={<Checkbox color="primary" checked={confirmed} onClick={() => setConfirmed(!confirmed)} />}
        label={
          'Hiermit bestätige ich, zu meinen angegebenen Zeiten als Landwirtschaftshelfer zu arbeiten. ' +
          'Mir ist bewusst, dass mit Absenden dieser Unterstützung der Landwirt fest mit mir rechnet.'
        }
      />
    </Box>
  );
};

export default SelectHours;
