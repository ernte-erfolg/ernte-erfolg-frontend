import React, { useState } from 'react';
import { Helper, Workday } from '../api';
import {
  Box,
  useTheme,
  useMediaQuery,
  makeStyles,
  Dialog,
  DialogTitle,
  DialogContent,
  Avatar,
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  DialogActions,
  Button,
} from '@material-ui/core';
import PhoneIcon from '@material-ui/icons/Phone';
import EmailIcon from '@material-ui/icons/Email';
import { dateFromDto } from '../utils/workdayConverter';
import moment from 'moment';

interface IHelperTimeDialogProps {
  helper: Helper;
  days: Workday[];
  open?: boolean;
  setOpen?: (v: boolean) => void;
  activator?: React.ReactNode;
}

const useStyles = makeStyles((theme) => ({
  activator: {
    cursor: 'pointer',
  },
  icon: {
    backgroundColor: theme.palette.primary.main,
    marginRight: theme.spacing(1),
  },
}));

const HelperTimeDialog: React.FC<IHelperTimeDialogProps> = (props) => {
  const theme = useTheme();
  const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));
  const classes = useStyles();
  const name = `${props.helper.firstName} ${props.helper.lastName}`;
  const [open, setOpen] = useState(false);

  const openSetter = (v: boolean) => {
    if (props.setOpen) {
      props.setOpen(v);
    } else {
      setOpen(v);
    }
  };

  const openState = typeof props.open === 'boolean' ? props.open : open;
  const sortedDays = props.days.slice();
  sortedDays.sort((a, b) => moment(a.date).unix() - moment(b.date).unix());

  return (
    <>
      {props.activator && (
        <Box className={classes.activator} onClick={() => openSetter(true)}>
          {props.activator}
        </Box>
      )}
      <Dialog
        fullScreen={fullScreen}
        open={openState}
        fullWidth
        maxWidth="sm"
        onClose={() => openSetter(false)}
      >
        <DialogTitle>{name}</DialogTitle>
        <DialogContent>
          <Box my={2}>
            {props.helper.phone && (
              <a href={`tel:${props.helper.phone}`}>
                <Box mb={2} display="flex" alignItems="center">
                  <Avatar className={classes.icon}>
                    <PhoneIcon />
                  </Avatar>
                  {props.helper.phone}
                </Box>
              </a>
            )}
            {props.helper.email && (
              <a href={`mailto:${props.helper.email}`}>
                <Box mb={2} display="flex" alignItems="center">
                  <Avatar className={classes.icon}>
                    <EmailIcon />
                  </Avatar>
                  {props.helper.email}
                </Box>
              </a>
            )}
          </Box>
          <Box my={2}>
            <Table>
              <TableHead>
                <TableRow>
                  <TableCell>Datum</TableCell>
                  <TableCell>Stunden</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {sortedDays.map((d) => (
                  <TableRow key={d.date}>
                    <TableCell>{dateFromDto(d).format('DD. MMMM YYYY')}</TableCell>
                    <TableCell>{d.workingHours}h</TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </Box>
        </DialogContent>
        <DialogActions>
          <Button autoFocus onClick={() => openSetter(false)}>
            zurück
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
};

export default HelperTimeDialog;
