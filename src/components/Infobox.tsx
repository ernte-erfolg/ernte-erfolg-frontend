import React from 'react';
import { makeStyles, Typography, Box } from '@material-ui/core';

interface IInfoboxProps {
  icon?: React.ReactNode;
  title: string;
}

const useStyles = makeStyles(
  (theme) => ({
    root: {
      border: '1px solid #9C9C9C',
      padding: theme.spacing(4),
    },
    icon: {
      color: theme.palette.primary.main,
      marginBottom: theme.spacing(1),
    },
  }),
  { name: 'Infobox' }
);

const Infobox: React.FC<React.PropsWithChildren<IInfoboxProps>> = ({ icon, title, children }) => {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      {icon && <div className={classes.icon}>{icon}</div>}
      <Typography variant="h4">{title}</Typography>
      {children && <Box mt={1}>{children}</Box>}
    </div>
  );
};

export default Infobox;
