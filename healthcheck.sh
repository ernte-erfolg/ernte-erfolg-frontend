#!/bin/sh
set -e

# Disable health check during development
test "${BUILD_ENV}" == "local" && exit 0

# Check if nginx can be reached
curl --noproxy 127.0.0.1 --max-time 2 --fail http://127.0.0.1:8080 || ( echo "frontend healthcheck failed" && exit 1 )
