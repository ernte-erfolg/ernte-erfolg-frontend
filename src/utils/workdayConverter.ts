import { Workday } from '../api';
import { IWorkday, IBasicWorkdayInArray, IBasicWorkday } from '../types';
import moment, { Moment } from 'moment';

export function dateFromDto(dto: Pick<Workday, 'date'>): Moment {
  return moment(dto.date);
}

export function dateToDateString(date: Moment): string {
  return date.format('YYYY-MM-DD');
}

export function workdayToDto(workday: IWorkday): Workday {
  return {
    date: dateToDateString(workday.date),
    workingHours: workday.hours,
  };
}

export function dtoToWorkday(dto: Pick<Workday, 'date' | 'workingHours'>): IWorkday {
  return {
    date: dateFromDto(dto),
    hours: dto.workingHours,
  };
}

export const convertWorkdaysDateToArray = <T extends IBasicWorkday, U extends IBasicWorkdayInArray>(
  workdays: T[]
): U[] =>
  workdays
    .sort((a, b) => a.date.unix() - b.date.unix())
    .map((day) => {
      const date = day.date;
      const dayUnknown: unknown = day;
      const dayCasted: U = dayUnknown as U;
      return { ...dayCasted, date: date.toArray() };
    });

export const convertDateArraysToWorkdays = <T extends IBasicWorkdayInArray, U extends IBasicWorkday>(
  workdays: T[]
): U[] =>
  workdays
    .map((day) => {
      const date = day.date;
      const dayUnknown: unknown = day;
      const dayCasted: U = dayUnknown as U;
      return { ...dayCasted, date: moment(date) };
    })
    .sort((a, b) => a.date.unix() - b.date.unix());
