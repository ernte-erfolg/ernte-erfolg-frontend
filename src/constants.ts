let base_url = window.location.origin + '/api';

export const BASE_URL = base_url;
export const HOURS_PER_HELPER_AND_DAY = 10;
export const SUPPORT_MAIL = 'support@ernte-erfolg.de';
