import React from 'react';
import ContentContainer from '../containers/ContentContainer';

export default function Privacy() {
  return (
    <ContentContainer>
      <h2>Datenschutzerklärung</h2>
      <p>
        Personenbezogene Daten (nachfolgend zumeist nur „Daten“ genannt) werden von uns nur im Rahmen der
        Erforderlichkeit sowie zum Zwecke der Bereitstellung eines funktionsfähigen und nutzerfreundlichen
        Internetauftritts, inklusive seiner Inhalte und der dort angebotenen Leistungen, verarbeitet.
      </p>
      <p>
        Gemäß Art. 4 Ziffer 1. der Verordnung (EU) 2016/679, also der Datenschutz-Grundverordnung (nachfolgend
        nur „DSGVO“ genannt), gilt als „Verarbeitung“ jeder mit oder ohne Hilfe automatisierter Verfahren
        ausgeführter Vorgang oder jede solche Vorgangsreihe im Zusammenhang mit personenbezogenen Daten, wie
        das Erheben, das Erfassen, die Organisation, das Ordnen, die Speicherung, die Anpassung oder
        Veränderung, das Auslesen, das Abfragen, die Verwendung, die Offenlegung durch Übermittlung,
        Verbreitung oder eine andere Form der Bereitstellung, den Abgleich oder die Verknüpfung, die
        Einschränkung, das Löschen oder die Vernichtung.
      </p>
      <p>
        Mit der nachfolgenden Datenschutzerklärung informieren wir Sie insbesondere über Art, Umfang, Zweck,
        Dauer und Rechtsgrundlage der Verarbeitung personenbezogener Daten, soweit wir entweder allein oder
        gemeinsam mit anderen über die Zwecke und Mittel der Verarbeitung entscheiden. Zudem informieren wir
        Sie nachfolgend über die von uns zu Optimierungszwecken sowie zur Steigerung der Nutzungsqualität
        eingesetzten Fremdkomponenten, soweit hierdurch Dritte Daten in wiederum eigener Verantwortung
        verarbeiten.
      </p>
      <p>Unsere Datenschutzerklärung ist wie folgt gegliedert:</p>
      <p>
        I. Informationen über uns als Verantwortliche
        <br />
        II. Rechte der Nutzer und Betroffenen
        <br />
        III. Informationen zur Datenverarbeitung
      </p>
      <h3>I. Informationen über uns als Verantwortliche</h3>
      <p>Verantwortlicher Anbieter dieses Internetauftritts im datenschutzrechtlichen Sinne ist:</p>
      <p>
        <span>
          Alexander Rüffer
          <br />
          Züricherstraße 124
          <br />
          81476 München
        </span>
      </p>
      <p>
        <span>
          Telefon: +49 89 416173590
          <br />
          E-Mail: kontakt@ernte-erfolg.de
        </span>
      </p>
      <h3>II. Rechte der Nutzer und Betroffenen</h3>
      <p>
        Mit Blick auf die nachfolgend noch näher beschriebene Datenverarbeitung haben die Nutzer und
        Betroffenen das Recht
      </p>
      <ul>
        <li>
          auf Bestätigung, ob sie betreffende Daten verarbeitet werden, auf Auskunft über die verarbeiteten
          Daten, auf weitere Informationen über die Datenverarbeitung sowie auf Kopien der Daten (vgl. auch
          Art. 15 DSGVO);
        </li>
        <li>
          auf Berichtigung oder Vervollständigung unrichtiger bzw. unvollständiger Daten (vgl. auch Art. 16
          DSGVO);
        </li>
        <li>
          auf unverzügliche Löschung der sie betreffenden Daten (vgl. auch Art. 17 DSGVO), oder, alternativ,
          soweit eine weitere Verarbeitung gemäß Art. 17 Abs. 3 DSGVO erforderlich ist, auf Einschränkung der
          Verarbeitung nach Maßgabe von Art. 18 DSGVO;
        </li>
        <li>
          auf Erhalt der sie betreffenden und von ihnen bereitgestellten Daten und auf Übermittlung dieser
          Daten an andere Anbieter/Verantwortliche (vgl. auch Art. 20 DSGVO);
        </li>
        <li>
          auf Beschwerde gegenüber der Aufsichtsbehörde, sofern sie der Ansicht sind, dass die sie
          betreffenden Daten durch den Anbieter unter Verstoß gegen datenschutzrechtliche Bestimmungen
          verarbeitet werden (vgl. auch Art. 77 DSGVO).
        </li>
      </ul>
      <p>
        Darüber hinaus ist der Anbieter dazu verpflichtet, alle Empfänger, denen gegenüber Daten durch den
        Anbieter offengelegt worden sind, über jedwede Berichtigung oder Löschung von Daten oder die
        Einschränkung der Verarbeitung, die aufgrund der Artikel 16, 17 Abs. 1, 18 DSGVO erfolgt, zu
        unterrichten. Diese Verpflichtung besteht jedoch nicht, soweit diese Mitteilung unmöglich oder mit
        einem unverhältnismäßigen Aufwand verbunden ist. Unbeschadet dessen hat der Nutzer ein Recht auf
        Auskunft über diese Empfänger.
      </p>
      <p>
        <strong>
          Ebenfalls haben die Nutzer und Betroffenen nach Art. 21 DSGVO das Recht auf Widerspruch gegen die
          künftige Verarbeitung der sie betreffenden Daten, sofern die Daten durch den Anbieter nach Maßgabe
          von Art. 6 Abs. 1 lit. f) DSGVO verarbeitet werden. Insbesondere ist ein Widerspruch gegen die
          Datenverarbeitung zum Zwecke der Direktwerbung statthaft.
        </strong>
      </p>
      <h3>III. Informationen zur Datenverarbeitung</h3>
      <p>
        Ihre bei Nutzung unseres Internetauftritts verarbeiteten Daten werden gelöscht oder gesperrt, sobald
        der Zweck der Speicherung entfällt, der Löschung der Daten keine gesetzlichen Aufbewahrungspflichten
        entgegenstehen und nachfolgend keine anderslautenden Angaben zu einzelnen Verarbeitungsverfahren
        gemacht werden.
      </p>

      <h4>Serverdaten</h4>
      <p>
        Aus technischen Gründen, insbesondere zur Gewährleistung eines sicheren und stabilen
        Internetauftritts, werden Daten durch Ihren Internet-Browser an uns bzw. an unseren Webspace-Provider
        übermittelt. Mit diesen sog. Server-Logfiles werden u.a. Typ und Version Ihres Internetbrowsers, das
        Betriebssystem, die Website, von der aus Sie auf unseren Internetauftritt gewechselt haben (Referrer
        URL), die Website(s) unseres Internetauftritts, die Sie besuchen, Datum und Uhrzeit des jeweiligen
        Zugriffs sowie die IP-Adresse des Internetanschlusses, von dem aus die Nutzung unseres
        Internetauftritts erfolgt, erhoben.
      </p>
      <p>
        Diese so erhobenen Daten werden vorrübergehend gespeichert, dies jedoch nicht gemeinsam mit anderen
        Daten von Ihnen.
      </p>
      <p>
        Diese Speicherung erfolgt auf der Rechtsgrundlage von Art. 6 Abs. 1 lit. f) DSGVO. Unser berechtigtes
        Interesse liegt in der Verbesserung, Stabilität, Funktionalität und Sicherheit unseres
        Internetauftritts.
      </p>
      <p>
        Die Daten werden spätestens nach 30 Tagen wieder gelöscht, soweit keine weitere Aufbewahrung zu
        Beweiszwecken erforderlich ist. Andernfalls sind die Daten bis zur endgültigen Klärung eines Vorfalls
        ganz oder teilweise von der Löschung ausgenommen.
      </p>

      <h4>Cookies</h4>
      <h5>a) Sitzungs-Cookies/Session-Cookies</h5>
      <p>
        Wir verwenden mit unserem Internetauftritt sog. Cookies. Cookies sind kleine Textdateien oder andere
        Speichertechnologien, die durch den von Ihnen eingesetzten Internet-Browser auf Ihrem Endgerät ablegt
        und gespeichert werden. Durch diese Cookies werden im individuellen Umfang bestimmte Informationen von
        Ihnen, wie beispielsweise Ihre Browser- oder Standortdaten oder Ihre IP-Adresse, verarbeitet. &nbsp;
      </p>
      <p>
        Durch diese Verarbeitung wird unser Internetauftritt benutzerfreundlicher, effektiver und sicherer, da
        die Verarbeitung bspw. die Wiedergabe unseres Internetauftritts in unterschiedlichen Sprachen oder das
        Angebot einer Warenkorbfunktion ermöglicht.
      </p>
      <p>
        Rechtsgrundlage dieser Verarbeitung ist Art. 6 Abs. 1 lit b.) DSGVO, sofern diese Cookies Daten zur
        Vertragsanbahnung oder Vertragsabwicklung verarbeitet werden.
      </p>
      <p>
        Falls die Verarbeitung nicht der Vertragsanbahnung oder Vertragsabwicklung dient, liegt unser
        berechtigtes Interesse in der Verbesserung der Funktionalität unseres Internetauftritts.
        Rechtsgrundlage ist in dann Art. 6 Abs. 1 lit. f) DSGVO.
      </p>
      <p>Mit Schließen Ihres Internet-Browsers werden diese Session-Cookies gelöscht.</p>
      <h5>b) Drittanbieter-Cookies</h5>
      <p>
        Gegebenenfalls werden mit unserem Internetauftritt auch Cookies von Partnerunternehmen, mit denen wir
        zum Zwecke der Werbung, der Analyse oder der Funktionalitäten unseres Internetauftritts
        zusammenarbeiten, verwendet.
      </p>
      <p>
        Die Einzelheiten hierzu, insbesondere zu den Zwecken und den Rechtsgrundlagen der Verarbeitung solcher
        Drittanbieter-Cookies, entnehmen Sie bitte den nachfolgenden Informationen.
      </p>
      <h5>c) Beseitigungsmöglichkeit</h5>
      <p>
        Sie können die Installation der Cookies durch eine Einstellung Ihres Internet-Browsers verhindern oder
        einschränken. Ebenfalls können Sie bereits gespeicherte Cookies jederzeit löschen. Die hierfür
        erforderlichen Schritte und Maßnahmen hängen jedoch von Ihrem konkret genutzten Internet-Browser ab.
        Bei Fragen benutzen Sie daher bitte die Hilfefunktion oder Dokumentation Ihres Internet-Browsers oder
        wenden sich an dessen Hersteller bzw. Support. Bei sog. Flash-Cookies kann die Verarbeitung allerdings
        nicht über die Einstellungen des Browsers unterbunden werden. Stattdessen müssen Sie insoweit die
        Einstellung Ihres Flash-Players ändern. Auch die hierfür erforderlichen Schritte und Maßnahmen hängen
        von Ihrem konkret genutzten Flash-Player ab. Bei Fragen benutzen Sie daher bitte ebenso die
        Hilfefunktion oder Dokumentation Ihres Flash-Players oder wenden sich an den Hersteller bzw.
        Benutzer-Support.
      </p>
      <p>
        Sollten Sie die Installation der Cookies verhindern oder einschränken, kann dies allerdings dazu
        führen, dass nicht sämtliche Funktionen unseres Internetauftritts vollumfänglich nutzbar sind.{' '}
      </p>

      <h4>Kundenkonto / Registrierungsfunktion</h4>
      <p>
        Falls Sie über unseren Internetauftritt ein Kundenkonto bei uns anlegen, werden wir die von Ihnen bei
        der Registrierung eingegebenen Daten (also bspw. Ihren Namen, Ihre Anschrift oder Ihre E-Mail-Adresse)
        ausschließlich für vorvertragliche Leistungen, für die Vertragserfüllung oder zum Zwecke der
        Kundenpflege (bspw. um Ihnen eine Übersicht über Ihre bisherigen Bestellungen bei uns zur Verfügung zu
        stellen oder um Ihnen die sog. Merkzettelfunktion anbieten zu können) erheben und speichern.
        Gleichzeitig speichern wir dann die IP-Adresse und das Datum Ihrer Registrierung nebst Uhrzeit. Eine
        Weitergabe dieser Daten an Dritte erfolgt natürlich nicht.
      </p>
      <p>
        Im Rahmen des weiteren Anmeldevorgangs wird Ihre Einwilligung in diese Verarbeitung eingeholt und auf
        diese Datenschutzerklärung verwiesen. Die dabei von uns erhobenen Daten werden ausschließlich für die
        Zurverfügungstellung des Kundenkontos verwendet.&nbsp;
      </p>
      <p>
        Soweit Sie in diese Verarbeitung einwilligen, ist Art. 6 Abs. 1 lit. a) DSGVO Rechtsgrundlage für die
        Verarbeitung.
      </p>
      <p>
        Sofern die Eröffnung des Kundenkontos zusätzlich auch vorvertraglichen Maßnahmen oder der
        Vertragserfüllung dient, so ist Rechtsgrundlage für diese Verarbeitung auch noch Art. 6 Abs. 1 lit. b)
        DSGVO.
      </p>
      <p>
        Die uns erteilte Einwilligung in die Eröffnung und den Unterhalt des Kundenkontos können Sie gemäß
        Art. 7 Abs. 3 DSGVO jederzeit mit Wirkung für die Zukunft widerrufen. Hierzu müssen Sie uns lediglich
        über Ihren Widerruf in Kenntnis setzen.
      </p>
      <p>
        Die insoweit erhobenen Daten werden gelöscht, sobald die Verarbeitung nicht mehr erforderlich ist.
        Hierbei müssen wir aber steuer- und handelsrechtliche Aufbewahrungsfristen beachten.
      </p>

      <h4>Adobe Fonts</h4>
      <h5>a) Art und Zweck der Verarbeitung</h5>
      <p>
        Wir setzen Adobe Typekit zur visuellen Gestaltung unserer Website ein. Typekit ist ein Dienst der
        Adobe Systems Software Ireland Companies (4-6 Riverwalk, Citywest Business Campus, Dublin 24, Republic
        of Ireland; nachfolgend „Adobe“), der uns den Zugriff auf eine Schriftartenbibliothek gewährt. Zur
        Einbindung der von uns benutzten Schriftarten, muss Ihr Browser eine Verbindung zu einem Server von
        Adobe in den USA aufbauen und die für unsere Website benötigte Schriftart herunterladen. Adobe erhält
        hierdurch die Information, dass von Ihrer IP-Adresse unsere Website aufgerufen wurde. Weitere
        Informationen zu Adobe Typekit finden Sie in den Datenschutzhinweisen von Adobe, die Sie hier abrufen
        können: https://www.adobe.com/de/privacy/policy.html
      </p>
      <h5>b) Rechtsgrundlage</h5>
      <p>
        Rechtsgrundlage für die Einbindung von Adobe Typekit und dem damit verbundenen Datentransfer zu Adobe
        ist Ihre Einwilligung (Art. 6 Abs. 1 lit. a DSGVO).
      </p>
      <h5>c) Empfänger</h5>
      <p>
        Der Aufruf von Svcriptbibliotheken oder Schriftbibliotheken löst automatisch eine Verbindung zum
        Betreiber der Bibliothek aus. Informationen über die Nutzung Ihrer Daten durch Adobe Typekit Web Fonts
        Sie unter https://typekit.com/ und in der Datenschutzerklärung von Adobe Typekit:
        https://www.adobe.com/de/privacy/policies/typekit.html.
      </p>
      <h5>Speicherdauer</h5>
      <p>Wir erheben keine personenbezogenen Daten durch die Einbindung von Adobe Typekit Web Fonts.</p>
      <h5>Drittlandtransfer</h5>
      <p>
        Adobe ist unter dem Privacy-Shield-Abkommen zertifiziert und bietet hierdurch eine Garantie, das
        europäische Datenschutzrecht einzuhalten
      </p>
      <p>
        <a
          target="_blank"
          rel="noopener noreferrer"
          href="https://www.privacyshield.gov/participant?id=a2zt0000000TNo9AAG&status=Active"
        >
          https://www.privacyshield.gov/participant?id=a2zt0000000TNo9AAG&status=Active
        </a>
      </p>
      <p>Bereitstellung vorgeschrieben oder erforderlich:</p>
      <p>
        Die Bereitstellung der personenbezogenen Daten ist weder gesetzlich, noch vertraglich vorgeschrieben.
        Allerdings kann ohne die korrekte Darstellung der Inhalte von Standardschriften nicht ermöglicht
        werden.
      </p>

      <h4>MailChimp</h4>
      <h5>a) Art und Zweck der Verarbeitung</h5>
      <p>
        Der Versand der Newsletter erfolgt mittels „MailChimp“, einer Newsletterversandplattform des
        US-Anbieters Rocket Science Group, LLC, 675 Ponce De Leon Ave NE #5000, Atlanta, GA 30308, USA. Die
        E-Mail-Adressen unserer Newsletterempfänger, als auch deren weitere, im Rahmen dieser Hinweise
        beschriebenen Daten, werden auf den Servern von MailChimp in den USA gespeichert. MailChimp verwendet
        diese Informationen zum Versand und zur Auswertung der Newsletter in unserem Auftrag. Des Weiteren
        kann MailChimp nach eigenen Informationen diese Daten zur Optimierung oder Verbesserung der eigenen
        Services nutzen, z.B. zur technischen Optimierung des Versandes und der Darstellung der Newsletter
        oder für wirtschaftliche Zwecke, um zu bestimmen aus welchen Ländern die Empfänger kommen. MailChimp
        nutzt die Daten unserer Newsletterempfänger jedoch nicht, um diese selbst anzuschreiben oder an Dritte
        weiterzugeben. Die Datenschutzbestimmungen von MailChimp finden Sie
        hier:https://mailchimp.com/legal/privacy
      </p>
      <h5>b) Statistische Erhebung und Analysen</h5>
      <p>
        Die Newsletter enthalten einen sog. „web-beacon“, d.h. eine pixelgroße Datei, die beim Öffnen des
        Newsletters von dem Server von MailChimp abgerufen wird. Im Rahmen dieses Abrufs werden zunächst
        technische Informationen, wie Informationen zum Browser und Ihrem System, als auch Ihre IP-Adresse und
        Zeitpunkt des Abrufs erhoben. Diese Informationen werden zur technischen Verbesserung der Services
        anhand der technischen Daten oder der Zielgruppen und ihres Leseverhaltens anhand derer Abruforte (die
        mit Hilfe der IP-Adresse bestimmbar sind) oder der Zugriffszeiten genutzt. Zu den statistischen
        Erhebungen gehört ebenfalls die Feststellung, ob die Newsletter geöffnet werden, wann sie geöffnet
        werden und welche Links geklickt werden. Diese Informationen können aus technischen Gründen zwar den
        einzelnen Newsletterempfängern zugeordnet werden. Es ist jedoch weder unser Bestreben, noch das von
        MailChimp, einzelne Nutzer zu beobachten. Auswertungen dienen uns viel mehr dazu, die Lesegewohnheiten
        unserer Nutzer zu erkennen und unsere Inhalte auf sie anzupassen oder unterschiedliche Inhalte
        entsprechend den Interessen unserer Nutzer zu versenden.
      </p>
      <h5>Drittlandtransfer</h5>
      <p>
        The Rocket Science Group LLC d/b/a MailChimp ist unter dem Privacy-Shield-Abkommen zertifiziert und
        bietet hierdurch eine Garantie, das europäische Datenschutzrecht einzuhalten
      </p>
      <p>
        <a
          target="_blank"
          rel="noopener noreferrer"
          href="https://www.privacyshield.gov/participant?id=a2zt0000000TO6hAAG&status=Active"
        >
          https://www.privacyshield.gov/participant?id=a2zt0000000TO6hAAG&status=Active
        </a>
      </p>

      <hr />

      <h3>Löschung von Daten</h3>
      <p>
        Wenn Sie Ihre Daten komplett von dieser Plattform löschen möchten, schreiben Sie bitte eine Mail an{' '}
        <a href="mailto:support@ernte-erfolg.de">support@ernte-erfolg.de</a> mit dem Betreff "Antrag auf
        Datenlöschung" und Ihrer auf ernte-erfolg.de verwendeten E-Mail Adresse.
      </p>
    </ContentContainer>
  );
}
