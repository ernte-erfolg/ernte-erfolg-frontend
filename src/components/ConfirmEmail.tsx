import React from 'react';
import { makeStyles, Typography, Link } from '@material-ui/core';
import { ProfileType } from '../api';

const useStyles = makeStyles((theme) => ({
  link: {
    cursor: 'pointer',
  },
}));

interface IConfirmEmailProps {
  profileType: ProfileType;
  resendEmail: () => any;
  onBack?: () => any;
}

const texts: Record<ProfileType, [string, string]> = {
  FARM: [
    'Bitte bestätigen Sie Ihre E-Mail-Adresse',
    'Wir haben Ihnen eine E-Mail mit einem Bestätigungslink geschickt. Bitte bestätigen Sie Ihre E-Mail-Adresse, um die Registrierung abzuschließen.',
  ],
  HELPER: [
    'Bitte bestätige Deine E-Mail-Adresse',
    'Wir haben Dir eine E-Mail mit einem Bestätigungslink geschickt. Bitte bestätige Die E-Mail-Adresse, um die Registrierung abzuschließen.',
  ],
};

const ConfirmEmail: React.FC<IConfirmEmailProps> = ({ profileType, resendEmail }) => {
  const classes = useStyles();
  return (
    <>
      <h2>{texts[profileType][0]}</h2>
      <Typography variant="body1">{texts[profileType][1]}</Typography>
      <br />
      <br />
      <Typography variant="h4" gutterBottom>
        E-Mail nicht angekommen?
      </Typography>
      <Link className={classes.link} underline="hover" variant="body1" onClick={resendEmail}>
        Bestätigungslink noch einmal senden
      </Link>
    </>
  );
};

export default ConfirmEmail;
