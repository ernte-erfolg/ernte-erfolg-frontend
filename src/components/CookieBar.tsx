import React, { useState, useEffect } from 'react';
import { Card, Typography, CardContent, Box, Button, Container } from '@material-ui/core';
import { setGtmVariable } from '../utils/sendGtmEvent';

const CookieBar: React.FC = () => {
  const [show, setShow] = useState(false);

  const setDecision = (decision: boolean) => {
    setGtmVariable('cookieBar', decision);
    localStorage.setItem('cookieDecision', decision ? '1' : '0');
    setShow(false);
  };

  useEffect(() => {
    if (localStorage.getItem('cookieDecision') !== null) {
      if (localStorage.getItem('cookieDecision') === '1') {
        setDecision(true);
      } else {
        setDecision(false);
      }
    } else {
      setShow(true);
    }
  }, []);

  return (
    <Box
      visibility={show ? 'visible' : 'hidden'}
      position="fixed"
      bottom={24}
      width="100vw"
      zIndex={10}
      display="flex"
      justifyContent="center"
    >
      <Container>
        <Card elevation={9}>
          <CardContent>
            <Typography variant="h4">Diese Webseite verwendet Cookies</Typography>
            <Box mt={2}>
              <Typography variant="body1">
                Wir verwenden Cookies zur Analyse der Seitenbenutzung. Dafür teilen wir Informationen über
                Ihre Benutzung der Seite mit unseren Analyse-Partnern, die diese unter Umständen mit anderen
                Informationen, welche diese über Sie gesammelt habe, kombinieren. Mit der weiteren Benutzung
                dieser Webseite stimmen Sie der Nutzung von Cookies zu.
              </Typography>
            </Box>
            <Box mt={2} display="flex" justifyContent="flex-end">
              <Box mr={2}>
                <Button variant="outlined" onClick={() => setDecision(false)}>
                  Ablehnen
                </Button>
              </Box>
              <Button variant="contained" color="primary" onClick={() => setDecision(true)}>
                Akzeptieren
              </Button>
            </Box>
          </CardContent>
        </Card>
      </Container>
    </Box>
  );
};

export default CookieBar;
