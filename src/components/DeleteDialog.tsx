import React, { useState } from 'react';
import { makeStyles, Dialog, Box, Button, Typography } from '@material-ui/core';
import DeleteButton from './DeleteButton';

interface IDeleteDialogProps {
  activator: React.ReactNode;
  title: string;
  onDelete: () => any;
}

const useStyles = makeStyles((theme) => ({
  activator: {
    cursor: 'pointer',
  },
  icon: {
    backgroundColor: theme.palette.primary.main,
    marginRight: theme.spacing(1),
  },
}));

const DeleteDialog: React.FC<React.PropsWithChildren<IDeleteDialogProps>> = (props) => {
  const classes = useStyles();
  const [open, setOpen] = useState(false);

  const performDelete = () => {
    setOpen(false);
    props.onDelete();
  };

  return (
    <>
      <Box className={classes.activator} onClick={() => setOpen(true)}>
        {props.activator}
      </Box>
      <Dialog open={open} fullWidth maxWidth="sm" onClose={() => setOpen(false)}>
        <Box p={3}>
          <Box mb={3}>
            <Typography variant="h3">{props.title}</Typography>
          </Box>
          {props.children}
          <Box mt={4} display="flex" justifyContent="flex-end">
            <Button autoFocus variant="outlined" size="large" onClick={() => setOpen(false)}>
              Abbrechen
            </Button>
            <Box ml={2}>
              <DeleteButton variant="contained" size="large" onClick={performDelete}>
                Löschen
              </DeleteButton>
            </Box>
          </Box>
        </Box>
      </Dialog>
    </>
  );
};

export default DeleteDialog;
