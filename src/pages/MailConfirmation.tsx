import React, { useEffect, useState } from 'react';
import { useParams, useHistory } from 'react-router-dom';
import { Typography } from '@material-ui/core';
import { Alert } from '@material-ui/lab';
import { AccountAPI } from '../api';
import { SUPPORT_MAIL } from '../constants';
import ContentContainer from '../containers/ContentContainer';
import { sendGtmEvent } from '../utils/sendGtmEvent';

const MailConfirmation: React.FC = () => {
  const { id } = useParams();
  const history = useHistory();
  const [error, setError] = useState('');

  useEffect(() => {
    (async () => {
      if (!id) {
        setError('Ungültiger Aktivierungslink');
        return;
      }
      try {
        await new AccountAPI().activateAccount(id);
        sendGtmEvent('eeRegistration');
        setTimeout(() => history.push('/login'), 1500);
      } catch (err) {
        setError(`Fehler beim Aktivieren der E-Mail. Bitte wende dich an unseren Support: ${SUPPORT_MAIL}`);
        console.error('Error', err);
      }
    })();
  }, [id, history]);

  return (
    <ContentContainer>
      <Typography variant="h4">E-Mail verifizieren</Typography>
      <p>
        {!error && <>Wir verifizieren gerade deine E-Mail-Adresse. Du wirst gleich weitergeleitet.</>}
        {error && (
          <Alert severity="error" color="error">
            {error}
          </Alert>
        )}
      </p>
    </ContentContainer>
  );
};

export default MailConfirmation;
