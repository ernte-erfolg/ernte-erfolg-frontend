import React from 'react';
import { Hidden } from '@material-ui/core';
import { useHistory } from 'react-router-dom';
import DesktopNavigationProfile from './desktop';
import MobileNavigationProfile from './mobile';
import { useDispatch, useSelector } from 'react-redux';
import { logout, selectUserState } from '../../slices/userSlice';

const NavigationProfile: React.FC = () => {
  const { user, farm, helper } = useSelector(selectUserState);
  const dispatch = useDispatch();
  const history = useHistory();

  let name = '';
  if (farm) {
    name = farm.name;
  } else if (helper) {
    name = `${helper.firstName} ${helper.lastName}`;
  } else {
    name = 'Neues Profil';
  }
  const avatarLetters = name
    .split(' ')
    .map((s) => s[0])
    .join('')
    .substr(0, 2)
    .toUpperCase();

  const doLogout = () => {
    dispatch(logout());
    history.push('/');
  };

  const isFarm = user?.userClass === 'FARM';

  return (
    <>
      <Hidden mdUp>
        <MobileNavigationProfile
          name={name}
          avatarLetters={avatarLetters}
          isFarm={isFarm}
          onLogout={doLogout}
        />
      </Hidden>
      <Hidden smDown>
        <DesktopNavigationProfile
          name={name}
          avatarLetters={avatarLetters}
          isFarm={isFarm}
          onLogout={doLogout}
        />
      </Hidden>
    </>
  );
};

export default NavigationProfile;
