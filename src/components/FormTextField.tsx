import React from 'react';
import { FieldError } from 'react-hook-form';
import { TextField, TextFieldProps } from '@material-ui/core';

export function getFormTextField<T extends Record<string, any>>(
  name: keyof T,
  label: string,
  registerFunction?: any,
  errors?: { [K in keyof T]?: FieldError },
  additionalProps?: Partial<TextFieldProps>
) {
  return (
    <TextField
      variant="outlined"
      name={name as string}
      label={label}
      fullWidth
      inputRef={registerFunction}
      error={errors && !!errors[name]}
      helperText={errors && errors[name]?.message}
      {...additionalProps}
    ></TextField>
  );
}
