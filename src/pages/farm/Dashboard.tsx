import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { Typography, Button, Box } from '@material-ui/core';
import { Task, TaskAPI } from '../../api';
import { LoadingErrorDisplay, TaskList } from '../../components';
import { GreyBarPage } from '../../containers';
import { selectUserState } from '../../slices/userSlice';
import { useSelector } from 'react-redux';
import { sortTasks } from '../../utils/taskUtils';

const Dashboard: React.FC = () => {
  const { farm } = useSelector(selectUserState);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState('');
  const [tasks, setTasks] = useState<Task[]>([]);

  useEffect(() => {
    (async () => {
      if (!farm || !farm.id) {
        return;
      }
      try {
        setLoading(true);
        const tasks = sortTasks(await new TaskAPI().getTasksByFarm(farm.id), 'firstDay', 'descending');
        setTasks(tasks);
      } catch (err) {
        console.log(err);
        setError((err as Error).message || 'Fehler beim Laden der Gesuche.');
      } finally {
        setLoading(false);
      }
    })();
    // if we would put farm in the dependency array, then we would
    // end in an infinite loop. Why? Because React!
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <GreyBarPage
      headline={<Typography variant="h1">Dashboard Gesuche</Typography>}
      greyBarContent={
        <>
          {!loading && tasks.length === 0 && (
            <Box mb={2}>
              <Typography variant="body2">
                Vielen Dank für Ihre Anmeldung bei Ernteerfolg. Es sind nur noch wenige Klicks nötig bis
                unsere Helfer Sie unterstützen können. Erstellen Sie ihr erstes Gesuch am besten gleich jetzt.
              </Typography>
            </Box>
          )}

          <Button component={Link} to="/farm/task/create" variant="contained" color="primary">
            Neues Gesuch einstellen
          </Button>
        </>
      }
    >
      <LoadingErrorDisplay error={error} loading={loading} />

      <Box my={3}>
        <TaskList title="Meine Gesuche" tasks={tasks}></TaskList>
      </Box>
    </GreyBarPage>
  );
};

export default Dashboard;
