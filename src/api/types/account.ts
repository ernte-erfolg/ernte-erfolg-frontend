export type ProfileType = 'FARM' | 'HELPER';

export interface User {
  id: number;
  activated: boolean;
  email: string;
  password: string;
  langKey: string;
  userClass: ProfileType;
}

export type CreateUser = Pick<User, 'email' | 'password' | 'langKey' | 'userClass'>;

export interface JWTToken {
  id_token: string;
}

export interface Login {
  password: string;
  rememberMe: boolean;
  username: string;
}
