import { createBrowserHistory } from 'history';
import { History } from 'history';

const customHistory: History = createBrowserHistory();

export default customHistory;
