FROM node:12 as builder

# Install dependencies and copy the code
WORKDIR /application
COPY package.json yarn.lock ./
RUN yarn install --silent --no-progress --frozen-lockfile --production=false
COPY . ./
RUN ./populate_env.sh && cat ./.env
RUN yarn run build

# Set up start script, health check and expose port
COPY entrypoint.sh healthcheck.sh /
ENTRYPOINT ["/entrypoint.sh"]
HEALTHCHECK --interval=10s --start-period=30s --retries=3 \
  CMD ["/healthcheck.sh"]

# Second stage
FROM nginx:1.14-alpine

# Install system dependencies
RUN apk add --update curl

# Copy NGINX config
COPY nginx_* ./

# Copy built files
WORKDIR /application/build
COPY --from=builder /application/build ./

# Set up start script, health check and expose port
COPY entrypoint.sh healthcheck.sh /
ENTRYPOINT ["/entrypoint.sh"]
HEALTHCHECK --interval=10s --start-period=30s --retries=3 \
  CMD ["/healthcheck.sh"]
EXPOSE 8080