import { BaseAPI } from './base';
import { Offer, CreateOffer } from '../types';

export class OfferAPI extends BaseAPI {
  protected baseUrl = '/offers';
  public getOffers = () => this.get<Offer[]>('/');
  public createOffer = (offer: CreateOffer) => this.post<Offer>('/', offer);
  public updateOffer = (id: number, offer: CreateOffer) => this.put<Offer>(`/${id}`, offer);
  public getOffer = (id: number) => this.get<Offer>(`/${id}`);
  public getOfferByTask = (taskId: number) => this.get<Offer>(`/byTask/${taskId}`);
}
