import React, { useState, useEffect } from 'react';
import { useParams, Link } from 'react-router-dom';
import { makeStyles, Box, Grid, Typography, LinearProgress, Avatar, /* Button */ } from '@material-ui/core';
import { Alert } from '@material-ui/lab';
import PhoneIcon from '@material-ui/icons/Phone';
import EmailIcon from '@material-ui/icons/Email';
import c from 'classnames';
import { Task, TaskAPI, Offer, OfferAPI } from '../api';
import {
  LoadingErrorDisplay,
  FarmerInfobox,
  HelperInfobox,
  ActionBar,
  /*DeleteButton,
  DeleteDialog,*/
} from '../components';
import { ContentContainer } from '../containers';
import { dateFromDto } from '../utils/workdayConverter';
import { getHoursFromTask } from '../utils/taskUtils';
import moment from 'moment';
import { useSelector } from 'react-redux';
import { selectUserState } from '../slices/userSlice';

const useStyles = makeStyles((theme) => ({
  greyedOut: {
    opacity: 0.7,
  },
  progressBar: {
    height: '40px',
    marginBottom: theme.spacing(1),
  },
  icon: {
    width: theme.spacing(4),
    height: theme.spacing(4),
    backgroundColor: theme.palette.primary.main,
    marginRight: theme.spacing(1),
  },
  mt: {
    marginTop: theme.spacing(2),
  },
}));

const TaskDetails: React.FC = () => {
  const classes = useStyles();
  const { id } = useParams();
  const { user } = useSelector(selectUserState);

  const [loading, setLoading] = useState(false);
  const [error, setError] = useState('');
  const [task, setTask] = useState<Task | null>(null);
  const [offer, setOffer] = useState<Offer | null>(null);

  useEffect(() => {
    (async () => {
      if (!id || !parseInt(id)) {
        return setError('Ungültige Task-ID');
      }

      let task = null;
      try {
        setLoading(true);
        setError('');
        task = await new TaskAPI().getTask(parseInt(id));
      } catch (err) {
        console.log(err);
        setError((err as Error).message || 'Fehler beim Laden des Gesuchs');
      } finally {
        setLoading(false);
      }

      let offer = null;
      if (user?.userClass === 'HELPER') {
        try {
          setLoading(true);
          offer = await new OfferAPI().getOfferByTask(parseInt(id));
        } catch (err) {
          // This is ok, because it is probably a 404, which means,
          // the helper doesn't yet have an offer for this task
          setOffer(null);
          // TODO: Better error handling for other HTTP status codes
        } finally {
          setLoading(false);
        }
      }

      // setting task and offer down here means both are loaded and there is no inconsistent state
      // where the task is already loaded but the offer is still loading
      setTask(task);
      setOffer(offer);
    })();
  }, [id, user]);

  /*const deleteTask = async () => {
    // TODO: Perform deletion API call
    console.log("Delete Task called")
  };*/

  /*const activateTask = async () => {
    if (!task) {
      return;
    }
    const updatedTask = await new TaskAPI().activateTask(task.id);
    setTask(updatedTask);
  };

  const deactivateTask = async () => {
    if (!task) {
      return;
    }
    const updatedTask = await new TaskAPI().deactivateTask(task.id);
    setTask(updatedTask);
  };*/

  const isFarmer = user?.userClass === 'FARM';
  let timespan = '';
  if (task) {
    const sortedDays = task.requestedDays.slice();
    sortedDays.sort((a, b) => dateFromDto(a).unix() - dateFromDto(b).unix());
    const firstDate = dateFromDto(sortedDays[0]).format('DD.MM.YYYY');
    const lastDate = dateFromDto(sortedDays[sortedDays.length - 1]).format('DD.MM.YYYY');
    timespan = `${firstDate} - ${lastDate}`;
  }

  let progress = 0;
  let progressInfo = '';
  if (task) {
    const hours = getHoursFromTask(task);
    progress = hours.requestedHours > 0 ? 100 * (hours.guaranteedHours / hours.requestedHours) : 0;
    progressInfo = `${hours.guaranteedHelpers} von ${hours.requestedHelpers} Helfertagen`;
  }

  let alert: string[] = [];
  let canHelp = true;
  if (task && !offer) {
    // check if task is already full
    const hours = getHoursFromTask(task);
    if (hours.guaranteedHours >= hours.requestedHours) {
      alert = [
        'Das Gesuch hat bereits genug Helfer.',
        'Du kannst dich nicht mehr auf das Gesuch anmelden, da bereits genug Helfer angemeldet sind.',
      ];
      canHelp = false;
    }

    // check if task is outdated
    const lastDay = task.requestedDays.reduce(
      (p, c) => (p.isBefore(moment(c.date), 'day') ? moment(c.date) : p),
      moment()
    );
    if (!lastDay.isAfter(moment(), 'day')) {
      if (isFarmer) {
        alert = [
          'Das Gesuch liegt in der Vergangenheit.',
          'Es können sich derzeit keine Helfer auf Ihr Gesuch anmelden.',
        ];
      } else {
        alert = [
          'Das Gesuch liegt in der Vergangenheit.',
          'Du kannst dich nicht auf ein Gesuch in der Vergangenheit anmelden.',
        ];
      }
      canHelp = false;
    }

    if (!task.active) {
      if (isFarmer) {
        alert = [
          'Das Gesuch ist deaktiviert.',
          'Es können sich derzeit keine Helfer auf Ihr Gesuch anmelden.',
        ];
      } else {
        alert = ['Das Gesuch ist deaktiviert.', 'Du kannst dich nicht auf deaktivierte Gesuche anmelden.'];
      }
      canHelp = false;
    }
  }

  let actionBar = undefined;
  if (isFarmer && task) {
    let leftActions: React.ReactNode;
    let rightActions: React.ReactNode;

    if (!task.active) {
      /*leftActions = (
        <DeleteDialog
          title="Gesuch löschen"
          onDelete={deleteTask}
          activator={
            <DeleteButton variant="outlined" size="large">
              Löschen
            </DeleteButton>
          }
        >
          Wollen Sie ihr Gesuch wirklich dauerhaft löschen? Es werden keine Helfer mehr angemeldet und bei der
          Ernte helfen.
        </DeleteDialog>
      );*/
      /*rightActions = (
        <Button variant="contained" size="large" color="primary" onClick={activateTask}>
          Aktivieren
        </Button>
      );*/
    } else {
      /*rightActions = (
        <Box display="flex">
          <Button size="large" variant="outlined" onClick={deactivateTask}>
            Deaktivieren
          </Button>
          <Box ml={2}>
            <Button size="large" variant="contained" color="primary">
              Bearbeiten
            </Button>
      </Box>
        </Box>
      );*/
    }

    actionBar = <ActionBar sticky leftActions={leftActions} rightActions={rightActions}></ActionBar>;
  }

  return (
    <ContentContainer>
      {(error || loading) && (
        <Box my={5}>
          <LoadingErrorDisplay loading={loading} error={error} />
        </Box>
      )}

      {alert.length > 0 && (
        <Box my={5}>
          <Alert severity="info">
            <b>{alert[0]}</b> {alert[1]}
          </Alert>
        </Box>
      )}

      <Grid className={c({ [classes.greyedOut]: !task?.active })} container spacing={4}>
        <Grid item xs={12}>
          <Typography variant="h1">{task?.title}</Typography>
          <Typography variant="body2">
            Gesuch von{' '}
            <u>
              <Link to={`/farm/profile/${task?.farmId}`}>{task?.farmName}</Link>
            </u>
          </Typography>
        </Grid>

        <Grid item xs={12}>
          <LinearProgress variant="determinate" className={classes.progressBar} value={progress} />
          <Typography variant="subtitle2">{progressInfo}</Typography>
        </Grid>

        <Grid item xs={12} lg={9}>
          <Box mb={5}>
            <Typography variant="h3">Zeitraum</Typography>
            <Typography variant="subtitle1">{timespan}</Typography>
          </Box>

          {task && (task.phone || task.email) && (
            <Box my={5}>
              <Typography variant="h3">Kontakt</Typography>
              {task?.phone && (
                <a href={`tel:${task.phone}`}>
                  <Box my={2} display="flex" alignItems="center">
                    <Avatar className={classes.icon}>
                      <PhoneIcon fontSize="small" />
                    </Avatar>
                    {task.phone}
                  </Box>
                </a>
              )}
              {task?.email && (
                <a href={`mailto:${task.email}`}>
                  <Box my={2} display="flex" alignItems="center">
                    <Avatar className={classes.icon}>
                      <EmailIcon fontSize="small" />
                    </Avatar>
                    {task.email}
                  </Box>
                </a>
              )}
            </Box>
          )}

          <Box my={5}>
            <Typography variant="h3">Beschreibung</Typography>
            <Box mt={1}>
              <Typography variant="body2">
                {task?.taskDescription || 'Keine Beschreibung angegeben.'}
              </Typography>
            </Box>
          </Box>

          <Box my={5}>
            <Grid item xs={12}>
              <Typography variant="h3">Informationen zum Einsatzort</Typography>
              <Box mt={1}>
                <Typography variant="body2">
                  {task?.street} {task?.houseNr}
                  <br />
                  {task?.zip} {task?.city}
                </Typography>
              </Box>
              <Box mt={1}>
                <Typography variant="body2">{task?.howToFindUs}</Typography>
              </Box>
            </Grid>
          </Box>
        </Grid>

        <Grid item xs={12} lg={3}>
          {isFarmer ? (
            <FarmerInfobox task={task} />
          ) : (
            <HelperInfobox task={task} offer={offer} canHelp={canHelp} />
          )}
        </Grid>
      </Grid>
      {actionBar}
    </ContentContainer>
  );
};

export default TaskDetails;
