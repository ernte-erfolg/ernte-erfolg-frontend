import React from 'react';
import { Grid, makeStyles } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  root: {
    marginTop: theme.spacing(4),
  },
}));

interface IDynamicGridProps {
  mt?: boolean;
}

const DynamicGrid: React.FC<React.PropsWithChildren<IDynamicGridProps>> = ({ mt, children }) => {
  const classes = useStyles();
  return (
    <Grid container className={mt ? classes.root : undefined}>
      <Grid item xs={12} sm={10} md={8} lg={6}>
        {children}
      </Grid>
    </Grid>
  );
}

export default DynamicGrid;
