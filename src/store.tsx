import { configureStore, ThunkAction, Action } from '@reduxjs/toolkit';
import user from './slices/userSlice';
import farmCalendar from './slices/farmCalendarSlice';
import helperCalendar from './slices/helperCalendarSlice';

const store = configureStore({
  reducer: {
    user,
    farmCalendar,
    helperCalendar,
  },
});

export type RootState = ReturnType<typeof store.getState>;
export type AppGetState = typeof store.getState;
export type AppDispatch = typeof store.dispatch;
export type AppThunk = ThunkAction<void, RootState, unknown, Action<string>>;

export default store;
