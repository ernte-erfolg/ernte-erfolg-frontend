import React from 'react';
import { Box, Avatar, List, ListItem, ListItemText } from '@material-ui/core';
import { Link } from 'react-router-dom';
import { INavigationProfileProps, useStyles } from './shared';

const MobileNavigationProfile: React.FC<INavigationProfileProps> = (props) => {
  const classes = useStyles();
  return (
    <Box display="flex" flexDirection="column">
      <Box display="flex" flexDirection="column" alignItems="center" my={3}>
        <Avatar className={classes.avatar}>{props.avatarLetters}</Avatar>
        <Box mt={2} textAlign="center">
          <b>{props.name}</b>
        </Box>
      </Box>
      <List>
        <ListItem component={Link} to={props.isFarm ? '/farm' : '/helping'}>
          <ListItemText>Dashboard</ListItemText>
        </ListItem>
        {!props.isFarm && (
          <ListItem component={Link} to="/helping/my-offers">
            <ListItemText>Meine Hilfsangebote</ListItemText>
          </ListItem>
        )}
        <ListItem component={Link} to={props.isFarm ? '/farm/edit-profile' : '/helping/edit-profile'}>
          <ListItemText>Profil bearbeiten</ListItemText>
        </ListItem>
        <ListItem onClick={props.onLogout}>
          <ListItemText>Logout</ListItemText>
        </ListItem>
      </List>
    </Box>
  );
};

export default MobileNavigationProfile;
