import React from 'react';
import { makeStyles, Container, Box } from '@material-ui/core';
import ContentContainer from './ContentContainer';
import c from "classnames";

const useStyles = makeStyles(
  (theme) => ({
    root: {
      marginTop: theme.spacing(3),
    },
    greyBar: {
      position: 'relative',
      paddingTop: theme.spacing(5),
      paddingBottom: theme.spacing(5),
      backgroundColor: theme.palette.grey[200],
    },
  }),
  { name: 'greyBarPage' }
);

interface IGreyBarPageProps {
  headline?: React.ReactNode;
  greyBarContent: React.ReactNode;
  disableMarginTop?: boolean;
}

const GreyBarPage: React.FC<React.PropsWithChildren<IGreyBarPageProps>> = ({
  headline,
  greyBarContent,
  children,
  disableMarginTop
}) => {
  const classes = useStyles();
  return (
    <div className={c({ [classes.root]: !disableMarginTop })}>
      {headline && <Container>{headline}</Container>}
      <Box className={classes.greyBar} marginTop={headline ? 3 : 0}>
        <Container>{greyBarContent || {}}</Container>
      </Box>
      {children && <ContentContainer>{children}</ContentContainer>}
    </div>
  );
};

export default GreyBarPage;
