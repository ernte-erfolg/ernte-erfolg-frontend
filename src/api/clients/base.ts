import axios, { AxiosRequestConfig, AxiosError } from 'axios';
import { BASE_URL } from '../../constants';

export abstract class BaseAPI {
  protected abstract baseUrl: string;
  private token: string | null;

  public constructor() {
    this.token = localStorage.getItem('token');
  }

  public handleError(err: AxiosError): Error {
    const msg = err.response?.data?.title ?? "";
    return new Error(msg);
  }

  protected async get<T>(path: string, config: AxiosRequestConfig = {}): Promise<T> {
    try {
      const r = await axios.get<T>(this.getUrl(path), this.getConfig(config));
      return r.data;
    } catch (err) {
      throw this.handleError(err);
    }
  }

  protected async post<T>(path: string, data: any, config: AxiosRequestConfig = {}): Promise<T> {
    try {
      const r = await axios.post<T>(this.getUrl(path), data, this.getConfig(config));
      return r.data;
    } catch (err) {
      throw this.handleError(err);
    }
  }

  protected async put<T>(path: string, data: any, config: AxiosRequestConfig = {}): Promise<T> {
    try {
      const r = await axios.put<T>(this.getUrl(path), data, this.getConfig(config));
      return r.data;
    } catch (err) {
      throw this.handleError(err);
    }
  }

  protected async delete<T>(path: string, config: AxiosRequestConfig = {}): Promise<T> {
    try {
      const r = await axios.delete<T>(this.getUrl(path), this.getConfig(config));
      return r.data;
    } catch (err) {
      throw this.handleError(err);
    }
  }

  private getConfig(config?: AxiosRequestConfig): AxiosRequestConfig {
    if (!config) {
      config = {};
    }
    if (this.token) {
      config.headers = config.headers ?? {};
      config.headers['Authorization'] = `Bearer ${this.token}`;
    }
    return config;
  }

  private getUrl(path: string): string {
    return BASE_URL + this.baseUrl + path;
  }
}
