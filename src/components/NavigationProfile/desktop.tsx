import React, { useState } from 'react';
import { Box, Avatar, Link, Typography, Menu, MenuItem } from '@material-ui/core';
import { Link as RouterLink } from 'react-router-dom';
import { INavigationProfileProps, useStyles } from './shared';
import ExpandMore from '@material-ui/icons/ExpandMore';
import ExpandLess from '@material-ui/icons/ExpandLess';
import { useSelector } from 'react-redux';
import { selectUserState } from '../../slices/userSlice';

const DesktopNavigationProfile: React.FC<INavigationProfileProps> = (props) => {
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
  const { farm } = useSelector(selectUserState);

  const openMenu = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };
  const closeMenu = () => {
    setAnchorEl(null);
  };

  return (
    <Box display="flex" flexDirection="row-reverse" alignItems="center">
      <Avatar className={classes.avatar}>{props.avatarLetters}</Avatar>
      <Box ml={3} mr={3}>
        <Typography variant="body2" className={classes.menuOpener} onClick={openMenu}>
          {props.name + ' '}
          {!anchorEl && <ExpandMore fontSize="small" style={{ verticalAlign: 'middle' }} />}
          {!!anchorEl && <ExpandLess fontSize="small" style={{ verticalAlign: 'middle' }} />}
        </Typography>
        <Menu
          id="simple-menu"
          anchorEl={anchorEl}
          getContentAnchorEl={
            null /* see https://stackoverflow.com/questions/48157863/how-to-make-a-dropdown-menu-open-below-the-appbar-using-material-ui */
          }
          anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}
          transformOrigin={{ vertical: 'top', horizontal: 'center' }}
          keepMounted
          open={!!anchorEl}
          onClose={closeMenu}
        >
          {!props.isFarm && (
            <MenuItem onClick={closeMenu}>
              <RouterLink className={classes.menuLink} to="/helping/my-offers">
                Meine Hilfsangebote
              </RouterLink>
            </MenuItem>
          )}
          {props.isFarm && farm && (
            <MenuItem onClick={closeMenu}>
              <RouterLink className={classes.menuLink} to={`/farm/profile/${farm.id}`}>
                Profil
              </RouterLink>
            </MenuItem>
          )}
          {(!props.isFarm || (props.isFarm && !farm)) && (
            <MenuItem onClick={closeMenu}>
              <RouterLink
                className={classes.menuLink}
                to={props.isFarm ? '/farm/edit-profile' : '/helping/edit-profile'}
              >
                Profil bearbeiten
              </RouterLink>
            </MenuItem>
          )}
          <MenuItem
            onClick={() => {
              closeMenu();
              props.onLogout();
            }}
          >
            Logout
          </MenuItem>
        </Menu>
      </Box>
      <Box display="flex">
        <Link component={RouterLink} to={props.isFarm ? '/farm' : '/helping'}>
          Dashboard
        </Link>
      </Box>
    </Box>
  );
};

export default DesktopNavigationProfile;
