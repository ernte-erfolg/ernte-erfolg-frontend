import React, { useEffect, useState } from 'react';
import { Typography, Box, Grid } from '@material-ui/core';
import moment from 'moment';
import { TaskList, /* HelperBadge, */ LoadingErrorDisplay } from '../../components';
import { ContentContainer } from '../../containers';
// import HelperBadge from '../../components/HelperBadge';
import { Offer, OfferAPI } from '../../api';
import { dateFromDto } from '../../utils/workdayConverter';

const MyOffers: React.FC = () => {
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState('');
  const [offers, setOffers] = useState<Offer[] | null>(null);

  useEffect(() => {
    (async () => {
      try {
        setError('');
        setLoading(true);
        setOffers(await new OfferAPI().getOffers());
      } catch (err) {
        console.log(err);
        setError((err as Error).message || 'Fehler beim Laden der Hilfsangebote.');
      } finally {
        setLoading(false);
      }
    })();
  }, []);

  const upcomingOffers: Offer[] = [];
  const pastOffers: Offer[] = [];
  if (offers) {
    for (const o of offers) {
      if (o.guaranteedDays.every((d) => dateFromDto(d).diff(moment()) < 0)) {
        pastOffers.push(o);
      } else {
        upcomingOffers.push(o);
      }
    }
  }

  return (
    <ContentContainer>
      <Grid container spacing={4}>
        <Grid item sm={12} md={7}>
          <LoadingErrorDisplay error={error} loading={loading} />
          <Box my={2}>
            <Typography variant="h1">Deine Hilfsangebote</Typography>
          </Box>
          <Box>
            <Typography variant="body2">
              Hier kannst du deine Hilfsangebote an die Landwirte in Deutschland einsehen. Wir listen hier
              auch auch die bereits erledigten Hilfsangebote auf.
            </Typography>
          </Box>
        </Grid>
        {/*<Grid item xs={12} md={2}>
          <Box display="flex" flexDirection="column" justifyContent="center" alignItems="center">
            <HelperBadge level={1} percentage={0} />
            <Box
              mt={2}
              fontWeight="fontWeightLight"
              fontSize="18px"
              display="flex"
              flexDirection="column"
              justifyContent="center"
              alignItems="center"
            >
              <div>Level 3 - Superhelfer</div>
              <div>3 Einsätze</div>
              <div>65 Helferpunkte</div>
            </Box>
          </Box>
        </Grid>

        <Grid item md={3}>
          <Box mb={1}>
            <Typography variant="body2">
              Für jede Stunde die du einem Landwirt hilfst bekommst du von uns 2 Helferpunkte.
            </Typography>
          </Box>
          <Box mb={1}>
            <Typography variant="body2">
              Für einen abgeschlossenen Einsatz erhältst du zudem 5 Helferpunkte.
            </Typography>
          </Box>
          <Box mb={1}>
            <Typography variant="body2">Pro 20 Helferpunkte steigst du eine Stufe auf.</Typography>
          </Box>
  </Grid>*/}
      </Grid>
      <Box mt={5}>
        <TaskList title="Kommende Einsätze" tasks={upcomingOffers.map((o) => o.task)} />
      </Box>
      <Box mt={5}>
        <TaskList title="Erledigte Einsätze" tasks={pastOffers.map((o) => o.task)} />
      </Box>
    </ContentContainer>
  );
};

export default MyOffers;
