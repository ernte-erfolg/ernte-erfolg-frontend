import React from 'react';
import { Typography, makeStyles, Theme } from '@material-ui/core';

interface IHelperBadgeProps {
  level: number;
  percentage: number;
  size?: number;
}

type StyleProps = Pick<IHelperBadgeProps, 'percentage' | 'size'>;

const useStyles = makeStyles((theme: Theme) => ({
  wrapper: {
    width: ({ size }: StyleProps) => size,
    height: ({ size }: StyleProps) => size,
  },
  outerCircle: {
    transform: ({ size }: StyleProps) => `scale(${size ? `0.${Math.floor((size * 100) / 130)}` : 1})`,
    transformOrigin: 'top left',
    position: 'relative',
    width: 130,
    height: 130,
    borderRadius: `50%`,
    background: ({ percentage }: StyleProps) =>
      `conic-gradient(from 180deg, ${theme.palette.primary.main} ${(360 / 100) * percentage}deg, ${
        theme.palette.common.white
      } ${(360 / 100) * percentage}deg)`,
  },
  innerCircle: {
    position: 'absolute',
    left: '8%',
    top: '8%',
    border: `1px solid #442C1C`,
    backgroundColor: '#442C1C',
    height: 110,
    width: 110,
    borderRadius: '50%',
    color: 'white',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
}));

const HelperBadge: React.FC<IHelperBadgeProps> = ({ level, percentage, size }) => {
  const classes = useStyles({ percentage, size });
  return (
    <div className={classes.wrapper}>
      <div className={classes.outerCircle}>
        <div className={classes.innerCircle}>
          <Typography variant="h3">{level}</Typography>
        </div>
      </div>
    </div>
  );
};

export default HelperBadge;
