import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { AppDispatch, RootState, AppGetState } from '../store';
import { IWorkdayInArray, IWorkday, IBasicWorkday } from '../types';
import { convertWorkdaysDateToArray, convertDateArraysToWorkdays } from '../utils/workdayConverter';

interface SliceState {
  selectedDays: IWorkdayInArray[];
}

const initialState: SliceState = {
  selectedDays: [],
};

export const farmCalendarSlice = createSlice({
  name: 'farmCalendar',
  initialState,
  reducers: {
    setDays: (state, action: PayloadAction<IWorkdayInArray[]>) => {
      state.selectedDays = action.payload;
    },
    resetFarmCalendarState: () => initialState,
  },
});

export const { setDays, resetFarmCalendarState } = farmCalendarSlice.actions;

export const setFarmWorkdays = (workdays: IWorkday[]) => (dispatch: AppDispatch) => {
  dispatch(setDays(convertWorkdaysDateToArray(workdays)));
};

export const addFarmWorkdays = (newBasicWorkdays: IBasicWorkday[]) => async (
  dispatch: AppDispatch,
  getState: AppGetState
) => {
  const currentWorkdays: IWorkday[] = convertDateArraysToWorkdays(await getState().farmCalendar.selectedDays);
  const newWorkdays = newBasicWorkdays.map((workday) => ({ ...workday, hours: 20 }));
  const combinedWorkDays = currentWorkdays.concat(newWorkdays);
  const noDuplicatesWorkDays = combinedWorkDays.filter((v, i) => {
    return combinedWorkDays.findIndex((workday) => v.date.isSame(workday.date, 'day')) === i;
  });
  dispatch(setDays(convertWorkdaysDateToArray(noDuplicatesWorkDays)));
};

export const removeFarmWorkdays = (removedWorkdays: IBasicWorkday[]) => async (
  dispatch: AppDispatch,
  getState: AppGetState
) => {
  const currentWorkdays: IWorkday[] = convertDateArraysToWorkdays(await getState().farmCalendar.selectedDays);
  const updatedWorkDays = currentWorkdays.reduce((acc: IWorkday[], cur) => {
    const isDayToKeep = !removedWorkdays
      .map((removedDay) => removedDay.date.isSame(cur.date, 'day'))
      .includes(true);
    isDayToKeep && acc.push(cur);
    return acc;
  }, []);

  dispatch(setDays(convertWorkdaysDateToArray(updatedWorkDays)));
};

export const updateWorkPeriodHours = (updatedWorkdays: IWorkday[], hours: number) => async (
  dispatch: AppDispatch,
  getState: AppGetState
) => {
  const currentWorkdays: IWorkday[] = convertDateArraysToWorkdays(await getState().farmCalendar.selectedDays);
  const updatedFarmWorkdays = currentWorkdays.map((workday) => {
    const isUpdated = !!updatedWorkdays.find((day) => day.date.isSame(workday.date, 'day'));
    if (isUpdated) {
      return { ...workday, hours };
    } else {
      return workday;
    }
  });

  dispatch(setDays(convertWorkdaysDateToArray(updatedFarmWorkdays)));
};
export const updateWorkDayHours = (updatedWorkday: IWorkday, hours: number) => async (
  dispatch: AppDispatch,
  getState: AppGetState
) => {
  const currentWorkdays: IWorkday[] = convertDateArraysToWorkdays(await getState().farmCalendar.selectedDays);
  const updatedFarmWorkdays = currentWorkdays.map((workday) => {
    const isUpdated = updatedWorkday.date.isSame(workday.date, 'day');
    if (isUpdated) {
      return { ...workday, hours };
    } else {
      return workday;
    }
  });

  dispatch(setDays(convertWorkdaysDateToArray(updatedFarmWorkdays)));
};

export const selectFarmCalendarState = (state: RootState) => state.farmCalendar;
export const selectFarmDaysMoment = (state: RootState): IWorkday[] =>
  convertDateArraysToWorkdays(state.farmCalendar.selectedDays);

export default farmCalendarSlice.reducer;
