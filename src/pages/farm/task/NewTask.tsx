import React, { useState, useEffect } from 'react';
import { makeStyles, Button, Box, Stepper, StepLabel, Step, Typography } from '@material-ui/core';
import { useForm } from 'react-hook-form';
import { useHistory, Link } from 'react-router-dom';

import { TaskAPI, CreateTask } from '../../../api';
import Calendar from './Calendar';
import Time from './Time';
import Details from './Details';
import { workdayToDto } from '../../../utils/workdayConverter';
import { sendGtmEvent } from '../../../utils/sendGtmEvent';
import { GreyBarPage } from '../../../containers';
import { LoadingErrorDisplay, ActionBar } from '../../../components';
import { useSelector, useDispatch } from 'react-redux';
import { selectUserState } from '../../../slices/userSlice';
import { selectFarmDaysMoment, resetFarmCalendarState } from '../../../slices/farmCalendarSlice';

const useStyles = makeStyles((theme) => ({
  bottomBar: {
    position: 'sticky',
    width: '100%',
    bottom: '0',
    zIndex: 3,
    padding: theme.spacing(4),
    marginTop: theme.spacing(4),
    marginBottom: theme.spacing(4),
    borderTop: `1px solid ${theme.palette.divider}`,
    backgroundColor: theme.palette.background.default,
  },
  stepper: {
    backgroundColor: 'transparent',
    padding: 0,
  },
}));

type DetailsType = Omit<CreateTask, 'farmId' | 'requestedDays'>;

const NewTask: React.FC = () => {
  const classes = useStyles();
  const history = useHistory();
  const dispatch = useDispatch();
  const { farm } = useSelector(selectUserState);
  const days = useSelector(selectFarmDaysMoment);

  const { register, errors, formState, getValues } = useForm<DetailsType>({
    mode: 'onChange',
    defaultValues: {
      street: farm?.street,
      houseNr: farm?.houseNr,
      zip: farm?.zip,
      city: farm?.city,
      phone: farm?.phone,
      email: farm?.contactMail,
    },
  });

  const [step, setStep] = useState(0);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState('');

  useEffect(() => {
    return () => {
      dispatch(resetFarmCalendarState());
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const next = async () => {
    if (step < 2) {
      setStep(step + 1);
    } else {
      if (!farm?.id) {
        return setError('Farm-Profil nicht gefunden. Haben Sie ihr Profil bereits angelegt?');
      }
      try {
        setError('');
        setLoading(true);
        const newTask = await new TaskAPI().createTask({
          ...getValues(),
          requestedDays: days.map(workdayToDto),
        });
        sendGtmEvent('eeTaskCreated');
        dispatch(resetFarmCalendarState());
        history.push(`/task/${newTask.id}`);
      } catch (err) {
        console.log(err);
        setLoading(false);
        setError((err as Error).message || 'Fehler beim Erstellen des Gesuchs.');
      }
    }
  };

  const canBack = step > 0;
  const back = () => {
    if (step > 0) {
      setError('');
      setStep(step - 1);
    }
  };

  const cancel = () => {}; // TODO

  const canContinue =
    (step === 0 && days.length > 0) ||
    (step === 1 && days.every((d) => d.hours > 0)) ||
    (step === 2 && formState.isValid);

  const stepper = (
    <Stepper className={classes.stepper} activeStep={step} alternativeLabel>
      <Step>
        <StepLabel>Zeitraum</StepLabel>
      </Step>
      <Step>
        <StepLabel>Personenbedarf</StepLabel>
      </Step>
      <Step>
        <StepLabel>Details</StepLabel>
      </Step>
    </Stepper>
  );

  const actionBar = (
    <ActionBar
      sticky
      topActions={<LoadingErrorDisplay loading={loading} error={error} />}
      leftActions={
        <Button variant="outlined" size="large" disabled={!canBack || loading} onClick={back}>
          Zurück
        </Button>
      }
      rightActions={
        <Box display="flex">
          <Button
            component={Link}
            to="/farm"
            variant="outlined"
            size="large"
            onClick={cancel}
            disabled={loading}
          >
            Abbrechen
          </Button>
          <Box ml={2}>
            <Button
              variant="contained"
              color="primary"
              size="large"
              disabled={!canContinue || loading}
              onClick={next}
            >
              {step < 2 ? 'Weiter' : 'Veröffentlichen'}
            </Button>
          </Box>
        </Box>
      }
    />
  );

  return (
    <GreyBarPage greyBarContent={stepper} headline={<Typography variant="h1">Gesuch erstellen</Typography>}>
      {step === 0 && <Calendar />}
      {step === 1 && <Time />}
      {step === 2 && <Details register={register} errors={errors} />}
      {actionBar}
    </GreyBarPage>
  );
};

export default NewTask;
