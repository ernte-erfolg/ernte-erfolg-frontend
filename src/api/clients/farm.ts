import { BaseAPI } from './base';
import { Farm, CreateFarm } from '../types';

export class FarmAPI extends BaseAPI {
  protected baseUrl = '/farms';
  public createFarm = (farm: CreateFarm) => this.post<Farm>('/', farm);
  public updateFarm = (farm: CreateFarm) => this.put<Farm>('/', farm);
  public getFarm = (id: number) => this.get<Farm>(`/${id}`);
  public getFarmOfCurrentUser = () => this.get<Farm>('/byUserIsCurrentUser');
  public deleteFarm = () => this.delete('/');
  public confirmDelete = (key: string) => this.get('/confirmDeletion', { params: { key } });
}
