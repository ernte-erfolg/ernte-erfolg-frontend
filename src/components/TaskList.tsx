import React, { useState } from 'react';
import { Typography, Grid, Box, Button } from '@material-ui/core';
import { Task } from '../api';
import TaskView from './Task';

interface ITaskListProps {
  title: string | React.ReactNode;
  tasks: Task[] | null;
  emptyText?: string;
}

const RequestList: React.FC<ITaskListProps> = ({ title, tasks, emptyText }) => {
  const [showOverflowing, setShowOverflowing] = useState(false);

  if (tasks === null) {
    tasks = [];
  }
  const isOverflowing = tasks.length > 8;
  const tasksToShow = (isOverflowing && showOverflowing) || tasks.length === 8 ? tasks : tasks.slice(0, 7);

  const titleNode =
    typeof title === 'string' ? (
      <Box mb={3}>
        <Typography variant="h2">{title}</Typography>
      </Box>
    ) : (
      title
    );

  return (
    <>
      {titleNode}
      <Grid container spacing={3}>
        {tasksToShow.length === 0 && (
          <Grid item xs={12}>
            <Typography variant="body1">{emptyText || 'Keine Gesuche vorhanden'}</Typography>
          </Grid>
        )}
        {tasksToShow.map((t) => (
          <Grid key={t.id} item xs={12} sm={6} md={4} lg={3}>
            <TaskView task={t}></TaskView>
          </Grid>
        ))}
        {isOverflowing && !showOverflowing && (
          <Grid item xs={12} sm={6} md={4} lg={3}>
            <Box display="flex" height="100%" justifyContent="center" alignItems="center">
              <Button variant="outlined" onClick={() => setShowOverflowing(true)}>
                Mehr anzeigen
              </Button>
            </Box>
          </Grid>
        )}
      </Grid>
    </>
  );
};

export default RequestList;
