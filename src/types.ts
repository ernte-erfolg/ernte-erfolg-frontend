import { Moment } from 'moment';

export interface IBasicWorkday {
  date: Moment;
}

export interface IBasicWorkdayInArray {
  date: number[];
}

export interface ICreateOfferWorkday extends IBasicWorkday {
  requestedHours: number;
  remainingHours: number;
  selectedHours: number;
}

export interface ICreateOfferWorkdayInArray extends IBasicWorkdayInArray {
  requestedHours: number;
  remainingHours: number;
  selectedHours: number;
}

export interface IWorkday extends IBasicWorkday {
  hours: number;
}

export interface IWorkdayInArray extends IBasicWorkdayInArray {
  hours: number;
}
