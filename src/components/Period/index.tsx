import React, { useState } from 'react';
import { makeStyles, Box, Card, Grid, TextField } from '@material-ui/core';
import PersonList from './PersonList';
import DetailsView from './DetailsView';
import { IWorkday } from '../../types';
import { helpersToHours, hoursToHelpers } from '../../utils/hourConversions';
import CardHeader from '../CardHeader';
import { useDispatch } from 'react-redux';
import { updateWorkPeriodHours } from '../../slices/farmCalendarSlice';

const useStylesMonth = makeStyles((theme) => ({
  root: {
    marginTop: theme.spacing(4),
    marginBottom: theme.spacing(4),
  },
  detailSwitcher: {
    color: theme.palette.text.secondary,
    textDecoration: 'underline',
    cursor: 'pointer',
  },
}));

const Period: React.FC<{ days: IWorkday[] }> = ({ days }) => {
  const classes = useStylesMonth();
  const dispatch = useDispatch();
  const [showDetails, setShowDetails] = useState(false);

  const startDay = days[0];
  const endDay = days[days.length - 1];

  let helpersNeeded = hoursToHelpers(days[0].hours);
  let helpersNeededString = '';
  if (!days.every((d) => d.hours === days[0].hours) && !days.every((d) => isNaN(d.hours))) {
    helpersNeededString = 's. Detailansicht';
    helpersNeeded = 0;
  } else if (!Number.isNaN(helpersNeeded)) {
    helpersNeededString = helpersNeeded.toString();
  }

  const setHelpersNeeded = (value: number) => dispatch(updateWorkPeriodHours(days, helpersToHours(value)));

  return (
    <Grid container>
      <Grid item xs={12} lg={showDetails ? 12 : 6}>
        <Card className={classes.root} variant="outlined">
          <CardHeader
            title={`${startDay.date.format('DD. MMMM')} - ${endDay.date.format('DD. MMMM YYYY')}`}
          />
          <Box p={4}>
            <Grid container spacing={4}>
              <Grid item container xs={12} lg={showDetails ? 6 : 12}>
                <Grid container item alignItems="center" justify="flex-start" xs={12}>
                  <Box ml={5} className={classes.detailSwitcher} onClick={() => setShowDetails(!showDetails)}>
                    {showDetails ? 'Personen für den gesamten Zeitraum definieren' : 'Detailansicht öffnen'}
                  </Box>
                </Grid>
              </Grid>

              {!showDetails && (
                <>
                  <Grid item xs={12}>
                    <TextField
                      variant="outlined"
                      size="small"
                      helperText="1 Person = 10 Stunden"
                      value={helpersNeededString}
                      onInput={(e) => setHelpersNeeded(parseInt((e.target as HTMLInputElement).value))}
                    ></TextField>
                  </Grid>

                  <Grid item xs={12}>
                    <PersonList
                      value={Number.isNaN(helpersNeeded) ? 0 : helpersNeeded}
                      onInput={setHelpersNeeded}
                    ></PersonList>
                  </Grid>
                </>
              )}

              {showDetails && (
                <Grid item xs={12}>
                  <DetailsView days={days} />
                </Grid>
              )}
            </Grid>
          </Box>
        </Card>
      </Grid>
    </Grid>
  );
};

export default Period;
