import React from 'react';
import { GreyBarPage } from '../containers';
import { Box, Grid, Button } from '@material-ui/core';
import { Link } from 'react-router-dom';

const NotFound: React.FC = () => {
  return (
    <GreyBarPage
      headline={<h1>Ups</h1>}
      greyBarContent={
        <Box fontSize="46px" fontWeight="bold">
          404 - Seite wurde nicht gefunden
        </Box>
      }
    >
      <Grid container>
        <Grid item xs={12} lg={6}>
          <p>
            Da ist wohl etwas schief gelaufen. Diese Seite wurde nicht gefunden. Geh am besten zurück zur
            Startseite und probier es noch mal, dafür kannst du auch direkt hier klicken.
          </p>
          <Button component={Link} to="/" variant="contained" color="primary">
            Startseite
          </Button>
        </Grid>
        <Grid item xs={12} lg={6}>
          <img
            src="/img/404_karotte.png"
            alt="404 Symbolbild"
            style={{ width: '100%', maxHeight: '40vh', objectFit: 'contain' }}
          ></img>
        </Grid>
      </Grid>
    </GreyBarPage>
  );
};

export default NotFound;
