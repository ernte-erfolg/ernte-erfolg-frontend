import React from 'react';
import { makeStyles } from '@material-ui/core';
import c from 'classnames';

const useStyles = makeStyles((theme) => ({
  root: {
    [theme.breakpoints.up('lg')]: {
      maxWidth: '1440px',
    },
    width: '100%',
    marginLeft: 'auto',
    boxSizing: 'border-box',
    marginRight: 'auto',
    display: 'block', // Fix IE 11 layout when used with main.
  },
  negativeMargin: {
    marginTop: '-61px',
  },
}), { name: 'WideContainer' });

interface IWideContainerProps {
  applyNegativeMargin?: boolean;
}

const WideContainer: React.FC<React.PropsWithChildren<IWideContainerProps>> = ({
  applyNegativeMargin,
  children,
}) => {
  const classes = useStyles();
  return <div className={c(classes.root, { [classes.negativeMargin]: applyNegativeMargin })}>{children}</div>;
};

export default WideContainer;
