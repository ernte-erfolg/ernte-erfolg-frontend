import React from 'react';
import {
  useMediaQuery,
  useTheme,
  Dialog,
  DialogTitle,
  DialogContent,
  IconButton,
  Typography,
  makeStyles,
  Box,
} from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import Login from './Login';

interface ILoginDialogProps {
  open: boolean;
  setOpen: (v: boolean) => void;
  onLoginComplete?: () => void;
}

const useStyles = makeStyles((theme) => ({
  title: {
    borderBottom: `1px solid ${theme.palette.grey[300]}`,
  },
  closeButton: {
    color: theme.palette.grey[500],
  },
}));

const LoginDialog: React.FC<ILoginDialogProps> = ({ open, setOpen, onLoginComplete }) => {
  const theme = useTheme();
  const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));
  const classes = useStyles();

  const handleLogin = () => {
    setOpen(false);
    if (onLoginComplete) {
      onLoginComplete();
    }
  };

  const close = () => setOpen(false);

  return (
    <Dialog fullScreen={fullScreen} open={open} fullWidth maxWidth="sm" onClose={close}>
      <DialogTitle disableTypography>
        <Box display="flex" alignItems="center" className={classes.title}>
          <Box flexGrow="1">
            <Typography variant="h3">Anmelden</Typography>
          </Box>
          <IconButton aria-label="close" className={classes.closeButton} onClick={close}>
            <CloseIcon />
          </IconButton>
        </Box>
      </DialogTitle>
      <DialogContent>
        <Login onLoginComplete={handleLogin} onRegisterClicked={close} />
      </DialogContent>
    </Dialog>
  );
};

export default LoginDialog;
