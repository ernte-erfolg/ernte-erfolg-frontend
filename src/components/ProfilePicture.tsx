import React from 'react';

interface IProfilePictureProps {
  url: string;
}

const ProfilePicture: React.FC<IProfilePictureProps> = ({ url }) => {
  const dimensions = 210;
  const path = `M${dimensions / 2},0 L${dimensions},${dimensions / 2} L${dimensions / 2},${dimensions} L0,${dimensions/2} Z`;
  return (
    <svg width={dimensions + 3} height={dimensions + 3} viewBox={`-1 -1 ${dimensions + 2} ${dimensions + 2}`}>
      <defs>
        <clipPath id="logoClip">
          <path d={path} />
        </clipPath>
      </defs>
      <rect x="0" y="0" width={dimensions} height={dimensions} fill="white" clipPath="url(#logoClip)" />
      <image fill="white" x="0" y="0" width={dimensions} height={dimensions} href={url} clipPath="url(#logoClip)" />
      <path d={path} stroke="#3E3E3E" strokeWidth="2" fill="none" />
    </svg>
  )
};

export default ProfilePicture;
