import React, { useState } from 'react';
import { Button, Typography, Grid, Box } from '@material-ui/core';
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';
import moment from 'moment';
import { MonthView } from '../../../components';

const Calendar: React.FC = () => {
  const [startDate, setStartDate] = useState(moment());

  const months = [0, 1, 2, 3].map((n) => startDate.clone().add(n, 'M'));

  return (
    <Grid container spacing={3}>
      <Grid item xs={12}>
        <Typography variant="h3">Zeitraum</Typography>
        <Typography variant="body2">
          Bitte wählen Sie hier den Zeitraum an dem Sie Untersützung benötigen.
        </Typography>
      </Grid>
      <Grid item xs={6}>
        <Box textAlign="left">
          <Button onClick={() => setStartDate(startDate.clone().subtract(1, 'M'))} color="primary">
            <KeyboardArrowLeft />
            Vorheriger Monat
          </Button>
        </Box>
      </Grid>
      <Grid item xs={6}>
        <Box textAlign="right">
          <Button onClick={() => setStartDate(startDate.clone().add(1, 'M'))} color="primary">
            Nächster Monat
            <KeyboardArrowRight />
          </Button>
        </Box>
      </Grid>
      <Grid item xs={12}>
        <Grid container>
          {months.map((m, i) => (
            <Grid key={i} item xs={12} lg={6}>
              <Box my={4} ml={{ lg: i % 2 !== 0 ? 4 : 0 }} mr={{ lg: i % 2 !== 0 ? 0 : 4 }}>
                <Typography variant="h6">{m.format('MMMM YYYY')}</Typography>
                <MonthView year={m.year()} month={m.month()} />
              </Box>
            </Grid>
          ))}
        </Grid>
      </Grid>
    </Grid>
  );
};

export default Calendar;
