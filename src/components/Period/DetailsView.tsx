import React from 'react';
import CalendarDay from '../Calendar/CalendarDay';
import { makeStyles, Box, Grid, TextField } from '@material-ui/core';
import { IWorkday } from '../../types';
import { helpersToHours, hoursToHelpers } from '../../utils/hourConversions';
import { updateWorkDayHours } from '../../slices/farmCalendarSlice';
import { useDispatch } from 'react-redux';

const useStyles = makeStyles(() => ({
  calendarDay: {
    width: '64px',
    height: '64px',
  },
}));

const DetailsView: React.FC<{ days: IWorkday[] }> = ({ days }) => {
  const classes = useStyles();
  const dispatch = useDispatch();

  const handleHoursChange = (updatedDay: IWorkday) => (e: React.FormEvent<HTMLDivElement>) => {
    const hours = helpersToHours(parseInt((e.target as HTMLInputElement).value));
    dispatch(updateWorkDayHours(updatedDay, hours));
  };

  return (
    <Grid container spacing={3}>
      {days.map((d) => {
        const hours = hoursToHelpers(d.hours);
        return (
          <Grid key={d.date.unix()} item xs={12} md={6} lg={4}>
            <Box display="flex" alignItems="center" mx={2}>
              <Box className={classes.calendarDay}>
                <CalendarDay status="covered" selected day={d.date.format('DD')} onClick={() => {}} />
              </Box>
              <Box ml={3} flexGrow="1">
                <TextField
                  variant="outlined"
                  fullWidth
                  label="Personen pro Tag"
                  value={Number.isNaN(hours) ? '' : hours.toString()}
                  onInput={handleHoursChange(d)}
                />
              </Box>
            </Box>
          </Grid>
        );
      })}
    </Grid>
  );
};

export default DetailsView;
