import { BaseAPI } from './base';
import { User, CreateUser, Login, JWTToken } from '../types';

export class AccountAPI extends BaseAPI {
  protected baseUrl = '/account';
  public getAccount = () => this.get<User>('/');
  public activateAccount = (token: string) => this.get<void>('/activate', { params: { key: token } });
  public registerAccount = (data: CreateUser) => this.post('/register', data);
  public authorize = (login: Login) => this.post<JWTToken>('/authenticate', login);
}
