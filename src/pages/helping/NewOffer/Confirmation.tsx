import React from 'react';
import { Task, Offer } from '../../../api';
import { Link } from 'react-router-dom';
import { Box, Button } from '@material-ui/core';
import { dtoToWorkday } from '../../../utils/workdayConverter';

interface IConfirmationProps {
  task: Task;
  offer: Offer;
}

const Confirmation: React.FC<IConfirmationProps> = ({ task, offer }) => {
  const sortedDays = offer.guaranteedDays.map(dtoToWorkday);
  sortedDays.sort((a, b) => a.date.unix() - b.date.unix());

  return (
    <>
      <h2>Unterstützung eingetragen</h2>
      <p>
        Wir haben deine Unterstützung empfangen und leiten diese an den Landwirt weiter. Wenn du Fragen hast,
        wende dich einfach direkt an den Landwirt. Die Kontaktinformationen findest du im{' '}
        <Link to={`/task/${task.id}`}>
          <u>Gesuch</u>
        </Link>{' '}
        oder auf der{' '}
        <Link to={`/farm/profile/${task.farmId}`}>
          <u>Profilseite des Landwirts</u>
        </Link>
        .
      </p>
      <Box my={4}>
        <p>
          <b>{task.farmName} erwartet dich:</b>
        </p>
        <ul>
          {sortedDays.map((d) => (
            <li key={d.date.unix()}>
              am {d.date.format('DD.MM.YYYY')} für {d.hours} Stunden
            </li>
          ))}
        </ul>
      </Box>
      <p>Danke, dass du deinen Teil dazu beiträgst, die Nahrungsversorgung in Deutschland zu sichern!</p>
      <Box display="flex">
        <Button component={Link} to="/helping/my-offers" variant="outlined" size="large">
          Deine Hilfsangebote
        </Button>
        <Box ml={2}>
          <Button component={Link} to="/helping" variant="contained" color="primary" size="large">
            Dashboard
          </Button>
        </Box>
      </Box>
    </>
  );
};

export default Confirmation;
