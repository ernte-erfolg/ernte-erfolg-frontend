#!/bin/sh

if [ "${ENABLE_REVERSE_PROXY}" ]; then
  echo "Enabling reverse proxy"
  cp /nginx_reverse_proxy.conf /etc/nginx/nginx.conf
else
  echo "Reverse proxy disabled"
  cp /nginx_base.conf /etc/nginx/nginx.conf
fi

if [ "${DISALLOW_ROBOTS}" ]; then
  echo "Disallowing robots"
  printf "User-agent: *\nDisallow: /" > /application/build/robots.txt
fi

set -- nginx -g "daemon off;"
exec "$@"
