import axios from 'axios';
import { IBackendInfo } from '../types/version';
import { BASE_URL } from '../../constants';

export class VersionAPI {
  public async getVersion(): Promise<IBackendInfo> {
    try {
      const r = await axios.get<IBackendInfo>(new URL('../management/info', BASE_URL).toString());
      return r.data;
    } catch (err) {
      // TODO
      throw new Error();
    }
  }
}
