const { setupGlobals, createAccount } = require('./support/utils');

async function main() {
  setupGlobals();

  // create some accounts which will be used in tests
  await createAccount('farm1@ernte-erfolg.de', '112211aA', 'FARM', true);
  await createAccount('helper1@ernte-erfolg.de', '112211aA', 'HELPER', true);
  await createAccount('farm2@ernte-erfolg.de', '112211aA', 'FARM', true, {
    name: 'Testfarm',
    contactMail: 'farm2@ernte-erfolg.de',
    phone: '+49 111 123456',
    street: 'Spargelstraße',
    houseNr: '8',
    zip: '37308',
    city: 'Spargelstadt',
    description: 'Testbeschreibung',
  });
  await createAccount('helper2@ernte-erfolg.de', '112211aA', 'HELPER', true, {
    firstName: 'Max',
    lastName: 'Musterhelfer',
    phone: '+49 123 456789',
    zip: '37308',
  });

  process.exit(0);
}

main();
