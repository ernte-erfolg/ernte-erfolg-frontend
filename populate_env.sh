#!/bin/bash

COMMIT_HASH=$(git rev-parse --short HEAD)
TAGS=$(git tag --points-at HEAD | sed -e 'H;${x;s/\n/,/g;s/^,//;p;};d')
BRANCH=$(git rev-parse --abbrev-ref HEAD)
BUILD_TIME=$(date +%s)

echo "REACT_APP_COMMIT_HASH = $COMMIT_HASH" >> .env
echo "REACT_APP_TAGS = ${TAGS:-$BRANCH}" >> .env
echo "REACT_APP_BUILD_TIME = $BUILD_TIME" >> .env