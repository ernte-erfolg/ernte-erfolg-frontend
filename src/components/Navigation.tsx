import React, { useState } from 'react';
import {
  makeStyles,
  Button,
  Container,
  Drawer,
  Hidden,
  List,
  ListItemText,
  ListItem,
  IconButton,
  Box,
} from '@material-ui/core';
import MenuIcon from '@material-ui/icons/Menu';
import { Link } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { selectUserState } from '../slices/userSlice';
import NavigationProfile from './NavigationProfile';
import LoginDialog from './LoginDialog';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'grid',
    gridTemplateColumns: 'auto 1fr',
    gridTemplateRows: 'auto 60px',
  },
  img: {
    width: '180px',
    height: '180px',
    gridRowEnd: 'span 2',
    zIndex: 2,
  },
  navigationContainer: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    borderBottom: `1px solid ${theme.palette.grey[400]}`,
  },
}));

const Navigation: React.FC = () => {
  const classes = useStyles();
  const { user } = useSelector(selectUserState);
  const [showDrawer, setShowDrawer] = useState(false);
  const [showLoginDialog, setShowLoginDialog] = useState(false);

  return (
    <>
      <Hidden mdUp>
        <Drawer anchor="left" open={showDrawer} onClose={() => setShowDrawer(false)}>
          <Box onClick={() => setShowDrawer(false)}>
            {!user && (
              <List>
                <ListItem onClick={() => setShowLoginDialog(true)}>
                  <ListItemText>Anmelden</ListItemText>
                </ListItem>
              </List>
            )}
            {!!user && <NavigationProfile />}
          </Box>
        </Drawer>
      </Hidden>
      <Container>
        <div className={classes.root}>
          <div className={classes.img}>
            <Link to={!user ? '/' : user.userClass === 'FARM' ? '/farm' : '/helping'}>
              <img className={classes.img} src="/img/logo.png" alt="Logo" />
            </Link>
          </div>

          <div className={classes.navigationContainer}>
            <Hidden mdUp>
              <IconButton onClick={() => setShowDrawer(!showDrawer)}>
                <MenuIcon />
              </IconButton>
            </Hidden>
            <Hidden smDown>
              {!user && <Button onClick={() => setShowLoginDialog(true)}>Anmelden</Button>}
              {!!user && <NavigationProfile />}
            </Hidden>
          </div>
        </div>
      </Container>
      <LoginDialog open={showLoginDialog} setOpen={setShowLoginDialog} />
    </>
  );
};

export default Navigation;
