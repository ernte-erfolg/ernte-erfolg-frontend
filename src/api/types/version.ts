export interface IBackendInfo {
  'display-ribbon-on-profiles': string;
  git: {
    branch: string;
    commit: {
      id: {
        abbrev: string;
        describe: string;
      };
    };
  };
  build: {
    artifact: string;
    name: string;
    time: string;
    version: string;
    group: string;
  };
  activeProfiles: string[];
}
