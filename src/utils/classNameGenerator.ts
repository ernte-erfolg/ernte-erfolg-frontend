import type { Rule, StyleSheet } from 'jss';

/**
 * By default, JSS generates stuff like MuiDialog-paper-24, which is horrible to test as the suffix counter
 * is not deterministic. Also it is not a problem if the class MuiDialog-paper is defined multiple times,
 * as long as the CSS is always the same for the class.
 * This function tries to cleverly generate the classnames without a prefix by caching every class name
 * and its corresponding CSS rules. Only if there are different CSS rules for the same class name, a
 * numeric suffix is appended.
 */
export const classNameGenerator = () => {
  // TODO: Prevent JSS from inserting the same CSS rule multiple times for the same class name
  const rules = new Map<string, string[]>();
  let counter = 0;
  return (rule: Rule, sheet?: StyleSheet<string>): string => {
    let key = rule.key;
    if (sheet?.options?.classNamePrefix) {
      key = `${sheet.options.classNamePrefix}-${key}`;
    }
    try {
      const css = rule.toString();
      if (rules.has(key)) {
        const sheetsForRule = rules.get(key)!;
        if (css === '') {
          sheetsForRule.push(css);
          return `${key}-${sheetsForRule.length - 1}`;
        } else if (sheetsForRule.length === 1 && sheetsForRule[0] === css) {
          return key;
        } else {
          const index = sheetsForRule.indexOf(css);
          if (index >= 0) {
            return `${key}-${index}`;
          } else {
            sheetsForRule.push(css);
            return `${key}-${sheetsForRule.length - 1}`;
          }
        }
      } else {
        rules.set(key, [css]);
        return key;
      }
    } catch (err) {
      console.warn(key);
      return `${key}-${counter++}`;
    }
  };
};
