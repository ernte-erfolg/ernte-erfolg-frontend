import React from 'react';
import { Link } from 'react-router-dom';
import { makeStyles, Container, Grid } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  footer: {
    width: '100%',
    background: theme.palette.grey[200],
  },
  inner: {
    paddingTop: theme.spacing(4),
    paddingBottom: theme.spacing(4),
  },
  icon: {
    width: '28px',
    height: '28px',
    marginRight: theme.spacing(1),
  },
  headline: {
    fontSize: '18px',
    marginBottom: theme.spacing(2),
  },
  link: {
    fontWeight: 'lighter',
    color: theme.palette.grey[800],
    marginTop: theme.spacing(1),
  },
}));

const Footer: React.FC = () => {
  const classes = useStyles();
  return (
    <div className={classes.footer}>
      <Container>
        <div className={classes.inner}>
          <Grid container spacing={4}>
            <Grid item xs={12} lg={4}>
              <div className={classes.headline}>ErnteErfolg</div>
              <div className={classes.link}>
                <Link to="/faq">FAQ</Link>
              </div>
              <div className={classes.link}>
                <Link to="/version">Version</Link>
              </div>
            </Grid>
            <Grid item xs={12} lg={4}>
              <div className={classes.headline}>Kontakt</div>
              <div className={classes.link}>
                <a href="mailto:kontakt@ernte-erfolg.de">kontakt@ernte-erfolg.de</a>
              </div>
              <div className={classes.link}>
                <a href="https://www.instagram.com/ernteerfolg/">
                  <img className={classes.icon} src="/img/icon_instagram.svg" alt="Instagram" />
                </a>
                <a href="https://www.facebook.com/ernteerfolg/">
                  <img className={classes.icon} src="/img/icon_fb.svg" alt="Facebook" />
                </a>
                <a href="https://twitter.com/ernte_erfolg">
                  <img className={classes.icon} src="/img/icon_twitter.svg" alt="Twitter" />
                </a>
                <a href="https://www.linkedin.com/company/ernteerfolg/">
                  <img className={classes.icon} src="/img/icon_linkedin.svg" alt="LinkedIn" />
                </a>
                <a href="https://www.gitlab.com/ernte-erfolg">
                  <img className={classes.icon} src="/img/icon_gitlab.svg" alt="GitLab" />
                </a>
              </div>
            </Grid>
            <Grid item xs={12} lg={4}>
              <div className={classes.headline}>Rechtliches</div>
              <div className={classes.link}>
                <Link to="/imprint">Impressum</Link>
              </div>
              <div className={classes.link}>
                <Link to="/privacy">Datenschutz</Link>
              </div>
            </Grid>
          </Grid>
        </div>
      </Container>
    </div>
  );
};

export default Footer;
