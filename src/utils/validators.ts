export function isPhone(phone: string) {
  return phone.match(/\(?\+\(?49\)?[ ()]?([- ()]?\d[- ()]?){10}/);
}

export function isEmail(email: string) {
  return email.match(
    /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  );
}

export function isZip(zip: string) {
  return [4, 5].includes(zip.length);
}

export function checkPassword(password: string) {
  // http://regexlib.com/REDetails.aspx?regexp_id=1923
  // It expects at least 1 lowercase letter, 1 uppercase letter, and 1 digit.
  // It will also allow for some special characters.
  // The length should be greater than 8 characters.
  // The sequence of the characters is not important.
  return password.match(/(?=^.{8,}$)(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s)[0-9a-zA-Z!@#$%^&*()]*$/);
}
