import React from 'react';
import { Typography, Grid, Box } from '@material-ui/core';
import FavoriteIcon from '@material-ui/icons/Favorite';
import Infobox from '../Infobox';
import { Task } from '../../api';
import HelperTimeDialog from '../HelperTimeDialog';
import HelperBadge from '../HelperBadge';

interface IFarmerInfoboxProps {
  task: Task | null;
}

const FarmerInfobox: React.FC<IFarmerInfoboxProps> = ({ task }) => {
  return (
    <Infobox title="Angemeldete Helfer" icon={<FavoriteIcon />}>
      <Grid container spacing={1} alignContent="flex-start">
        {(!task || (task.helpers.length === 0 && !task.hiddenHelpers)) && (
          <Grid item xs={12}>
          <Typography variant="body2">
            Bisher haben sich noch keine Helfer auf das Gesuch angemeldet.
          </Typography>
          </Grid>
        )}
        {task?.hiddenHelpers && (
          <Grid item xs={12}>
          <Typography variant="body2">
            Aus Datenschutzgründen wird die Liste der Helfer nach 90 Tagen ausgeblendet.
          </Typography>
          </Grid>
        )}
        {task?.helpers?.map((h) => (
          <Grid key={h.id} item xs={12}>
            <HelperTimeDialog
              helper={h}
              days={task?.guaranteedDaysByHelper![h.id.toString()]}
              activator={
                <Box display="flex" alignItems="center">
                  <HelperBadge percentage={h.levelProgress} level={h.level} size={60} />
                  <Box ml={3}>{`${h.firstName} ${h.lastName}`}</Box>
                </Box>
              }
            />
          </Grid>
        ))}
      </Grid>
    </Infobox>
  );
};

export default FarmerInfobox;
