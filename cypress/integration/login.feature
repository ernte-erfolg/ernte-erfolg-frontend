Feature: Login

  Background:
    Given I am not logged in
    And I am on the "/" page
    And I click on the "Anmelden" button

  Scenario Outline: Normal Login Farm
    Given I type "<email>" into the "email" field
    And I type "<password>" into the "password" field
    When I click on the login button
    Then I should be logged in as "<profileName>"
    And I should be on the "/farm" page

    Examples:
    | email                 | password | profileName |
    | farm2@ernte-erfolg.de | 112211aA | Testfarm    |
  
  Scenario Outline: Normal Login Helper
    Given I type "<email>" into the "email" field
    And I type "<password>" into the "password" field
    When I click on the login button
    Then I should be logged in as "<profileName>"
    And I should be on the "/helping" page

    Examples:
    | email                   | password | profileName      |
    | helper2@ernte-erfolg.de | 112211aA | Max Musterhelfer |

  Scenario: Invalid Credentials
    Given I type "invalid@mail.de" into the "email" field
    And I type "112211aA" into the "password" field
    When I click on the login button
    Then I should see an error message
