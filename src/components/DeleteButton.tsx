import React from 'react';
import { makeStyles, Button, ButtonProps } from '@material-ui/core';

const useStyles = makeStyles(
  (theme) => ({
    root: (props: ButtonProps) => ({
      color: props.variant === 'contained' ? theme.palette.error.contrastText : theme.palette.error.main,
      border: props.variant === 'outlined' ? `1px solid ${theme.palette.error.main}` : undefined,
      backgroundColor: props.variant === 'contained' ? theme.palette.error.main : undefined,
      '&:hover': {
        backgroundColor: props.variant === 'contained' ? theme.palette.error.dark : undefined,
      },
    }),
  }),
  { name: 'DeleteButton' }
);

const DeleteButton: React.FC<ButtonProps> = (props) => {
  const classes = useStyles(props);
  return <Button className={classes.root} {...props} />;
};

export default DeleteButton;
