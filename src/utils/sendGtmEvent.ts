import TagManager from 'react-gtm-module';

export function sendGtmEvent(eventName: string) {
  TagManager.dataLayer({ dataLayer: { event: eventName }});
}

export function setGtmVariable(variable: string, value: any) {
  TagManager.dataLayer({ dataLayer: { [variable]: value } });
}
