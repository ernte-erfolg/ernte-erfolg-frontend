import React from 'react';
import { Moment } from 'moment';
import { useSelector } from 'react-redux';
import { selectHelperAvailableDaysMoment } from '../../../slices/helperCalendarSlice';
import { Grid, Typography, Box } from '@material-ui/core';
import { MonthView, Infobox, CalendarLegend } from '../../../components';

const SelectDays: React.FC = () => {
  const availableDays = useSelector(selectHelperAvailableDaysMoment);

  const months: Moment[] = [];
  for (const d of availableDays) {
    if (!months.find((m) => m.isSame(d.date, 'month'))) {
      months.push(d.date);
    }
  }

  months.sort((a, b) => a.unix() - b.unix());

  return (
    <Grid container spacing={3}>
      <Grid item xs={12} lg={9}>
        <Grid container>
          <Grid item xs={12}>
            <Box mb={4}>
              <h2>Zeitraum</h2>
              <p>Bitte wähle hier die Tage aus, an welchen du helfen kannst.</p>
            </Box>
          </Grid>
          {months.map((m, i) => (
            <Grid key={m.unix()} item xs={12} lg={6}>
              <Box my={4} ml={{ lg: i % 2 !== 0 ? 4 : 0 }} mr={{ lg: i % 2 !== 0 ? 0 : 4 }}>
                <Box mb={2}>
                  <Typography variant="h4">{m.format('MMMM YYYY')}</Typography>
                </Box>
                <MonthView helper year={m.year()} month={m.month()} />
              </Box>
            </Grid>
          ))}
        </Grid>
      </Grid>
      <Grid item xs={12} lg={3}>
        <Infobox title="Legende">
          <CalendarLegend />
        </Infobox>
      </Grid>
    </Grid>
  );
};

export default SelectDays;
