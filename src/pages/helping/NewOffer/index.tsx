import React, { useState, useEffect } from 'react';
import { useParams, Link, useHistory } from 'react-router-dom';
import { Button, Stepper, Step, StepLabel, makeStyles, Box, Typography } from '@material-ui/core';
import { LoadingErrorDisplay, ActionBar } from '../../../components';
import { Task, CreateOffer, TaskAPI, OfferAPI, Offer } from '../../../api';
import { workdayToDto, dateFromDto } from '../../../utils/workdayConverter';
import SelectDays from './SelectDays';
import SelectHours from './SelectHours';
import { sendGtmEvent } from '../../../utils/sendGtmEvent';
import { useSelector, useDispatch } from 'react-redux';
import { selectUserState } from '../../../slices/userSlice';
import {
  setHelperAvailableWorkdays,
  selectHelperSelectedDaysMoment,
  resetHelperCalendarState,
} from '../../../slices/helperCalendarSlice';
import { GreyBarPage } from '../../../containers';
import Confirmation from './Confirmation';

const useStyles = makeStyles(() => ({
  stepper: {
    backgroundColor: 'transparent',
    padding: 0,
  },
}));

export default function OfferHelp() {
  const { taskId } = useParams();
  const history = useHistory();
  const classes = useStyles();
  const dispatch = useDispatch();
  const { helper } = useSelector(selectUserState);
  const selectedDays = useSelector(selectHelperSelectedDaysMoment);

  const [error, setError] = useState('');
  const [loading, setLoading] = useState(false);
  const [step, setStep] = useState(0);

  const [confirmed, setConfirmed] = useState(false);
  const [task, setTask] = useState<Task | null>(null);
  const [offer, setOffer] = useState<Offer | null>(null);

  useEffect(() => {
    (async () => {
      if (!taskId || !parseInt(taskId)) {
        return setError('Ungültige Task-ID');
      }
      try {
        setError('');
        setLoading(true);
        const t = await new TaskAPI().getTask(parseInt(taskId));

        try {
          await new OfferAPI().getOfferByTask(t.id);
          // when the above call didn't throw -> offer already exists
          return history.push(`/task/${t.id}`);
        } catch (err) {
          // offer not found for the task -> this is actually good because we don't allow duplicate offers.
        }

        setTask(t);
        setStep(1);

        dispatch(
          setHelperAvailableWorkdays(
            t.requestedDays.map((rd) => {
              const date = dateFromDto(rd);
              let remainingHours = rd.workingHours;
              const guaranteedDay = t.guaranteedDays.find((gd) => dateFromDto(gd).isSame(date, 'day'));
              if (guaranteedDay) {
                remainingHours = rd.workingHours - guaranteedDay.workingHours;
              }
              return {
                date,
                requestedHours: rd.workingHours,
                remainingHours,
                selectedHours: 0,
              };
            })
          )
        );

        setLoading(false);
      } catch (err) {
        console.log(err);
        setLoading(false);
        setError((err as Error).message || 'Fehler beim Laden des Gesuchs');
      }
    })();

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [taskId]);

  useEffect(() => {
    return () => {
      dispatch(resetHelperCalendarState());
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const sendOffer = async () => {
    if (!taskId || !parseInt(taskId)) {
      return setError('Ungültige TaskId');
    }
    if (!helper || !helper.id) {
      return setError('Noch kein Profil angelegt');
    }
    try {
      setError('');
      setLoading(true);
      const guaranteedDays = selectedDays
        .filter((d) => d.selectedHours > 0)
        .map((d) => workdayToDto({ date: d.date, hours: d.selectedHours }));

      const offer: CreateOffer = {
        taskId: parseInt(taskId),
        guaranteedDays,
      };
      const createdOffer = await new OfferAPI().createOffer(offer);
      sendGtmEvent('eeOfferSend');
      dispatch(resetHelperCalendarState());
      setOffer(createdOffer);
      setLoading(false);
      setStep(3);
    } catch (err) {
      console.log(err);
      setLoading(false);
      setError((err as Error).message || 'Fehler beim Senden');
    }
  };

  const canBack = step === 2;
  const canContinue = (step === 1 && selectedDays.length > 0) || (step === 2 && confirmed) || step === 3;

  const back = () => {
    if (canBack) {
      setStep(step - 1);
      window.scrollTo(0, 0);
    }
  };

  const next = () => {
    if (step === 1) {
      setStep(step + 1);
      window.scrollTo(0, 0);
    } else if (step === 2) {
      sendOffer();
    } else if (step === 3) {
      history.push(`/task/${task!.id}`);
    }
  };

  const stepper = (
    <Stepper className={classes.stepper} activeStep={step === 0 ? 0 : step - 1} alternativeLabel>
      <Step>
        <StepLabel>Zeitraum</StepLabel>
      </Step>
      <Step>
        <StepLabel>Arbeitsstunden</StepLabel>
      </Step>
      <Step>
        <StepLabel>Zusammenfassung</StepLabel>
      </Step>
    </Stepper>
  );

  const actionBar = (
    <ActionBar
      sticky
      topActions={<LoadingErrorDisplay loading={loading} error={error} />}
      leftActions={
        <Button variant="outlined" size="large" disabled={!canBack || loading} onClick={back}>
          Zurück
        </Button>
      }
      rightActions={
        <Box display="flex">
          <Button
            component={Link}
            to={task ? `/task/${task.id}` : '/helping'}
            variant="outlined"
            size="large"
          >
            Abbrechen
          </Button>
          <Box ml={2}>
            <Button
              variant="contained"
              color="primary"
              size="large"
              disabled={!canContinue || loading}
              onClick={next}
            >
              {step < 2 ? 'Weiter' : 'Hilfe anbieten'}
            </Button>
          </Box>
        </Box>
      }
    />
  );

  return (
    <GreyBarPage
      greyBarContent={stepper}
      headline={
        <>
          <Typography variant="h1">{task?.title}</Typography>
          <Typography variant="body2">
            Gesuch von{' '}
            <u>
              <Link to={`/farm/profile/${task?.farmId}`}>{task?.farmName}</Link>
            </u>
          </Typography>
        </>
      }
    >
      {step === 1 && <SelectDays />}
      {step === 2 && <SelectHours days={selectedDays} confirmed={confirmed} setConfirmed={setConfirmed} />}
      {step === 3 && <Confirmation task={task!} offer={offer!} />}
      {step !== 3 && actionBar}
    </GreyBarPage>
  );
}
