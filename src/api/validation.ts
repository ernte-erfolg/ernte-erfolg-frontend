function getMinLength(v: number) {
  return {
    value: v,
    message: `Bitte mindestens ${v} Zeichen eingeben`,
  };
}

function getMaxLength(v: number) {
  return {
    value: v,
    message: `Bitte maximal ${v} Zeichen eingeben`,
  };
}

export const ValidationTypes = {
  Email: {
    minLength: getMinLength(5),
    maxLength: getMaxLength(254),
    pattern: {
      // https://html.spec.whatwg.org/multipage/input.html#e-mail-state-(type=email)
      // eslint-disable-next-line no-useless-escape
      value: /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/,
      message: 'Ungültige E-Mail-Adresse',
    },
  },
  Password: {
    pattern: {
      value: /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}$/,
      message:
        'Das Passwort muss mindestens 8 Zeichen, einen Kleinbuchstaben, einen Großbuchstaben und eine Zahl beinhalten.',
    },
  },
  FarmName: { minLength: getMinLength(2), maxLength: getMaxLength(120) },
  FarmTitle: { minLength: getMinLength(1), maxLength: getMaxLength(120) },
  Phone: { minLength: getMinLength(1), maxLength: getMaxLength(30) },
  Street: { minLength: getMinLength(1), maxLength: getMaxLength(120) },
  HouseNr: { minLength: getMinLength(1), maxLength: getMaxLength(10) },
  Zip: { minLength: getMinLength(1), maxLength: getMaxLength(10) },
  City: { minLength: getMinLength(1), maxLength: getMaxLength(50) },
  Description: { minLength: getMinLength(1), maxLength: getMaxLength(3000) },
  FirstName: { minLength: getMinLength(1), maxLength: getMaxLength(30) },
  LastName: { minLength: getMinLength(1), maxLength: getMaxLength(30) },
};
