const axios = require('axios');

const setupGlobals = () => {
  global.mailhogUrl = process.env.MAILHOG_URL || 'http://localhost:8025';
  global.backendUrl = process.env.BACKEND_URL || 'http://localhost:8080';
  console.log("Mailhog URL:", global.mailhogUrl);
  console.log("Backend URL:", global.backendUrl);
};

const clearInbox = async () => {
  await axios.delete(`${global.mailhogUrl}/api/v1/messages`);
};

const createAccount = async (email, password, userClass, activate, profileData) => {
  const ax = axios.create({
    baseURL: global.backendUrl + '/api',
  });

  console.log('Creating user', email);

  if (activate) {
    await clearInbox();
  }

  console.log("Registering");
  await ax.post('/account/register', {
    email,
    password,
    userClass,
    langKey: 'de',
  });

  if (activate) {
    console.log('Activating user', email);

    // wait 5s for the email to arrive
    await new Promise((res) => setTimeout(res, 5000));

    const messages = (await axios.get(`${global.mailhogUrl}/api/v1/messages`)).data;
    const mail = messages[0];
    const matches = /confirm\/([a-zA-Z0-9]+)/.exec(mail.Content.Body);
    const key = matches[1];
    await ax.get(`/account/activate?key=${key}`);

    if (profileData) {
      console.log('Setting profile of', email);
      // login
      const token = (await ax.post('/account/authenticate', { username: email, password, rememberMe: true }))
        .data.id_token;
      await ax.post(userClass === 'FARM' ? '/farms' : '/helpers', profileData, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
    }
  }
};

module.exports = {
  setupGlobals,
  clearInbox,
  createAccount,
};
