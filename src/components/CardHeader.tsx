import React from 'react';
import { makeStyles, Typography } from '@material-ui/core';
import c from 'classnames';

const useStyles = makeStyles(
  (theme) => ({
    root: {
      backgroundColor: theme.palette.primary.main,
      color: theme.palette.common.white,
      padding: theme.spacing(2),
    },
    disabled: {
      backgroundColor: theme.palette.grey[500]
    }
  }),
  { name: 'CardHeader' }
);

interface ICardHeaderProps {
  title: string;
  disabled?: boolean;
}

const CardHeader: React.FC<ICardHeaderProps> = ({ title, disabled }) => {
  const classes = useStyles();
  return (
    <div className={c(classes.root, { [classes.disabled]: !!disabled })}>
      <Typography variant="h5">{title}</Typography>
    </div>
  );
};

export default CardHeader;
