import { BaseAPI } from './base';
import { CreateTask, Task, TasksInArea } from '../types';

export class TaskAPI extends BaseAPI {
  protected baseUrl = '/tasks';
  public createTask = (task: CreateTask) => this.post<Task>('/', task);
  public updateTask = (id: number, task: CreateTask) => this.put<Task>(`/${id}`, task);
  public deactivateTask = (id: number) => this.put<Task>(`/${id}/deactivate`, undefined);
  public activateTask = (id: number) => this.put<Task>(`/${id}/activate`, undefined);
  public getTask = (id: number) => this.get<Task>(`/${id}`);
  public getTasksByFarm = (farmId: number) => this.get<Task[]>(`/byFarm/${farmId}`);
  public searchTasks = (zip: string) => this.get<TasksInArea[]>('/search', { params: { zip } });
}
