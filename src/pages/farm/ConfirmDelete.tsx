import React, { useEffect, useState } from 'react';
import { useParams, useHistory } from 'react-router-dom';
import { Box, Typography } from '@material-ui/core';
import { Alert } from '@material-ui/lab';
import { FarmAPI } from '../../api';
import { useDispatch } from 'react-redux';
import { logout } from '../../slices/userSlice';

const ConfirmDelete: React.FC = () => {
  const dispatch = useDispatch();
  const { key } = useParams();
  const history = useHistory();
  const [error, setError] = useState('');

  useEffect(() => {
    (async () => {
      if (!key) {
        setError('Ungültiger Link');
        return;
      }
      try {
        await new FarmAPI().confirmDelete(key);
        dispatch(logout());
        setTimeout(() => history.push('/'), 1500);
      } catch (err) {
        setError(`Fehler beim Löschen des Accounts`);
        console.error('Error', err);
      }
    })();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [key]);

  return (
    <Box my={4}>
      <Typography variant="h4">Account löschen</Typography>
      <p>
        {!error && <>Wir löschen gerade Ihren Account. Sie werden gleich weitergeleitet.</>}
        {error && (
          <Alert severity="error" color="error">
            {error}
          </Alert>
        )}
      </p>
    </Box>
  );
};

export default ConfirmDelete;
