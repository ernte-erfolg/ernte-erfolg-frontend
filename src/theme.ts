import { createMuiTheme } from '@material-ui/core/styles';

export default createMuiTheme({
  typography: {
    fontFamily: 'Roboto, sans-serif',
    fontSize: 16,
    h1: {
      fontFamily: 'Roboto Slab',
      fontSize: '34px',
      fontWeight: 'bold',
    },
    h2: {
      fontSize: '30px',
      fontWeight: 'normal',
    },
    h3: {
      fontFamily: 'Roboto Slab',
      fontSize: '26px',
      fontWeight: 'normal',
    },
    h4: {
      fontSize: '24px',
      fontWeight: 'normal',
    },
    h5: {
      fontSize: '20px',
      fontWeight: 'normal',
    },
    h6: {
      fontSize: '16px',
      textTransform: 'uppercase',
    },
    subtitle1: {
      fontSize: '20px',
    },
    subtitle2: {
      fontSize: '14px',
    },
  },
  palette: {
    primary: {
      main: '#9AAA25',
      contrastText: '#ffffff',
    },
    secondary: {
      main: '#4B3425',
    },
    background: {
      default: '#ffffff',
    },
    text: {
      primary: '#3E3E3E',
    },
  },
  shape: {
    borderRadius: 0,
  },
  overrides: {
    MuiOutlinedInput: {
      root: {
        backgroundColor: 'white',
      },
    },
    MuiButton: {
      disableElevation: true as any,
      root: {
        textTransform: 'unset',
        '&:hover': {
          boxShadow: 'none !important',
        },
      },
      contained: {
        boxShadow: 'none',
      },
    },
  },
});
