import React from 'react';
import { Button, CircularProgress, makeStyles, ButtonProps } from '@material-ui/core';

const useStyles = makeStyles(() => ({
  buttonProgress: {
    position: 'absolute',
    top: '50%',
    left: '50%',
    marginTop: -12,
    marginLeft: -12,
  },
}));

interface ILoadingButtonProps extends ButtonProps {
  disabled?: boolean;
  loading?: boolean;
}

const LoadingButton: React.FC<React.PropsWithChildren<ILoadingButtonProps>> = (props) => {
  const classes = useStyles();
  return (
    <Button
      {...{...props, loading: undefined }}
      style={{ position: 'relative' }}
      disabled={props.disabled || props.loading}
    >
      {props.children}
      {props.loading && (
        <CircularProgress size={24} className={classes.buttonProgress} />
      )}
    </Button>
  );
}

export default LoadingButton;
