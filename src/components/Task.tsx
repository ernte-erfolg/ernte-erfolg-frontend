import React from 'react';
import { Link } from 'react-router-dom';
import { Box, Card, CardContent, LinearProgress, Typography, makeStyles } from '@material-ui/core';
import { Task as TaskModel } from '../api';
import { getHoursFromTask, getFirstDayOfTask, getLastDayOfTask } from '../utils/taskUtils';
import CardHeader from './CardHeader';

const useStyles = makeStyles((theme) => ({
  root: {
    cursor: 'pointer',
    '&:hover': {
      boxShadow: theme.shadows[7],
    },
  },
  cardContent: {
    paddingBottom: theme.spacing(1),
  },
  progressBar: {
    height: '16px',
    backgroundColor: theme.palette.grey[200],
    '& .MuiLinearProgress-bar': {
      backgroundColor: theme.palette.grey[500],
    },
  },
}));

interface ITaskProps {
  task: Pick<
    TaskModel,
    'title' | 'requestedDays' | 'guaranteedDays' | 'id' | 'farmName' | 'street' | 'houseNr' | 'zip' | 'city' | 'active'
  >;
}

const Task: React.FC<ITaskProps> = ({ task }) => {
  const classes = useStyles();
  const hours = getHoursFromTask(task);
  const percentage =
    hours.requestedHours > 0 ? Math.round(100 * (hours.guaranteedHours / hours.requestedHours)) : 0;

  const firstDate = getFirstDayOfTask(task).format('DD.MM.YYYY');
  const lastDate = getLastDayOfTask(task).format('DD.MM.YYYY');

  return (
    <Link to={`/task/${task.id}`}>
      <Card className={classes.root}>
        <CardHeader title={task.title} disabled={!task.active} />
        <CardContent className={classes.cardContent}>
          <Box mb={1}>
            <Typography variant="body2">{task.farmName}</Typography>
          </Box>
          <Box mb={2} fontWeight="light" fontSize="14px">
            {task.street} {task.houseNr}, {task.zip} {task.city}
          </Box>
          <Box mb={2}>
            <Typography variant="body2">
              {firstDate ? `${firstDate} - ${lastDate}` : 'Kein Datum angegeben'}
            </Typography>
          </Box>
          <Box fontWeight="light" fontSize="12px" mb={1}>
            {hours.guaranteedHelpers} von {hours.requestedHelpers} Helfertagen
          </Box>
          <LinearProgress className={classes.progressBar} variant="determinate" value={percentage} />
        </CardContent>
      </Card>
    </Link>
  );
};

export default Task;
