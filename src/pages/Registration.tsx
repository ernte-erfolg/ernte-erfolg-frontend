import React, { useState } from 'react';
import {
  Typography,
  TextField,
  FormControlLabel,
  Checkbox,
  Box,
  Link,
  Grid,
  Button,
} from '@material-ui/core';
import InfoIcon from '@material-ui/icons/Info';
import { useForm } from 'react-hook-form';
import { Link as RouterLink } from 'react-router-dom';
import { AccountAPI, ProfileType, ValidationTypes, CreateUser } from '../api';
import { ConfirmEmail, LoadingButton, LoadingErrorDisplay, Infobox } from '../components';
import { GreyBarPage } from '../containers';

interface IRegistrationProps {
  profileType: ProfileType;
}

interface IFields {
  email: string;
  password: string;
  repeatPassword: string;
  confirmed: boolean;
}

const farmInfoText = [
  'Sie besitzen einen Hof und/oder Ackerland und brauchen Hilfe bei der Ernte oder sonstigen Aufgaben auf Ihrem Hof? Dann sind Sie hier genau richtig.',
  'Wenn Sie stattdessen Bauernhöfe bei anfallenden Aufgaben unterstützen wollen, sollten Sie sich als Helfer registrieren.',
];

const helperInfoText = [
  'Du willst Bauernhöfe bei anfallenden Aufgaben unterstützen? Dann bist Du hier genau richtig! Gemeinsam mit Ernte-Erfolg sicherst Du die Ernte.',
  'Wenn Du einen eigenen Bauernhof oder Ackerland besitzt, auf dem Du Hilfe benötigst, solltest Du dich als Landwirt registrieren.',
];

const Registration: React.FC<IRegistrationProps> = ({ profileType }) => {
  const { register, errors, handleSubmit, formState, getValues } = useForm<IFields>({ mode: 'onChange' });

  const [error, setError] = useState('');
  const [loading, setLoading] = useState(false);
  const [showConfirmation, setShowConfirmation] = useState(false);
  const [createAccountPayload, setCreateAccountPayload] = useState<any>(null);

  const createUser = async (createUser: CreateUser) => {
    try {
      setLoading(true);
      setError('');
      await new AccountAPI().registerAccount(createUser);
      setShowConfirmation(true);
    } catch (err) {
      console.error('Error', err.message);
      return setError((err as Error).message || 'Ups, da ist leider etwas schief gegangen :/');
    } finally {
      setLoading(false);
    }
  };

  const onSubmit: (v: IFields) => Promise<void> = async ({ email, password, repeatPassword, confirmed }) => {
    if (password !== repeatPassword) {
      return setError('Die Passwörter müssen übereinstimmen');
    } else if (!confirmed) {
      return setError('Die Datenschutzerklärung muss akzeptiert werden.');
    }
    const accountPayload = {
      email: email.trim(),
      password,
      langKey: 'de',
      userClass: profileType,
    };
    setCreateAccountPayload(accountPayload);
    await createUser(accountPayload);
  };

  const LoginFields = (
    <>
      <form onSubmit={handleSubmit(onSubmit)}>
        <Box my={2}>
          <TextField
            name="email"
            fullWidth
            required
            label="E-Mail"
            variant="outlined"
            inputRef={register({ ...ValidationTypes.Email, required: true })}
            error={!!errors.email}
            helperText={errors.email?.message}
          />
        </Box>

        <Box my={2}>
          <TextField
            name="password"
            fullWidth
            required
            label="Passwort"
            variant="outlined"
            type="password"
            inputRef={register({ ...ValidationTypes.Password, required: true })}
            error={!!errors.password}
            helperText={errors.password?.message}
          />
        </Box>

        <Box my={2}>
          <TextField
            name="repeatPassword"
            fullWidth
            required
            label="Passwort wiederholen"
            variant="outlined"
            type="password"
            inputRef={register({
              required: true,
              validate: (v) => getValues().password === v || 'Die Passwörter müssen übereinstimmen',
            })}
            error={!!errors.repeatPassword}
            helperText={errors.repeatPassword?.message}
          />
        </Box>

        <Box my={2}>
          <FormControlLabel
            control={
              <Checkbox
                name="confirmed"
                color="primary"
                required
                inputRef={register({
                  required: true,
                })}
              />
            }
            label={
              <span>
                Ich habe die{' '}
                <Link component={RouterLink} to="/privacy" target="_blank">
                  Datenschutzerklärung
                </Link>{' '}
                zu Kenntnis genommen
              </span>
            }
          />
        </Box>

        <Box my={2}>
          <LoadingButton
            variant="contained"
            color="primary"
            loading={loading}
            disabled={!formState.isValid}
            type="submit"
            fullWidth
          >
            Jetzt anmelden
          </LoadingButton>
        </Box>
      </form>
      <Box mt={6}>
        <p>Doch als {profileType === 'FARM' ? 'Helfer' : 'Landwirt'} registrieren? Kein Problem.</p>
        <Button
          component={RouterLink}
          to={`/${profileType === 'FARM' ? 'helping' : 'farm'}/register`}
          variant="outlined"
          fullWidth
        >
          Als {profileType === 'FARM' ? 'Helfer' : 'Landwirt'} registrieren
        </Button>
      </Box>
    </>
  );

  let greyBarText = '';
  if (showConfirmation) {
    greyBarText =
      profileType === 'FARM'
        ? 'Danke für Ihre Registrierung bei Ernte-Erfolg. Bitte bestätigen Sie Ihre E-Mail-Adresse.'
        : 'Danke für Deine Registrierung bei Ernte-Erfolg. Bitte bestätige Deine E-Mail-Adresse.';
  } else {
    greyBarText =
      profileType === 'FARM'
        ? 'Danke für Ihr Interesse an Ernte-Erfolg. Bitte geben Sie Ihre E-Mail-Adresse und ein von Ihnen gewähltes, sicheres Passwort an.'
        : 'Danke für Dein Interesse an Ernte-Erfolg. Bitte gib Deine E-Mail-Adresse und ein von Dir gewähltes, sicheres Passwort an.';
  }

  return (
    <GreyBarPage
      greyBarContent={<Typography variant="body2">{greyBarText}</Typography>}
      headline={<Typography variant="h1">Registrieren</Typography>}
    >
      <Grid container>
        <Grid item xs={12} md={8} lg={6}>
          <LoadingErrorDisplay loading={loading} error={error} />
          {showConfirmation ? (
            <ConfirmEmail profileType={profileType} resendEmail={() => createUser(createAccountPayload)} />
          ) : (
            LoginFields
          )}
        </Grid>
        <Grid item xs={12} md={1} lg={3}>
          <Box my={4}></Box>
        </Grid>
        <Grid item xs={12} md={3}>
          <Infobox
            title={`Als ${profileType === 'FARM' ? 'Landwirt' : 'Helfer'} registrieren`}
            icon={<InfoIcon />}
          >
            {(profileType === 'FARM' ? farmInfoText : helperInfoText).map((t, i) => (
              <p key={i}>{t}</p>
            ))}
          </Infobox>
        </Grid>
      </Grid>
    </GreyBarPage>
  );
};

export default Registration;
